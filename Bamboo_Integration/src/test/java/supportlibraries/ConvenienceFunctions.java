package supportlibraries;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.mfa.framework.Status;
import com.mfa.framework.selenium.CraftDriver;
import allocator.Allocator;

/**
 * Contains low level functions for clicking, typing, verifying, etc.
 * 
 * @author cognizant
 */
public class ConvenienceFunctions extends ReusableLibrary
{
	public ConvenienceFunctions(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	/**
	 * The {@link DataTable} object (passed from the test script)
	 */
	// protected DataTable dataTable;
	/**
	 * The {@link Report} object (passed from the test script)
	 */
	// protected Report report;
	/**
	 * The {@link WebDriver} object
	 */
	// protected CraftDriver driver;
	/**
	 * The {@link ScriptHelper} object (required for calling one reusable
	 * library from another)
	 */
	// protected ScriptHelper scriptHelper;
	/**
	 * The {@link Properties} object with settings loaded from the framework
	 * properties file
	 */

	// protected Properties properties = Settings.getInstance();
	/**
	 * The {@link //frameworkparameters} object
	 */
	public Boolean verifyElementPresent(By by, String strElementDesc) throws Exception
	{
		Boolean bElement = isElementPresent(by, strElementDesc);
		if (bElement)
		{
			return true;
		}
		else
		{
			report.updateTestLog("verifyElementPresent", "The Element <b>" + strElementDesc + "</b> is not present in the Current page.", Status.FAIL);
			return false;
		}
	}

	public void appLoader() throws Exception
	{
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		WebDriverWait wait2 = new WebDriverWait(driver.getWebDriver(), 1);
		if (isElementVisible(By.xpath("//img[@id='app-loader']"), "app-loader"))
			for (int i = 1; i < 20; i++)
			{
				try
				{
					wait2.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//img[@id='app-loader']")));
					System.out.println("App load in progress is over");
					break;
				}
				catch (Exception e)
				{
					System.out.println("waiting continues");
				}
			}
	}

	public void lightningSpinner() throws Exception
	{
		WebDriverWait wait1 = new WebDriverWait(driver.getWebDriver(), 1);
		if (isElementVisible(By.xpath("//lightning-spinner"), "app-loader"))
			for (int i = 1; i < 20; i++)
			{
				try
				{
					wait1.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//lightning-spinner")));
					System.out.println("App load in progress is over");
					break;
				}
				catch (Exception e)
				{
					System.out.println("Waiting continues");
				}
			}
	}

	public boolean isElementPresent(By by, String strElementDesc)
	{
		int iWaitSeconds = Allocator.timeout;
		Boolean bElement = isElementPresent(by, strElementDesc, iWaitSeconds);
		return bElement;
	}

	public boolean isElementPresent(By by, String strElementDesc, int iWaitSeconds)
	{
		Boolean bElement = false;
		try
		{
			bElement = isElementPresent2(by, strElementDesc, iWaitSeconds);
		}
		catch (InvocationTargetException e)
		{
			waitForSeconds(1);
			try
			{
				bElement = isElementPresent(by, strElementDesc, iWaitSeconds);
			}
			catch (Exception e1)
			{
			}
		}
		catch (Exception e)
		{
			// e.printStackTrace();
		}
		return bElement;
	}

	public boolean isElementPresent2(By by, String strElementDesc, int iWaitSeconds) throws InvocationTargetException, Exception
	{
		Boolean bElement = false;
		int iTryCount = 2;
		// System.out.println( "Is Element Present: " + strElementDesc);
		for (int i = 0; i < iTryCount; i++)
		{
			try
			{
				WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), iWaitSeconds);
				wait.until(ExpectedConditions.presenceOfElementLocated(by));
				bElement = true;
				break;
			}
			catch (StaleElementReferenceException e)
			{
				// System.out.println( i + " - waitTillElementExist : " +
				// strElementDesc); repeat again
			}
			catch (NoSuchElementException e)
			{
				// System.out.println("NoSuchElementException - WebElement not
				// present : " + strElementDesc);
				// report.updateTestLog("Validate <b>"+strElementDesc+"</b>
				// should present", "Element is not present", Status.FAIL);
				// i = iTryCount;
			}
			catch (Exception e)
			{
				// System.out.println("Exception - WebElement not present : " +
				// strElementDesc);
				// report.updateTestLog("Validate <b>"+strElementDesc+"</b>
				// should present", "Element is not present", Status.FAIL);
				// i = iTryCount;
			}
		}
		if (bElement)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public boolean isElementPresent(WebElement element, String strElementDesc)
	{
		Boolean bElement = false;
		int iWaitSeconds = Allocator.timeout;
		try
		{
			bElement = isElementPresent2(element, strElementDesc, iWaitSeconds);
		}
		catch (InvocationTargetException e)
		{
			waitForSeconds(1);
			try
			{
				bElement = isElementPresent(element, strElementDesc, iWaitSeconds);///////////////////////////////////////////////////////////////////////////////////////////////////
			}
			catch (Exception e1)
			{
			}
		}
		catch (Exception e)
		{
			// e.printStackTrace();
		}
		return bElement;
	}

	/**
	 * Description: Check whether the given element is present or not in the
	 * Current page.
	 * 
	 * @param WebElement
	 *            element - Element present in the page
	 * @param String
	 *            strElementDesc - Description of the Element
	 * @param int
	 *            iWaitSeconds - wait time in seconds return Boolean - returns
	 *            True, if present else returns False
	 * @throws Exception
	 */
	public boolean isElementPresent(WebElement element, String strElementDesc, int iWaitSeconds)
	{
		Boolean bElement = false;
		try
		{
			bElement = isElementPresent2(element, strElementDesc, iWaitSeconds);
		}
		catch (InvocationTargetException e)
		{
			waitForSeconds(1);
			try
			{
				bElement = isElementPresent(element, strElementDesc, iWaitSeconds);///////////////////////////////////////////////////////////////////////////////////////////
			}
			catch (Exception e1)
			{
			}
		}
		catch (Exception e)
		{
			// e.printStackTrace();
		}
		return bElement;
	}

	public boolean isElementPresent2(WebElement element, String strElementDesc, int iWaitSeconds) throws InvocationTargetException, Exception
	{
		Boolean bElement = false;
		for (int i = 1; i <= iWaitSeconds; i++)
		{
			try
			{
				if (element.isDisplayed() || element.isEnabled())
				{
					bElement = true;
					break;
				}
				else
				{
					waitForSeconds(1); // waitFor 1 second
				}
			}
			catch (StaleElementReferenceException e)
			{
				// System.out.println( i + " - isElementPresent : " +
				// strElementDesc);
			}
			catch (NoSuchElementException e)
			{
				// System.out.println(i + " NoSuchElementException - WebElement
				// not present : " + strElementDesc);
				// i = iTryCount;
			}
			catch (Exception e)
			{
				// System.out.println(i+ " Exception - WebElement not present :
				// " + strElementDesc);
				// i = iTryCount;
			}
		}
		if (bElement)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public boolean isElementVisibleforToggle(By by, String strElementDesc)
	{
		Boolean bElement = false;
		int iWaitSeconds = Allocator.toogletimeout;
		try
		{
			bElement = isElementVisible2(by, strElementDesc, iWaitSeconds);
		}
		catch (InvocationTargetException e)
		{
			waitForSeconds(1);
			try
			{
				bElement = isElementVisible(by, strElementDesc, iWaitSeconds);///////////////////////////////////////////////////////////////////////////////////////
			}
			catch (Exception e1)
			{
			}
		}
		catch (Exception e)
		{
		}
		return bElement;
	}

	public boolean verifyElementVisible(By by, String strElementDesc) throws Exception
	{
		int iWaitSeconds = Allocator.toogletimeout;
		return verifyElementVisible(by, strElementDesc, iWaitSeconds);
	}

	public Boolean verifyElementVisible(By by, String strElementDesc, int iWaitSeconds) throws Exception
	{
		if (isElementVisible(by, strElementDesc, iWaitSeconds))
		{
			return true;
		}
		else
		{
			report.updateTestLog("verifyElementVisible", "Element: " + strElementDesc + " not Visible", Status.FAIL);
			System.out.println("verifyElementVisible - Element: " + strElementDesc + " not Visible");
			return false;
		}
	}

	public boolean isElementVisible(By by, String strElementDesc) throws Exception
	{
		int iWaitSeconds = Allocator.timeout;
		return isElementVisible(by, strElementDesc, iWaitSeconds);
	}

	/**
	 * Description: Check whether the given element is visible or not in the
	 * Current page.
	 * 
	 * @param By
	 *            byLocator - Locator of the Element in By Class reference
	 * @param String
	 *            strElementDesc - Description of the Element
	 * @param int
	 *            iWaitSeconds - wait time in seconds return Boolean - returns
	 *            True, if present else returns False
	 * @throws Exception
	 */
	public boolean isElementVisible(By by, String strElementDesc, int iWaitSeconds) throws Exception
	{
		Boolean bElement = false;
		try
		{
			bElement = isElementVisible2(by, strElementDesc, iWaitSeconds);
		}
		catch (InvocationTargetException e)
		{
			waitForSeconds(1);
			try
			{
				bElement = isElementVisible(by, strElementDesc, iWaitSeconds);
			}
			catch (Exception e1)
			{
			}
		}
		catch (Exception e)
		{
		}
		return bElement;
	}

	public boolean isElementVisible2(By by, String strElementDesc, int iWaitSeconds) throws InvocationTargetException, Exception
	{
		Boolean bElement = false;
		int iTryCount = 2;
		for (int i = 0; i < iTryCount; i++)
		{
			try
			{
				WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), iWaitSeconds);
				wait.until(ExpectedConditions.visibilityOfElementLocated(by));
				bElement = true;
				break;
			}
			catch (StaleElementReferenceException e)
			{
				System.out.println(i + " - waitTillElementExist : " + strElementDesc);
			}
			catch (NoSuchElementException e)
			{
				i = iTryCount;
			}
			catch (Exception e)
			{
				i = iTryCount;
			}
		}
		if (bElement)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public boolean isElementNotVisible(By by, String strElementDesc, int iWaitSeconds) throws InvocationTargetException, Exception
	{
		Boolean bElement = false;
		try
		{
			bElement = isElementNotVisible2(by, strElementDesc, iWaitSeconds);
		}
		catch (InvocationTargetException e)
		{
			waitForSeconds(3);
			try
			{
				bElement = isElementNotVisible(by, strElementDesc, iWaitSeconds);
			}
			catch (Exception e1)
			{
			}
		}
		catch (Exception e)
		{
		}
		return bElement;
	}

	public boolean isElementNotVisible2(By by, String strElementDesc, int iWaitSeconds) throws InvocationTargetException, Exception
	{
		Boolean bElement = false;
		int iTryCount = 2;
		for (int i = 0; i < iTryCount; i++)
		{
			try
			{
				WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), iWaitSeconds);
				wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
				bElement = true;
				break;
			}
			catch (StaleElementReferenceException e)
			{
				System.out.println(i + " - waitTillElement not Visible : " + strElementDesc);
			}
			catch (NoSuchElementException e)
			{
				System.out.println("NoSuchElementException - WebElement  not Present: " + strElementDesc);
				i = iTryCount;
			}
			catch (Exception e)
			{
				System.out.println("Exception - WebElement not Present: " + strElementDesc);
				i = iTryCount;
			}
		}
		if (bElement)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public boolean isElementClickable(By by, String strElementDesc)
	{
		int iWaitSeconds = Allocator.timeout;
		Boolean bElement = isElementClickable(by, strElementDesc, iWaitSeconds);
		return bElement;
	}

	public boolean isElementClickable(By by, String strElementDesc, int iWaitSeconds)
	{
		Boolean bElement = false;
		try
		{
			bElement = isElementClickable2(by, strElementDesc, iWaitSeconds);
		}
		catch (InvocationTargetException e)
		{
			waitForSeconds(3);
			try
			{
				bElement = isElementClickable(by, strElementDesc, iWaitSeconds);/////////////////////////////////////////////////////////////////////////
			}
			catch (Exception e1)
			{
			}
		}
		catch (Exception e)
		{
		}
		return bElement;
	}

	public boolean isElementClickable2(By by, String strElementDesc, int iWaitSeconds) throws InvocationTargetException, Exception
	{
		Boolean bElement = false;
		int iTryCount = 2;
		for (int i = 0; i < iTryCount; i++)
		{
			try
			{
				WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), iWaitSeconds);
				// wait.until(ExpectedConditions.visibilityOfElementLocated(by));
				wait.until(ExpectedConditions.elementToBeClickable(by));
				bElement = true;
				break;
			}
			catch (StaleElementReferenceException e)
			{
				System.out.println(i + " - waitTillElementExist : " + strElementDesc);
			}
			catch (NoSuchElementException e)
			{
				// System.out.println("NoSuchElementException - WebElement not
				// Visible : " + strElementDesc);
				i = iTryCount;
			}
			catch (Exception e)
			{
				// System.out.println("Exception - WebElement not Visible : " +
				// strElementDesc);
				i = iTryCount;
			}
		}
		if (bElement)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public boolean isElementClickable(WebElement element, String strElementDesc)
	{
		int iWaitSeconds = Allocator.timeout;
		Boolean bElement = isElementClickable(element, strElementDesc, iWaitSeconds);
		return bElement;
	}

	public boolean isElementClickable(WebElement element, String strElementDesc, int iWaitSeconds)
	{
		Boolean bElement = false;
		try
		{
			bElement = isElementClickable2(element, strElementDesc, iWaitSeconds);
		}
		catch (InvocationTargetException e)
		{
			waitForSeconds(3);
			try
			{
				bElement = isElementClickable(element, strElementDesc, iWaitSeconds);
			}
			catch (Exception e1)
			{
			}
		}
		catch (Exception e)
		{
		}
		return bElement;
	}

	public boolean isElementClickable2(WebElement element, String strElementDesc, int iWaitSeconds) throws InvocationTargetException, Exception
	{
		Boolean bElement = false;
		int iTryCount = 2;
		for (int i = 0; i < iTryCount; i++)
		{
			try
			{
				WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), iWaitSeconds);
				// wait.until(ExpectedConditions.visibilityOfElementLocated(by));
				wait.until(ExpectedConditions.elementToBeClickable(element));
				bElement = true;
				break;
			}
			catch (StaleElementReferenceException e)
			{
				System.out.println(i + " - waitTillElementExist : " + strElementDesc);
			}
			catch (NoSuchElementException e)
			{
				// System.out.println("NoSuchElementException - WebElement not
				// Visible : " + strElementDesc);
				i = iTryCount;
			}
			catch (Exception e)
			{
				// System.out.println("Exception - WebElement not Visible : " +
				// strElementDesc);
				i = iTryCount;
			}
		}
		if (bElement)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Description: method to validate the element present
	 */
	public boolean isElementPresent(SearchContext sc, By by)
	{
		boolean toReturn;
		try
		{
			if (sc.findElement(by).isDisplayed())
				toReturn = true;
			else
				toReturn = false;
		}
		catch (Exception e)
		{
			toReturn = false;
		}
		return toReturn;
	}

	public boolean isElementPresent(By by)
	{
		boolean toReturn;
		try
		{
			if (driver.getWebDriver().findElements(by).size() > 0)
				toReturn = true;
			else
				toReturn = false;
		}
		catch (Exception e)
		{
			toReturn = false;
		}
		return toReturn;
	}

	public boolean isElementVisible(WebElement element)
	{
		boolean toReturn = false;
		try
		{
			toReturn = isElementVisible2(element);
		}
		catch (InvocationTargetException e1)
		{
			toReturn = isElementVisible(element);
		}
		catch (Exception e1)
		{
		}
		return toReturn;
	}

	public boolean isElementVisible2(WebElement element) throws InvocationTargetException, Exception
	{
		boolean toReturn;
		try
		{
			if (element.isDisplayed())
				toReturn = true;
			else
				toReturn = false;
		}
		catch (Exception e)
		{
			toReturn = false;
		}
		return toReturn;
	}

	public void verifyPageSourceTextContains(WebDriver sc, String textValue)
	{
		try
		{
			if (driver.getPageSource().contains(textValue))
				report.updateTestLog("Validate <b>" + textValue + "</b> should present", textValue + " is present", Status.PASS);
			else
				report.updateTestLog("Validate <b>" + textValue + "</b> should present", textValue + " is not present", Status.FAIL);
		}
		catch (Exception e)
		{
			report.updateTestLog("Validate <b>" + textValue + "</b> should present", textValue + " is not present", Status.FAIL);
		}
		driver.manage().timeouts().implicitlyWait(Allocator.timeout, TimeUnit.SECONDS);
	}

	// Get Element Value
	public int getElementIntegerValue(By by, String strElementDesc) throws Exception
	{
		int Return = 0;
		if (isElementPresent(by, strElementDesc))
		{
			moveToElement(driver.findElement(by));
			String eleText = driver.findElement(by).getText();
			try
			{
				int iTest = Integer.parseInt(eleText);
				Return = iTest;
			}
			catch (NumberFormatException e)
			{
				Return = 0;
			}
		}
		else
		{
			report.updateTestLog("getElementIntegerValue", "The Element <b>" + strElementDesc + "</b> is not present", Status.FAIL);
		}
		return Return;
	}

	public String getElementValue(By by, String strElementDesc) throws Exception
	{
		String Return = null;
		if (isElementPresent(by, strElementDesc))
		{
			moveToElement(driver.findElement(by));
			String eleText = driver.findElement(by).getText().trim();
			Return = eleText;
		}
		else
		{
			report.updateTestLog("getElementValue", "The Element <b>" + strElementDesc + "</b> is not present", Status.FAIL);
		}
		return Return;
	}

	public void moveToElement(WebElement element)
	{
		try
		{
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
			Thread.sleep(10);
		}
		catch (Exception e)
		{
		}
	}

	public String getImageValue(By by, String strElementDesc) throws Exception
	{
		String Return = null;
		if (isElementPresent(by, strElementDesc))
		{
			moveToElement(driver.findElement(by));
			String src = driver.findElement(by).getAttribute("src");
			Return = src;
		}
		return Return;
	}

	public void clickifElementPresent(By by, String strElementDesc) throws Exception
	{
		boolean bElementPresent = isElementPresent(by, strElementDesc);
		if (bElementPresent)
		{
			try
			{
				driver.findElement(by).click();
				report.updateTestLog("Verify the button <b>" + strElementDesc + "</b> is clicked", "The button <b>" + strElementDesc + "</b> is clicked", Status.DONE);
			}
			catch (NoSuchElementException exc)
			{
				// report.updateTestLog("Verify the button <b>"+buttonName+"</b>
				// is clicked", "The button <b>"+buttonName+"</b> is not
				// present", Status.FAIL);
			}
			catch (WebDriverException wdexc)
			{
				JavascriptExecutor js = ((JavascriptExecutor) driver.getWebDriver());
				js.executeScript("arguments[0].click();", driver.findElement(by));
				report.updateTestLog("Verify the button <b>" + strElementDesc + "</b> is clicked", "The button <b>" + strElementDesc + "</b> is clicked", Status.DONE);
			}
			catch (Exception exc)
			{
				report.updateTestLog("Verify the button <b>" + strElementDesc + "</b> is clicked", "Unable to click the button <b>" + strElementDesc + "</b>", Status.FAIL);
				// frameworkparameters.setStopExecution(true);
			}
		}
	}

	/**
	 * 
	 * Description: method to validate the element present
	 *
	 *
	 * 
	 */
	public boolean isElementPresent(CraftDriver driver, By by)
	{
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		boolean toReturn;
		try
		{
			if (driver.findElement(by).isDisplayed())
				toReturn = true;
			else
				toReturn = false;
		}
		catch (Exception e)
		{
			toReturn = false;
		}
		driver.manage().timeouts().implicitlyWait(Allocator.timeout, TimeUnit.SECONDS);
		return toReturn;
	}

	/**
	 * 
	 * Description: method to validate the element present
	 *
	 *
	 * 
	 */
	public void isElementPresent(CraftDriver driver, By by, String element)
	{
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		try
		{
			if (driver.findElement(by).isDisplayed())
				report.updateTestLog("Validate <b>" + element + "</b> should present", "Element is present", Status.PASS);
			else
				report.updateTestLog("Validate <b>" + element + "</b> should present", "Element is not present", Status.FAIL);
		}
		catch (Exception e)
		{
			report.updateTestLog("Validate <b>" + element + "</b> should present", "Element is not present", Status.FAIL);
		}
		driver.manage().timeouts().implicitlyWait(Allocator.timeout, TimeUnit.SECONDS);
	}

	/**
	 * Description: Method to let element to be visbile Date : 17 Aug 2017
	 * Author :
	 */
	public boolean waitForVisible(CraftDriver driver, By by, int sec)
	{
		try
		{
			WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), sec);
			wait.until(ExpectedConditions.visibilityOfElementLocated(by));
			return true;
		}
		catch (Exception e)
		{
			return false;
		}
	}

	/**
	 * 
	 * Description: method to validate the element not present
	 */
	public boolean isElementNotPresent(CraftDriver driver, By by)
	{
		Boolean toReturn = false;
		try
		{
			driver.findElement(by);
		}
		catch (NoSuchElementException nsee)
		{
			toReturn = true;
		}
		return toReturn;
	}

	/**
	 * Component to display the Element as visible
	 * 
	 * @since Aug 18, 2017
	 * @author autotest
	 */
	public void makeBlock(WebElement element)
	{
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].style.display = 'block'", element);
	}

	/**
	 * Description: method to return the visible element
	 * 
	 * @throws Exception
	 *
	 *
	 * 
	 */
	public WebElement returnVisibleElement(List<WebElement> lstEle)
	{
		for (WebElement element : lstEle)
		{
			try
			{
				if (element.isDisplayed())
					return element;
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * Description: method to click on an button
	 * 
	 * @throws Exception
	 */
	public void clickButton(By by, String buttonName) throws Exception
	{
		Boolean bElementVisible = isElementVisible(by, buttonName);
		if (bElementVisible)
		{
			if (isElementClickable(by, buttonName))
			{
				highLightElement(by, buttonName);
				try
				{
					driver.findElement(by).click();
					report.updateTestLog("Verify the button <b>" + buttonName + "</b> is clicked", "The button <b>" + buttonName + "</b> is clicked", Status.PASS);
					System.out.println(buttonName + " is clicked");
				}
				catch (Exception exc)
				{
					report.updateTestLog("Verify the button <b>" + buttonName + "</b> is clicked", "Unable to click the button <b>" + buttonName + "</b>", Status.FAIL);
				}
			}
			else
			{
				report.updateTestLog("Verify the button <b>" + buttonName + "</b> is clicked", "<b>" + buttonName + " is not clickable." + "</b>", Status.FAIL);
				System.out.println(buttonName + " is not Clickable.");
			}
		}
		else
		{
			report.updateTestLog("Verify the button <b>" + buttonName + "</b> is clicked", "The button <b>" + buttonName + "</b> is not present", Status.FAIL);
			System.out.println(buttonName + " is not present");
		}
	}

	/**
	 * Description: method to click on a link
	 * 
	 * @throws Exception
	 *
	 *
	 * 
	 */
	public void clickLink(By by, String linkName) throws Exception
	{
		Boolean bElementVisible = isElementVisible(by, linkName);
		if (bElementVisible)
		{
			if (isElementClickable(by, linkName))
			{
				highLightElement(by, linkName);
				try
				{
					driver.findElement(by).click();
					report.updateTestLog("Verify the Link <b>" + linkName + "</b> is clicked", "The Link <b>" + linkName + "</b> is clicked", Status.PASS);
					System.out.println(linkName + " is clicked");
				}
				catch (Exception exc)
				{
					report.updateTestLog("Verify the Link <b>" + linkName + "</b> is clicked", "Unable to click the Link <b>" + linkName + "</b>", Status.FAIL);
				}
			}
			else
			{
				report.updateTestLog("Verify the Link <b>" + linkName + "</b> is clicked", "<b>" + linkName + " is not clickable." + "</b>", Status.FAIL);
				System.out.println(linkName + " is not Clickable.");
				// frameworkparameters.setStopExecution(true);
			}
		}
		else
		{
			report.updateTestLog("Verify the Link " + linkName + " is clicked", "The Link " + linkName + " is not present", Status.FAIL);
			// frameworkparameters.setStopExecution(true);
		}
	}

	public void clickElement(By by, String strElementDesc) throws Exception
	{
		Boolean b = isElementVisible(by, strElementDesc);
		if (b)
		{
			highLightElement(by, strElementDesc);
			try
			{
				driver.findElement(by).click();
				System.out.println("Element Clicked: " + strElementDesc);
				report.updateTestLog("clickElement", "Element Clicked: <b>" + strElementDesc + "</b>", Status.PASS);
				driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
				driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			}
			catch (Exception exc)
			{
				report.updateTestLog("Verify the button <b>" + strElementDesc + "</b> is clicked", "Unable to click the button <b>" + strElementDesc + "</b>", Status.FAIL);
			}
		}
		else
		{
			report.updateTestLog("clickElement", "Element not Present: <b>" + strElementDesc + "</b>", Status.FAIL);
			System.out.println("The Element: " + strElementDesc + " is not present and so element not clicked");
		}
	}

	/**
	 * Description: Click an Element present in the application page.
	 * 
	 * @param WebElement
	 *            element - Element present in the page
	 * @param String
	 *            strElementDesc - Description of the Element
	 * @throws Exception
	 */
	public void clickElement(WebElement element, String strElementDesc) throws Exception
	{
		Boolean b = isElementPresent(element, strElementDesc);
		// System.out.println("Element present: " + b);
		if (b)
		{
			highLightElement(element, strElementDesc);
			// System.out.println("Element to Click2: " + strElementDesc);
			try
			{
				element.click();
				System.out.println("Element Clicked: " + strElementDesc);
				report.updateTestLog("clickElement", "Element Clicked: <b>" + strElementDesc + "</b>", Status.DONE);
			}
			catch (Exception exc)
			{
				report.updateTestLog("Verify the button <b>" + strElementDesc + "</b> is clicked", "Unable to click the button <b>" + strElementDesc + "</b>", Status.FAIL);
			}
		}
		else
		{
			report.updateTestLog("clickElement", "Element not Present: <b>" + strElementDesc + "</b>", Status.FAIL);
			System.out.println("The Element: " + strElementDesc + " is not present and so element not clicked");
		}
	}

	// changed by kusuma on Aug 2nd
	public void clickByJSE(By by, String strElementDesc) throws Exception
	{
		Boolean bElementPresent = isElementPresent(by, strElementDesc);
		if (bElementPresent)
		{
			highLightElement(by, strElementDesc);
			JavascriptExecutor js = ((JavascriptExecutor) driver.getWebDriver());
			js.executeScript("arguments[0].click();", driver.findElement(by));
			report.updateTestLog("Verify the Element <b>" + strElementDesc + "</b> is clicked", "The Element <b>" + strElementDesc + "</b> is clicked", Status.PASS);
			System.out.println("The Element: " + strElementDesc + " is clicked");
		}
		else
		{
			report.updateTestLog("Verify the Element <b>" + strElementDesc + "</b> is clicked", "The Element <b>" + strElementDesc + "</b> is not present", Status.FAIL);
			System.out.println("The Element: " + strElementDesc + " is not present and so element not clicked");
		}
	}

	public void clickByJSEShadowRoot(By by, String strElementDesc) throws Exception
	{
		Boolean bElementPresent = isElementPresent(by, strElementDesc);
		if (bElementPresent)
		{
			highLightElement(by, strElementDesc);
			JavascriptExecutor js = ((JavascriptExecutor) driver.getWebDriver());
			js.executeScript("return arguments[0].shadowRoot", driver.findElement(by));
			report.updateTestLog("Verify the Element <b>" + strElementDesc + "</b> is clicked", "The Element <b>" + strElementDesc + "</b> is clicked", Status.PASS);
			System.out.println("The Element: " + strElementDesc + " is clicked");
		}
		else
		{
			report.updateTestLog("Verify the Element <b>" + strElementDesc + "</b> is clicked", "The Element <b>" + strElementDesc + "</b> is not present", Status.FAIL);
			System.out.println("The Element: " + strElementDesc + " is not present and so element not clicked");
		}
	}

	public void enterTextByJSE(By by, String text, String strElementDesc) throws Exception
	{
		System.out.println("TEXT " + text);
		Boolean bElementPresent = isElementPresent(by, strElementDesc);
		if (bElementPresent)
		{
			highLightElement(by, strElementDesc);
			JavascriptExecutor js = ((JavascriptExecutor) driver.getWebDriver());
			js.executeScript("arguments[0].value='" + text + "';", driver.findElement(by));
			report.updateTestLog("Verify the Element <b>" + strElementDesc + "</b> is clicked", "The Element <b>" + strElementDesc + "</b> is clicked", Status.DONE);
			System.out.println("The Element: " + strElementDesc + " is clicked");
		}
		else
		{
			report.updateTestLog("Verify the Element <b>" + strElementDesc + "</b> is clicked", "The Element <b>" + strElementDesc + "</b> is not present", Status.FAIL);
			System.out.println("The Element: " + strElementDesc + " is not present and so element not clicked");
		}
	}

	public void selectOptionByJSE(By by, String option, String strElementDesc) throws Exception
	{

		Boolean bElementPresent = isElementPresent(by, strElementDesc);
		if (bElementPresent)
		{
			// highLightElement(by,strElementDesc);
			System.out.println("inside jse options" + option);
			JavascriptExecutor js = ((JavascriptExecutor) driver.getWebDriver());
			js.executeScript("arguments[0].value='" + option + "';", driver.findElement(by));
			report.updateTestLog("Verify the Element <b>" + strElementDesc + "</b> is clicked", "The Element <b>" + strElementDesc + "</b> is clicked", Status.DONE);
			System.out.println("The Element: " + strElementDesc + " is clicked");
		}
		else
		{
			report.updateTestLog("Verify the Element <b>" + strElementDesc + "</b> is clicked", "The Element <b>" + strElementDesc + "</b> is not present", Status.FAIL);
			System.out.println("The Element: " + strElementDesc + " is not present and so element not clicked");
		}
	}

	public void doubleClickByJSE(By by, String strElementDesc) throws Exception
	{
		Boolean bElementPresent = isElementPresent(by, strElementDesc);
		if (bElementPresent)
		{
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].doubleClick();", driver.findElement(by));
			report.updateTestLog("Verify the Element <b>" + strElementDesc + "</b> is clicked", "The Element <b>" + strElementDesc + "</b> is clicked", Status.DONE);
			System.out.println("The Element: " + strElementDesc + " is double clicked");
		}
		else
		{
			report.updateTestLog("Verify the Element <b>" + strElementDesc + "</b> is clicked", "The Element <b>" + strElementDesc + "</b> is not present", Status.FAIL);
		}
	}

	// Changed By Kusuma on Aug 2nd
	public void clickSFLinkLet(String strLinkletText) throws Exception
	{
		// div[@class='listHoverLinks']/a[@class='linklet']/span[text()='Cases']
		strLinkletText = strLinkletText.trim();
		By byLinklets = By.xpath("//div[@class='listHoverLinks']/a");
		if (isElementPresent(byLinklets, "Linklets"))
		{
			// List<WebElement> eLinklets = driver.findElements(byLinklets);
			By byLinkLet = By.xpath("//div[@class='listHoverLinks']//span[starts-with(text(),'" + strLinkletText + "')]");
			String strAppLinkLetTxt = getData(byLinkLet, "LinkLet Title", "text");
			if (strAppLinkLetTxt.toLowerCase().startsWith(strLinkletText.toLowerCase()))
			{
				if (strAppLinkLetTxt.contains("["))
				{
					int indexOfOpenSquareBracket = strAppLinkLetTxt.indexOf("[");
					strAppLinkLetTxt = strAppLinkLetTxt.substring(0, indexOfOpenSquareBracket).trim();
					if (strAppLinkLetTxt.equalsIgnoreCase(strLinkletText))
					{
						highLightElement(byLinkLet, "LinkLet Title: " + strLinkletText);
						clickByJSE(byLinkLet, "LinkLet Title: " + strLinkletText);
					}
				}
			}
		}
	}

	public By isSFLinkLetPresent(String strLinkletText) throws Exception
	{
		// div[@class='listHoverLinks']/a[@class='linklet']/span[text()='Cases']
		strLinkletText = strLinkletText.trim();
		By byLinkLet = null;
		By byLinklets = By.xpath("//div[@class='listHoverLinks']/a");
		if (isElementPresent(byLinklets, "Linklets"))
		{
			// List<WebElement> eLinklets = driver.findElements(byLinklets);
			By byLinkLetTmp = By.xpath("//div[@class='listHoverLinks']//span[starts-with(text(),'" + strLinkletText + "')]");
			if (isElementVisible(byLinkLetTmp, "Linklet - " + strLinkletText, 10))
			{
				String strAppLinkLetTxt = getData(byLinkLetTmp, "LinkLet Title", "text");
				if (strAppLinkLetTxt.toLowerCase().startsWith(strLinkletText.toLowerCase()))
				{
					if (strAppLinkLetTxt.contains("["))
					{
						int indexOfOpenSquareBracket = strAppLinkLetTxt.indexOf("[");
						strAppLinkLetTxt = strAppLinkLetTxt.substring(0, indexOfOpenSquareBracket).trim();
						if (strAppLinkLetTxt.equalsIgnoreCase(strLinkletText))
						{
							byLinkLet = byLinkLetTmp;
						}
					}
				}
			}
		}
		return byLinkLet;
	}

	public int getSFLinkLetRecordCount(String strLinkletText) throws Exception
	{
		strLinkletText = strLinkletText.trim();
		Boolean bLinkLetFound = false;
		int iLinkLetRecordCount = -1;
		By byLinklets = By.xpath("//div[@class='listHoverLinks']/a");
		if (isElementVisible(byLinklets, "Linklets"))
		{
			// List<WebElement> eLinklets = driver.findElements(byLinklets);
			int iLinkletsCount = driver.findElements(byLinklets).size();
			for (int i = 1; i <= iLinkletsCount; i++)
			{
				By byLinkLet = By.xpath("//div[@class='listHoverLinks']/a[" + i + "]/span[@class='listTitle']");
				String strAppLinkLetTxt = getData(byLinkLet, "LinkLet Title", "text");
				if (strAppLinkLetTxt.toLowerCase().startsWith(strLinkletText.toLowerCase()))
				{
					bLinkLetFound = true;
					highLightElement(byLinkLet, "LinkLet Title: " + strLinkletText);
					By byLinkLetRecordCount = By.xpath("//div[@class='listHoverLinks']/a[" + i + "]/span[@class='listTitle']/span[@class='count']");
					String strRecordCount = getData(byLinkLetRecordCount, strAppLinkLetTxt + " - Record Count", "text");
					strRecordCount = strRecordCount.replace("[", "").replace("]", "").replace("+", "").trim();
					try
					{
						iLinkLetRecordCount = Integer.parseInt(strRecordCount);
					}
					catch (Exception e)
					{
					}
					break;
				}
			}
			if (bLinkLetFound)
			{
				if (iLinkLetRecordCount == -1)
				{
					report.updateTestLog("getSFLinkLetRecordCount", "Error in getting Record Count from Linklet: " + strLinkletText, Status.FAIL);
					System.out.println("getSFLinkLetRecordCount - Error in getting Record Count from Linklet: " + strLinkletText);
				}
			}
			else
			{
				report.updateTestLog("Click Linklet", "Linklet: " + strLinkletText + " not present", Status.FAIL);
				System.out.println("The Element: " + strLinkletText + " is not present and so element not clicked");
			} // bLinkLetFound
		}
		else
		{
			report.updateTestLog("Click Linklet", "Linklet group is not present", Status.FAIL);
		}
		return iLinkLetRecordCount;
	}

	public Boolean isSFLinkLetRecordCountMore(String strLinkletText) throws Exception
	{
		Boolean bMore = false;
		By byLinkLetReocrd = By.xpath("//div[@class='listHoverLinks']/a/span[@class='listTitle'][contains(text(),'" + strLinkletText + "')]/span");
		if (verifyElementVisible(byLinkLetReocrd, "LinkLet: " + strLinkletText))
		{
			String strLinkLetRecordCount = getData(byLinkLetReocrd, "LinkLet: " + strLinkletText, "text");
			if (strLinkLetRecordCount.contains("+"))
			{
				bMore = true;
			}
		}
		return bMore;
	}

	/**
	 * Description: method to set the value in edit field
	 * 
	 * @throws Exception
	 */
	public void setData(By by, String fieldName, String fieldValue) throws Exception
	{
		Boolean bElementPresent = isElementPresent(by, fieldName);
		if (bElementPresent)
		{
			highLightElement(by, fieldName);
			try
			{
				driver.findElement(by).clear();
				driver.findElement(by).sendKeys(Keys.chord(Keys.CONTROL, "a"), fieldValue);
				report.updateTestLog("Verify the data is populated in the edit Box", "The data <b>" + fieldValue + "</b> is successfully populated in the EditBox <b>" + fieldName, Status.PASS);
				System.out.println("The data " + fieldValue + " is successfully populated in the EditBox " + fieldName);
				driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
				driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			}
			catch (Exception e)
			{
				report.updateTestLog("Verify the data is populated in the edit Box", "The data <b>" + fieldValue + "</b> is not populated in the EditBox", Status.FAIL);
				e.printStackTrace();
			}
		}
		else
		{
			report.updateTestLog("Verify the Text Box <b>" + fieldName + "</b> is present", "The Text Box <b>" + fieldName + "</b> is not present", Status.FAIL);
			System.out.println("The Text Box " + fieldName + " is not present");
		}
	}

	public void keyInEditBox(By by, String fieldName, Keys keyData) throws Exception
	{
		Boolean bElementPresent = isElementPresent(by, fieldName);
		if (bElementPresent)
		{
			// highLightElement(by,fieldName);
			try
			{
				// driver.findElement(by).clear();
				driver.findElement(by).sendKeys(keyData);
				// report.updateTestLog("Verify the data is populated in the
				// edit Box", "The data <b>"+fieldValue+"</b> is successfully
				// populated in the EditBox <b>"+fieldName, Status.DONE);
				// System.out.println("The data "+fieldValue+" is successfully
				// populated in the EditBox "+fieldName);
				// TimeUnit.SECONDS.sleep(6);
			}
			catch (Exception e)
			{
				// report.updateTestLog("Verify the data is populated in the
				// edit Box", "The data <b>"+fieldValue+"</b> is not populated
				// in the EditBox", Status.FAIL);
				// e.printStackTrace();
			}
		}
		else
		{
			report.updateTestLog("Verify the Text Box <b>" + fieldName + "</b> is present", "The Text Box <b>" + fieldName + "</b> is not present", Status.FAIL);
			// System.out.println("The Text Box "+fieldName+" is not present");
		}
		// }
	}

	/**
	 * Description: Select an option present from the drop down list of an
	 * Element.
	 * 
	 * @param By
	 *            byLocator - Locator of the Element in By Class format
	 * @param String
	 *            strElementDesc - Description of the Element
	 * @param String
	 *            strInoutData - Option to be selected.
	 * @throws Exception
	 */
	public void selectData(By by, String strElementDesc, String strInoutData) throws Exception
	{
		Boolean bOptionSelected = false;
		String strOption = null;
		if (isElementVisible(by, strElementDesc))
		{
			highLightElement(by, strElementDesc);
			try
			{
				Select dropdown = new Select(driver.findElement(by));
				strOption = dropdown.getFirstSelectedOption().getText().trim();
				strInoutData = strInoutData.trim();
				if (strOption.equalsIgnoreCase(strInoutData))
				{
					bOptionSelected = true;
				}
				else
				{
					List<WebElement> eOptions = dropdown.getOptions();
					int iOptionCount = dropdown.getOptions().size();
					for (int i = 0; i < iOptionCount; i++)
					{
						strOption = eOptions.get(i).getText();
						if (strOption.trim().equalsIgnoreCase(strInoutData))
						{
							dropdown.selectByVisibleText(strOption);
							bOptionSelected = true;
							break;
						}
					}
				}
				if (bOptionSelected)
				{
					System.out.println("select Data " + "Option: (" + strOption + ") - selected from the Drop Down List: " + strElementDesc);
					report.updateTestLog("select Data", "Option: (<b>" + strOption + "</b>) - selected from the Drop Down List: <b>" + strElementDesc + "</b>", Status.PASS);
				}
				else
				{
					System.out.println("select Data " + "Option: (" + strInoutData + ") - not present in the Drop Down List: " + strElementDesc);
					report.updateTestLog("select Data", "Option: (<b>" + strInoutData + "</b>) - not present in the Drop Down List: <b>" + strElementDesc + "</b>", Status.DONE);
				}

				waitForSeconds(2);
			}
			catch (Exception e)
			{
				System.out.println("select Data " + "Option: (" + strInoutData + ") - not available in the Drop Down List: " + strElementDesc);
				report.updateTestLog("select Data", "Option: (<b>" + strInoutData + "</b>) - not available in the Drop Down List: " + strElementDesc, Status.FAIL);
			}
		}
		else
		{
			report.updateTestLog("Select Data", "<b>Drop Down Element not Visible: " + strElementDesc + "</b>", Status.FAIL);
			System.out.println("select Data " + "- Drop Down Element not Visible: " + strElementDesc);
		}
	}

	/**
	 * Description: Select multiple options present from the drop down list of
	 * an Element.
	 * 
	 * @param By
	 *            byLocator - Locator of the Element in By Class format
	 * @param String
	 *            strElementDesc - Description of the Element
	 * @param String
	 *            strInoutData - Options to be selected seperated by semicolon :
	 *            ';'
	 * @throws Exception
	 */
	public void multiSelectData(By by, String strElementDesc, String strInoutData) throws Exception
	{
		String strOption = null;
		int i = 0;
		if (isElementVisible(by, strElementDesc))
		{
			highLightElement(by, strElementDesc);
			try
			{
				Select dropdown = new Select(driver.findElement(by));
				List<WebElement> dropdownoptions = dropdown.getOptions();
				dropdown.deselectAll();
				ArrayList<String> listInputSelection = new ArrayList<String>(Arrays.asList(strInoutData.split(";")));
				for (String strInput : listInputSelection)
				{
					i = 0;
					for (WebElement optionToSelect : dropdownoptions)
					{
						i++;
						String strOptionToSelect = optionToSelect.getText();
						if (strOptionToSelect.trim().equalsIgnoreCase(strInput.trim()))
						{
							dropdown.selectByVisibleText(strOption);
							System.out.println("Multi-select Data " + "Option: (" + strOption + ") - selected from the Drop Down List: " + strElementDesc);
							report.updateTestLog("Multi-select Data", "Option: (<b>" + strOption + "</b>) - selected from the Drop Down List: <b>" + strElementDesc + "</b>", Status.DONE);
							break;
						}
						else if (i == dropdownoptions.size() - 1)
						{
							System.out.println("Multi-select Data" + "Option: (" + strInoutData + ") - not present in the Drop Down List: " + strElementDesc);
							report.updateTestLog("Multi-select Data", "Option: (<b>" + strInoutData + "</b>) - not present in the Drop Down List: <b>" + strElementDesc + "</b>", Status.DONE);
						}
					}
				}
			}
			catch (Exception e)
			{
				System.out.println("Multi-select Data" + "Option: (" + strInoutData + ") - not available in the Drop Down List: " + strElementDesc);
				report.updateTestLog("Multi-select Data", "Option: (<b>" + strInoutData + "</b>) - not available in the Drop Down List: " + strElementDesc, Status.FAIL);
			}
		}
		else
		{
			report.updateTestLog("Multi-select Data", "<b>Drop Down Element not Visible: " + strElementDesc + "</b>", Status.FAIL);
			System.out.println("Multi-select Data" + "- Drop Down Element not Visible: " + strElementDesc);
		}
	}

	public void verifyElementTextContains(By by, String strEleValue, String verifyDesc, String passDesc, String failDesc) throws Exception
	{
		if (isElementPresent(by, "ElementDesc"))
		{
			String strEleText = driver.findElement(by).getText().trim();
			if (strEleText != null && strEleText.contains(strEleValue))
				report.updateTestLog(verifyDesc, passDesc, Status.PASS);
			else
				report.updateTestLog(verifyDesc, failDesc, Status.FAIL);
		}
	}

	/**
	 * Description: method to equals the given text with element text
	 * 
	 * @throws Exception
	 *
	 *
	 * 
	 */
	public void verifyElementTextEquals(By by, String strEleValue, String strEleName, String strPage) throws Exception
	{
		if (isElementPresent(by, strEleName))
		{
			String strEleText = driver.findElement(by).getText().trim();
			if (strEleText != null && strEleText.equals(strEleValue))
				report.updateTestLog("Verify <b>" + strEleName + "</b> element present in page <b>" + strPage + "</b>", "Element present in page <b>" + strPage + "</b>", Status.PASS);
			else
				report.updateTestLog("Verify <b>" + strEleName + "</b> element present in page <b>" + strPage + "</b>", "Element not present in page <b>" + strPage + "</b>", Status.FAIL);
		}
	}

	/**
	 * Description: method to click on a option
	 *
	 *
	 * 
	 */
	public void clickOption(SearchContext sc, By by, String lnkName, String strPageName, String strOptionName)
	{
		boolean blnFlag = false;
		try
		{
			// if(isElementPresent(sc, by)){
			sc.findElement(by).click();
			blnFlag = true;
			// }
		}
		catch (WebDriverException wdexc)
		{
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].click();", sc.findElement(by));
			blnFlag = true;
		}
		catch (Exception exc)
		{
			blnFlag = false;
		}
		if (blnFlag)
			report.updateTestLog("Click the <b>" + lnkName + "</b> option from " + strPageName, "<b>" + strOptionName + ":" + lnkName + "</b> option is clicked successfully", Status.PASS);
		else
			report.updateTestLog("Click the <b>" + lnkName + "</b> option from " + strPageName, "<b>" + strOptionName + ":" + lnkName + "</b> option is not clicked successfully", Status.FAIL);
	}

	/**
	 * Description: method to create unique number using timestamp
	 */
	public String createUnique()
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		String strUnique = dateFormat.format(date).replaceAll("[.,:\\-// ]", "");
		return strUnique;
	}

	public boolean checkImage(By by, String expectedValue)
	{
		boolean value = false;
		try
		{
			moveToElement(driver.findElement(by));
			String imageValue = driver.findElement(by).getAttribute("src");
			if (imageValue.contains(expectedValue))
			{
				value = true;
			}
			else
			{
				value = false;
			}
		}
		catch (Exception e)
		{
			value = false;
		}
		return value;
	}

	/* Method to check and provide results on the element is present */
	public void checkandConfirmElementavailability(By by, String verifyDesc, String passDesc, String failDesc, String warningDesc)
	{
		try
		{
			if (isElementNotPresent(driver, by))
			{
				report.updateTestLog(verifyDesc, passDesc, Status.PASS);
			}
			else
			{
				report.updateTestLog(verifyDesc, failDesc, Status.FAIL);
			}
		}
		catch (Exception e)
		{
			report.updateTestLog(verifyDesc, warningDesc, Status.WARNING);
		}
	}

	public boolean waitForJStoLoad(CraftDriver driver, int waitSec)
	{
		WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), waitSec);
		// wait for jQuery to load
		ExpectedCondition<Boolean> jQueryLoad = new ExpectedCondition<Boolean>()
		{
			@SuppressWarnings("unused")
			public Boolean apply(CraftDriver driver)
			{
				try
				{
					JavascriptExecutor js = (JavascriptExecutor) driver;
					return ((Long) js.executeScript("return jQuery.active") == 0);
				}
				catch (Exception e)
				{
					return true;
				}
			}

			@Override
			public Boolean apply(WebDriver arg0)
			{
				return null;
			}
		};
		// wait for Javascript to load
		ExpectedCondition<Boolean> jsLoad = new ExpectedCondition<Boolean>()
		{
			@SuppressWarnings("unused")
			public Boolean apply(CraftDriver driver)
			{
				JavascriptExecutor js = (JavascriptExecutor) driver;
				return js.executeScript("return document.readyState").toString().equals("complete");
			}

			@Override
			public Boolean apply(WebDriver arg0)
			{
				return null;
			}
		};
		return wait.until(jQueryLoad) && wait.until(jsLoad);
	}

	/**
	 * Description: method to verify text in the element Date : 15 Aug 2017
	 * 
	 */
	public void verifyTextContains(SearchContext con, By ui, String strCategory, String element)
	{
		if (isElementPresent(con, ui))
		{
			if (con.findElement(ui).getText().trim().toLowerCase().contains(strCategory.toLowerCase()))
				report.updateTestLog("Validate " + strCategory + " should present in " + element, "Mentioned " + element + " is present", Status.PASS);
			else
				report.updateTestLog("Validate " + strCategory + " should present in " + element, "Mentioned " + element + " is not present", Status.FAIL);
		}
		else
			report.updateTestLog("Validate " + strCategory + " should present in " + element, "Mentioned " + element + " is not present", Status.FAIL);
	}

	public void isNewRowAddedtoWebTable(By tableTag, int tableID, By rowTag, By thcolumnTag, By tdcolumnTag, String valuetoConfirm, String verifyDesc, String passDesc, String warningDesc) throws InterruptedException
	{
		try
		{
			List<WebElement> findTables = driver.findElements(tableTag);
			WebElement baseTable = findTables.get(tableID);
			moveToElement(baseTable);
			List<WebElement> rows = baseTable.findElements(rowTag);
			try
			{
				for (int row = 0; row < rows.size(); row++)
				{
					try
					{
						List<WebElement> columns = rows.get(row).findElements(thcolumnTag);
						for (int column = 0; column < columns.size(); column++)
						{
							String Return = columns.get(column).getText();
							if (Return.contains(valuetoConfirm))
							{
								report.updateTestLog(verifyDesc, passDesc, Status.PASS);
								break;
							}
						}
					}
					catch (Exception e)
					{
					}
					try
					{
						List<WebElement> columns = rows.get(row).findElements(tdcolumnTag);
						for (int column = 0; column < columns.size(); column++)
						{
							String Return = columns.get(column).getText();
							if (Return.contains(valuetoConfirm))
							{
								report.updateTestLog(verifyDesc, passDesc, Status.PASS);
								break;
							}
						}
					}
					catch (Exception e)
					{
					}
				}
			}
			catch (Exception e)
			{
				report.updateTestLog(verifyDesc, warningDesc, Status.WARNING);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void clickDatafromWebTable(By tableTag, int tableID, By rowTag, By tdcolumnTag, By thcolumnTag, String searchValue, By by, String Desc, String expectedResults)
	{
		try
		{
			List<WebElement> findTables = driver.findElements(tableTag);
			WebElement baseTable = findTables.get(tableID);
			List<WebElement> rows = baseTable.findElements(rowTag);
			try
			{
				for (int row = 0; row < rows.size(); row++)
				{
					try
					{
						List<WebElement> columns = rows.get(row).findElements(tdcolumnTag);
						for (int column = 0; column < columns.size(); column++)
						{
							String Return = columns.get(column).getText();
							if (Return.contains(searchValue))
							{
								// driver.findElement(by).click();
								highLightElement(by, searchValue);
								clickByJSE(by, searchValue);
								waitForSeconds(10);
								// TimeUnit.SECONDS.sleep(10);
								report.updateTestLog(Desc, expectedResults, Status.PASS);
								break;
							}
						}
					}
					catch (Exception e)
					{
					}
					try
					{
						List<WebElement> columns = rows.get(row).findElements(thcolumnTag);
						for (int column = 0; column < columns.size(); column++)
						{
							String Return = columns.get(column).getText();
							if (Return.contains(searchValue))
							{
								clickByJSE(by, searchValue);
								waitForSeconds(10);
								report.updateTestLog(Desc, expectedResults, Status.PASS);
								break;
							}
						}
					}
					catch (Exception e)
					{
					}
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public String pickSpecificDatafromWebTable(By tableTag, int tableID, By rowTag, int rowNum, int columnNum, By columnTag, String Desc, String expectedResults)
	{
		String returnValue = null;
		try
		{
			List<WebElement> findTables = driver.findElements(tableTag);
			WebElement baseTable = findTables.get(tableID);
			List<WebElement> rows = baseTable.findElements(rowTag);
			List<WebElement> columns = rows.get(rowNum).findElements(columnTag);
			for (int i = columnNum; i <= columns.size();)
			{
				String Return = columns.get(i).getText();
				returnValue = Return;
				report.updateTestLog(Desc, expectedResults, Status.PASS);
				break;
			}
		}
		catch (Exception e)
		{
		}
		return returnValue;
	}

	public String pickDatafromWebTable(By tableTag, int tableID, By rowTag, By tdcolumnTag, String Desc, String expectedResults)
	{
		String returnValue = null;
		try
		{
			List<WebElement> findTables = driver.findElements(tableTag);
			WebElement baseTable = findTables.get(tableID);
			try
			{
				if (baseTable.getText().trim().contains("No records to display"))
				{
					report.updateTestLog("Verify Table is present", "Table is not present", Status.FAIL);
				}
				else
				{
					List<WebElement> rows = baseTable.findElements(rowTag);
					try
					{
						for (int row = 0; row < rows.size(); row++)
						{
							List<WebElement> columns = rows.get(row).findElements(tdcolumnTag);
							for (int column = 0; column < columns.size();)
							{
								String Return = columns.get(column).getText();
								returnValue = Return;
								report.updateTestLog(Desc, expectedResults, Status.PASS);
								break;
							}
						}
					}
					catch (Exception e)
					{
					}
				}
			}
			catch (Exception e)
			{
			}
		}
		catch (Exception e)
		{
		}
		return returnValue;
	}

	public void confirmStatus(By by, String stepName, String passDesc, String failDesc, String warningDesc)
	{
		try
		{
			if (isElementPresent(driver, by))
				report.updateTestLog(stepName, passDesc, Status.PASS);
			else
				report.updateTestLog(stepName, failDesc, Status.FAIL);
		}
		catch (Exception e)
		{
			report.updateTestLog(stepName, warningDesc, Status.WARNING);
		}
	}

	public String isAlertPresent()
	{
		// Boolean bAlertPresent = false;
		String strAlertMsg = null;
		try
		{
			WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), 7);
			wait.until(ExpectedConditions.alertIsPresent());
			strAlertMsg = driver.switchTo().alert().getText();
			// driver.switchTo().defaultContent();
			// bAlertPresent = true;
		}
		catch (Exception e1)
		{
			e1.printStackTrace();
		}
		if (strAlertMsg != null)
		{
			report.updateTestLog("isAlertPresent", "Alert is Present. Alert Message is " + strAlertMsg, Status.DONE);
		}
		else
		{
			/// no codes nere
		}
		return strAlertMsg;
	}

	public void handleAlert()
	{
		String strAlertMsg = isAlertPresent();
		if (strAlertMsg != null)
		{
			try
			{
				// String strAlertMsg = driver.switchTo().alert().getText();
				driver.switchTo().alert().accept();
				TimeUnit.SECONDS.sleep(20);
				driver.switchTo().defaultContent();
				System.out.println("AlertMsg: " + strAlertMsg);
			}
			catch (Throwable e)
			{
				System.err.println("Error came while waiting for the alert popup. " + e.getMessage());
			}
		}
		else
		{
			report.updateTestLog("isAlertPresent", "Alert is not Present", Status.FAIL);
		}
	}

	public void acceptAlert()
	{
		WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), 10);
		wait.until(ExpectedConditions.alertIsPresent());
		try
		{
			String strAlertMsg = driver.switchTo().alert().getText();
			driver.switchTo().alert().accept();
			TimeUnit.SECONDS.sleep(20);
			driver.switchTo().defaultContent();
			System.out.println("AlertMsg: " + strAlertMsg);
			System.out.println("Accepted the alert successfully.");
		}
		catch (Throwable e)
		{
			System.err.println("Error came while waiting for the alert popup. " + e.getMessage());
		}
	}

	public int checkforLateStatusinWebTable(int tableID) throws InterruptedException
	{
		int value = 0;
		try
		{
			List<WebElement> findTables = driver.findElements(By.xpath("//*[starts-with(@id, 'a0C')]/table"));
			WebElement baseTable = findTables.get(tableID);
			moveToElement(baseTable);
			System.out.println(baseTable.getText());
			try
			{
				if (baseTable.getText().trim().contains("No records to display"))
				{
					report.updateTestLog("Verify Table is present", "Table is not present", Status.FAIL);
					value = -1;
				}
				else
				{
					List<WebElement> rows = baseTable.findElements(By.tagName("tr"));
					try
					{
						for (int row = 0; row < rows.size(); row++)
						{
							List<WebElement> columns = rows.get(row).findElements(By.tagName("td"));
							for (int column = 0; column < columns.size(); column++)
							{
								int returnStatus = Integer.parseInt(columns.get(column).getText());
								if (returnStatus > 1)
								{
									value = value + 1;
									continue;
								}
								continue;
							}
						}
					}
					catch (Exception e)
					{
					}
				}
			}
			catch (Exception e)
			{
			}
		}
		catch (Exception e)
		{
		}
		return value;
	}

	public void selectSliderOption(By by, String fieldName, String fieldValue) throws Exception
	{
		if (isElementVisible(by, fieldName))
		{
			WebElement slider = driver.findElement(by);
			int sliderwidth = driver.findElement(by).getSize().getWidth();
			System.out.print(sliderwidth);
			scrollElementToMiddlePage(by, "Slider");
			// timeLapse(2);
			int Ans1 = Integer.parseInt(fieldValue);
			int AnsPosition1 = (int) ((sliderwidth) * ((-.096) * (5 - (Ans1))));
			if (AnsPosition1 < 1)
			{
				AnsPosition1 = AnsPosition1 - 20;
			}
			else
			{
				AnsPosition1 = AnsPosition1 + 20;
			}
			Actions builder = new Actions(driver.getWebDriver());
			// builder.moveToElement(slider).click().dragAndDropBy(slider,
			// AnsPosition1, 0).build().perform();
			builder.moveToElement(slider).moveByOffset(AnsPosition1, 0).doubleClick().perform();
			isElementVisible(By.xpath("//output[@id='output-number']"), "Slider Answer");
			String Output = driver.findElement(By.xpath("//output[@id='output-number']")).getText();
			if (Output.equalsIgnoreCase(fieldValue))
				report.updateTestLog("Verify the answer is selected in the slider", "The answer <b>" + fieldValue + "</b> is successfully selected in the slider <b>" + fieldName, Status.PASS);
			else
			{
				report.updateTestLog("Verify the answer is selected in the slider", "The answer <b>" + fieldValue + "</b> is not selected in the slider <b>", Status.FAIL);
			}
		}
		else
		{
			report.updateTestLog("selectSliderOption", "Element not Present: " + fieldName, Status.FAIL);
		}
	}

	////////////////////////////////////////////////////////////////////////
	public void selectSliderOption222(By by, String fieldName, String fieldValue) throws Exception
	{
		if (isElementPresent(by, fieldName))
		{
			WebElement slider = driver.findElement(by);
			highLightElement(slider, "Slider " + fieldName);
			int sliderwidth = driver.findElement(by).getSize().getWidth();
			System.out.print(sliderwidth);
			int Ans1 = Integer.parseInt(fieldValue);
			int AnsPosition1 = (int) ((sliderwidth) * ((-.096) * (5 - (Ans1))));
			System.out.println(fieldName + " - AnsPosition1: " + AnsPosition1);
			Actions builder = new Actions(driver.getWebDriver());
			builder.moveToElement(slider).click().dragAndDropBy(slider, AnsPosition1, 0).build().perform();
			String Output = driver.findElement(By.xpath("//output[@id='output-number']")).getText();
			System.out.println(fieldName + " - Output: " + Output);
			if (Output.equalsIgnoreCase(fieldValue))
				report.updateTestLog("Verify the answer is selected in the slider", "The answer <b>" + fieldValue + "</b> is successfully selected in the slider <b>" + fieldName, Status.PASS);
			else
			{
				report.updateTestLog("Verify the answer is selected in the slider", "The answer <b>" + fieldValue + "</b> is not selected in the slider <b>", Status.FAIL);
			}
		}
		else
		{
			report.updateTestLog("selectSliderOption", "Element not Present: " + fieldName, Status.FAIL);
		}
	}

	public void selectSliderRadioOption(WebDriver sc, By by, String fieldName, String fieldValue)
	{
		try
		{
			List<WebElement> radioButtons = driver.findElements(by);
			boolean isClicked = false;
			for (WebElement radio : radioButtons)
			{
				if (radio.getText().equalsIgnoreCase(fieldValue))
				{
					radio.click();
					isClicked = true;
				}
			}
			if (isClicked)
			{
				report.updateTestLog("Verify the radio option is selected in the slider", "The radio option <b>" + fieldValue + "</b> is successfully selected in the slider <b>" + fieldName, Status.PASS);
			}
			else
			{
				report.updateTestLog("Verify the radio option is selected in the slider", "The radio option <b>" + fieldValue + "</b> is not selected in the slider <b>" + fieldName, Status.FAIL);
			}
		}
		catch (Exception e)
		{
		}
	}

	public void selectSliderCheckBoxOption(WebDriver sc, By by, String fieldName, String fieldValue)
	{
		try
		{
			List<WebElement> checkbox = driver.findElements(by);
			boolean isClicked = false;
			for (WebElement option : checkbox)
			{
				if (option.getText().equalsIgnoreCase(fieldValue))
				{
					option.click();
					isClicked = true;
				}
			}
			if (isClicked)
			{
				report.updateTestLog("Verify the given option is selected in the slider", "The option <b>" + fieldValue + "</b> is successfully selected in the slider <b>" + fieldName, Status.PASS);
			}
			else
			{
				report.updateTestLog("Verify the given option is selected in the slider", "The option <b>" + fieldValue + "</b> is successfully selected in the slider <b>" + fieldName, Status.FAIL);
			}
		}
		catch (Exception e)
		{
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public void highLightElement(By byLocator, String strElementDesc) throws Exception
	{
		String strHighLightElement = properties.getProperty("HighLightElements").trim();
		if (strHighLightElement.equalsIgnoreCase("ON"))
		{
			if (isElementVisible(byLocator, strElementDesc))
			{
				try
				{
					WebElement element = driver.findElement(byLocator);
					JavascriptExecutor js = (JavascriptExecutor) driver.getWebDriver();
					for (int i = 0; i < 1; i++)
					{
						js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "border: 6px solid red;");
						waitForMilliSeconds(20);
						js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "");
						waitForMilliSeconds(20);
					}
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("highLightElement " + strElementDesc + " failed");
			} // isElementVisible
		} // strHighLightElement
	}

	public void highLightElement(WebElement element, String strElementDesc) throws Exception
	{
		String strHighLightElement = properties.getProperty("HighLightElements").trim();
		if (strHighLightElement.equalsIgnoreCase("ON"))
		{
			if (isElementPresent(element, strElementDesc))
			{
				JavascriptExecutor js = (JavascriptExecutor) driver.getWebDriver();
				for (int i = 0; i < 1; i++)
				{
					js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "border: 6px solid red;");
					waitForSeconds(1);
					js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "");
					waitForSeconds(1);
				}
			} // isElementPresent
		} // strHighLightElement
	}

	/*public void scrollToElement(By by, String strElementDesc) throws Exception
	{
		// scrollInToViewElement(by,strElementDesc,100);
		if (isElementPresent(by, strElementDesc))
		{
			WebElement webElement = driver.findElement(by);
			JavascriptExecutor je = ((JavascriptExecutor) driver.getWebDriver()); // Create
																					// instance
																					// of
																					// Javascript
																					// executor
			je.executeScript("arguments[0].scrollIntoView(true);", webElement);
			// System.out.println("Scroll in to view Element:" +
			// strElementDesc);
		}
		else
		{
			report.updateTestLog("ScrollToElement", "Element not Present: " + strElementDesc, Status.FAIL);
		}
	}*/

	public void scrollToElementAction(By by, String strElementDesc) throws Exception
	{
		WebElement webElement = driver.findElement(by);
		Actions action = new Actions(driver.getWebDriver());
		action.moveToElement(webElement);
		action.perform();
		System.out.println("Scroll in to view Element:" + strElementDesc);

	}

	public void scrollToElement(By by, String strElementDesc) throws Exception
	{
		WebElement webElement = driver.findElement(by);
		JavascriptExecutor js = ((JavascriptExecutor) driver.getWebDriver());
		js.executeScript("arguments[0].scrollIntoView(true);", webElement);
		js.executeScript("window.scrollBy(0,-50)");
		System.out.println("Scroll in to view Element:" + strElementDesc);
		waitForSeconds(1);
		highLightElement(by, strElementDesc);
		report.updateTestLog("ScrollToElement", "Element scrolled to : <b>" + strElementDesc + "</b>", Status.PASS);

	}

	public void scrollElementToMiddlePage(By by, String strElementDesc) throws Exception
	{
		// scrollInToViewElement(by,strElementDesc,100);
		if (isElementPresent(by, strElementDesc))
		{
			WebElement webElement = driver.findElement(by);
			String strJS = "arguments[0].scrollIntoView(true); var viewportH = Math.max(document.documentElement.clientHeight, window.innerHeight || 0); " + "window.scrollBy(0, (arguments[0].getBoundingClientRect().height-viewportH)/2);";
			JavascriptExecutor je = ((JavascriptExecutor) driver.getWebDriver()); // Create
																					// instance
																					// of
																					// Javascript
																					// executor
			je.executeScript(strJS, webElement);
			// System.out.println("Scroll in to view Element:" +
			// strElementDesc);
		}
		else
		{
			report.updateTestLog("ScrollToElement", "Element not Present: " + strElementDesc, Status.FAIL);
		}
	}

	public void scrollElementToMiddlePage(WebElement webElement, String strElementDesc) throws Exception
	{
		// scrollInToViewElement(by,strElementDesc,100);
		if (isElementPresent(webElement, strElementDesc))
		{
			// WebElement webElement = driver.findElement(by);
			String strJS = "arguments[0].scrollIntoView(true); var viewportH = Math.max(document.documentElement.clientHeight, window.innerHeight || 0); " + "window.scrollBy(0, (arguments[0].getBoundingClientRect().height-viewportH)/2);";
			JavascriptExecutor je = ((JavascriptExecutor) driver.getWebDriver()); // Create
																					// instance
																					// of
																					// Javascript
																					// executor
			je.executeScript(strJS, webElement);
			// System.out.println("Scroll in to view Element:" +
			// strElementDesc);
		}
		else
		{
			report.updateTestLog("ScrollToElement", "Element not Present: " + strElementDesc, Status.FAIL);
		}
	}

	public void scrollToElement(By by, String strElementDesc, int iScrollPixel) throws Exception
	{
		if (isElementPresent(by, strElementDesc))
		{
			WebElement webElement = driver.findElement(by);
			JavascriptExecutor je = ((JavascriptExecutor) driver.getWebDriver());
			je.executeScript("window.scrollTo(" + webElement.getLocation().x + "," + (webElement.getLocation().y - iScrollPixel) + ");");
			waitForSeconds(1);
			highLightElement(by, strElementDesc);
			report.updateTestLog("ScrollToElement", "Element scrolled to : <b>" + strElementDesc + "</b>", Status.PASS);
		}
		else
		{
			report.updateTestLog("ScrollToElement", "Element not Present: " + strElementDesc, Status.FAIL);
		}
	}

	/**
	 * Description: scroll the window to the bottom of the page.
	 * 
	 * @throws Exception
	 * 
	 * @throws Exception
	 */
	public void scrollToBottom2() throws Exception
	{
		System.out.println("Scroll to the Bottom of the page");
		try
		{
			JavascriptExecutor je = (JavascriptExecutor) driver.getWebDriver();
			je.executeScript("window.scrollTo(0, document.body.scrollHeight);");// scroll
																				// To
																				// Bottom
																				// of
																				// the
																				// Page
		}
		catch (Exception e)
		{
			report.updateTestLog("scrollToBottom", "Scroll to bottom failed", Status.FAIL);
		}
	}

	public void scrollToBottom()
	{
		System.out.println("Scroll to the Bottom of the page");
		JavascriptExecutor je = ((JavascriptExecutor) driver.getWebDriver());
		Long iScrollHeight = (long) 1.00;
		Long iScrollHeight_Old = (long) 0.00;
		while (iScrollHeight > iScrollHeight_Old)
		{
			((JavascriptExecutor) driver.getWebDriver()).executeScript("window.scrollTo(0, document.body.scrollHeight);");// scroll
																															// To
																															// Bottom
																															// of
																															// the
																															// Page
			waitForMilliSeconds(100);
			iScrollHeight_Old = iScrollHeight;
			iScrollHeight = (Long) je.executeScript("return document.body.scrollHeight;");
			// report.updateTestLog("testPage", "Scroll to bottom",
			// Status.SCREENSHOT);
		}
	}

	public void scrollToMiddlePage() throws Exception
	{
		System.out.println("Scroll to the Bottom of the page");
		JavascriptExecutor je = ((JavascriptExecutor) driver.getWebDriver());
		Double iScrollHeight = (Double) 1.00;
		Double iScrollHeight_Old = (Double) 0.00;
		while (iScrollHeight > iScrollHeight_Old)
		{
			((JavascriptExecutor) driver.getWebDriver()).executeScript("window.scrollTo(0, document.body.scrollHeight%2);");// scroll
																															// To
																															// Bottom
																															// of
																															// the
																															// Page
			TimeUnit.SECONDS.sleep(2);
			iScrollHeight_Old = iScrollHeight;
			iScrollHeight = (Double) je.executeScript("return document.body.scrollHeight/2;");
			// report.updateTestLog("testPage", "Scroll to bottom",
			// Status.SCREENSHOT);
		}
	}

	/**
	 * Description: scroll the window to the Top of the page.
	 * 
	 * @throws Exception
	 */
	public void scrollToTop()
	{
		// System.out.println("Scroll to Top Window");
		JavascriptExecutor je = ((JavascriptExecutor) driver.getWebDriver());
		// ((JavascriptExecutor)
		// driver.getWebDriver()).executeScript("window.scrollBy(0, -2500)");
		// je.executeScript("window.scrollTo(" + webElement.getLocation().x +
		// "," + (webElement.getLocation().y - 100) + ");");
		je.executeScript("window.scrollTo(0,0);");
		System.out.println("Scroll in to Top of the page");
	}

	public String loadEnvFile(String strEnv)
	{
		Properties propEnv = new Properties();
		InputStream input = null;
		try
		{
			input = new FileInputStream("env.properties");
			propEnv.load(input);
		}
		catch (IOException ex)
		{
			new FileNotFoundException("Check the env.properties file in workspace");
		}
		return propEnv.getProperty(strEnv);
	}

	public Boolean switchToFrame(String strFrameId) throws Exception
	{
		By byFrame = By.xpath("//iframe[@id='" + strFrameId + "']");
		Boolean bSwitchToFrame = false;
		if (isElementVisible(byFrame, strFrameId))
		{
			driver.switchTo().frame(strFrameId);
			report.updateTestLog("switchToFrame", "switched To Frame ", Status.PASS);
			bSwitchToFrame = true;
		}
		else
		{
			report.updateTestLog("switchToFrame", "switchToFrame failed, as Frame: " + strFrameId + " is not present", Status.FAIL);
			// frameworkparameters.setStopExecution(true);
		}
		return bSwitchToFrame;
	}

	public void switchToParentFrame()
	{
		try
		{
			driver.getWebDriver().switchTo().defaultContent();
			// driver.switchTo().parentFrame();
			// driver.switchTo().defaultContent();
		}
		catch (Exception e)
		{
			// e.printStackTrace();
			System.out.println("Error in Switch to defaultContent");
			try
			{
				// driver.switchTo().defaultContent();
				driver.getWebDriver().switchTo().parentFrame();
			}
			catch (Exception ee)
			{
				// e.printStackTrace();
				System.out.println("Error in Switch to Parent Frame");
			}
		}
	}

	public Boolean switchToFrame(By byFrame) throws Exception
	{
		driver.switchTo().defaultContent();
		Boolean bSwitchToFrame = false;
		String strFrameDesc = "NoFrameDesc";
		if (isElementVisible(byFrame, "Frame"))
		{
			driver.switchTo().frame(driver.findElement(byFrame));
			bSwitchToFrame = true;
			report.updateTestLog("switchToFrame", "switched To Frame: <b>" + strFrameDesc + "</b>", Status.DONE);
			System.out.println("switched To Frame: " + strFrameDesc);
		}
		else
		{
			report.updateTestLog("switchToFrame", "switchToFrame failed, as Frame: <b>" + strFrameDesc + "</b> is not present", Status.FAIL);
			System.out.println("switchToFrame failed, as Frame: " + strFrameDesc + " is not present");
		}
		return bSwitchToFrame;
	}

	public Boolean switchToFrame1(By byFrame) throws Exception
	{
		//driver.switchTo().parentFrame();
		Boolean bSwitchToFrame = false;
		String strFrameDesc = "NoFrameDesc";
		if (isElementVisible(byFrame, "Frame"))
		{
			driver.switchTo().frame(driver.findElement(byFrame));
			bSwitchToFrame = true;
			report.updateTestLog("switchToFrame", "switched To Frame: <b>" + strFrameDesc + "</b>", Status.DONE);
			System.out.println("switched To Frame: " + strFrameDesc);
		}
		else
		{
			report.updateTestLog("switchToFrame", "switchToFrame failed, as Frame: <b>" + strFrameDesc + "</b> is not present", Status.FAIL);
			System.out.println("switchToFrame failed, as Frame: " + strFrameDesc + " is not present");
		}
		return bSwitchToFrame;
	}

	public Boolean switchToFrame(By byFrame, String strFrameDesc) throws Exception
	{
		// driver.switchTo().parentFrame();
		//		switchToParentFrame();
		Boolean bSwitchToFrame = false;
		if (isElementVisible(byFrame, strFrameDesc))
		{
			// highLightElement(byFrame, strFrameDesc);
			driver.switchTo().frame(driver.findElement(byFrame));
			bSwitchToFrame = true;
			report.updateTestLog("switchToFrame", "switched To Frame: <b>" + strFrameDesc + "</b>", Status.DONE);
			System.out.println("switched To Frame: " + strFrameDesc);
		}
		else
		{
			report.updateTestLog("switchToFrame", "switchToFrame failed, as Frame: <b>" + strFrameDesc + "</b> is not present", Status.FAIL);
			System.out.println("switchToFrame failed, as Frame: " + strFrameDesc + " is not present");
		}
		return bSwitchToFrame;
	}

	public Boolean switchToFrame(By byFrame, String strpageDescription, String strPageTypeCheck) throws Exception
	{
		waitForJStoLoad(driver, 20);
		Boolean bSwitchToFrame = false;
		By byPageDesc = By.xpath("//div[@class='bPageTitle']//h2[@class='pageDescription']");
		By byPageType = By.xpath("//div[@class='bPageTitle']//h1[contains(@class,'pageType')]");
		// By byPageTypeWithNoSecondHeader =
		// By.xpath("//div[@class='bPageTitle']//h1[@class='noSecondHeader
		// pageType']"); //Change Case Owner only
		String strPageType = "", strPageDesc = "";
		// driver.switchTo().parentFrame();
		switchToParentFrame();
		if (isElementVisible(byFrame, "Frames " + strpageDescription + " - " + strPageTypeCheck))
		{
			List<WebElement> eFrames = driver.findElements(byFrame);
			int iFramesCount = eFrames.size();
			System.out.println("iFramesCount: " + iFramesCount);
			for (int i = 0; i < iFramesCount; i++)
			{
				driver.switchTo().parentFrame();
				eFrames = driver.findElements(byFrame);
				iFramesCount = eFrames.size();
				// String strFrameSrc = eFrames.get(i).getAttribute("src");
				// System.out.println(i + "strFrameSrc: " + strFrameSrc);
				driver.switchTo().frame(eFrames.get(i));
				// System.out.println(i + "switched To Frame: ");
				if (isElementPresent(driver, byPageDesc))
				{
					strPageDesc = getData(byPageDesc, "PageDesc", "Text");
					if (!(strPageDesc != null))
					{
						strPageDesc = "";
					}
				}
				if (isElementPresent(byPageType, "PageType", 10))
				{
					strPageType = getData(byPageType, "PageType", "Text");
					System.out.println(i + "strPageType: " + strPageType);
					System.out.println(i + "strPageDesc: " + strPageDesc);
					if (strPageTypeCheck.equalsIgnoreCase(strPageType) && strpageDescription.equalsIgnoreCase(strPageDesc))
					{
						bSwitchToFrame = true;
						// highLightElement(byFrame, "PageType: " +
						// strPageTypeCheck + ", PageDescription: " +
						// strpageDescription);
						report.updateTestLog("switchToFrame", "switched To Frame: <b>" + "PageType: " + strPageTypeCheck + ", PageDescription: " + strpageDescription + "</b>", Status.DONE);
						System.out.println("switched To Frame: " + "PageType: " + strPageTypeCheck + ", PageDescription: " + strpageDescription);
						break;
					}
				}
				else
				{
					System.out.println("switched To Frame: " + "PageType: " + strPageTypeCheck + " not present");
					report.updateTestLog("switchToFrame", "switched To Frame: <b>" + "PageType: " + strPageTypeCheck + " not present", Status.FAIL);
				}
			}
			if (!bSwitchToFrame)
			{
				report.updateTestLog("switchToFrame", "switchToFrame failed, as Frame: <b>" + "PageType: " + strPageTypeCheck + ", PageDescription: " + strpageDescription + "</b> is not present", Status.FAIL);
				System.out.println("switchToFrame failed, as Frame: " + "PageType: " + strPageTypeCheck + ", PageDescription: " + strpageDescription + " is not present");
			}
		}
		return bSwitchToFrame;
	}

	public void closeActivePrimaryTab() throws Exception
	{
		switchToParentFrame();
		By byTabClose = By.xpath("//div[@id='navigatortab']//div[contains(@class,'primary_tabstrip')]//ul/li[contains(@class,'x-tab-strip-active')]/a[@class='x-tab-strip-close']");
		By byTabName = By.xpath("//div[@id='navigatortab']//div[contains(@class,'primary_tabstrip')]//ul/li[contains(@class,'x-tab-strip-active')]/a[@href]//span[@class='tabText']");
		String strTabName = getData(byTabName, "byTabName", "text");
		clickByJSE(byTabClose, "Active Primary Tab: " + strTabName + " - Close");
	}

	public void closeActiveSecondaryTab() throws Exception
	{
		switchToParentFrame();
		By byActivePrimaryTab = By.xpath("//div[@id='navigatortab']//div[contains(@class,'primary_tabstrip')]//ul/li[contains(@class,'x-tab-strip-active')]");
		String strPTabId = getData(byActivePrimaryTab, "ActivePrimaryTab", "id");
		strPTabId = strPTabId.replaceAll("navigatortab__", "");
		String strPrimaryActiveTabXPath = "//div[@id='navigatortab']//div[@id='" + strPTabId + "']";
		By bySTabClose = By.xpath(strPrimaryActiveTabXPath + "//div[contains(@class,'secondary_tabstrip')]//ul/li[contains(@class,'x-tab-strip-active')]/a[@class='x-tab-strip-close']");
		By bySTabName = By.xpath(strPrimaryActiveTabXPath + "//div[contains(@class,'secondary_tabstrip')]//ul/li[contains(@class,'x-tab-strip-active')]/a[@href]//span[@class='tabText']");
		String strSTabName = getData(bySTabName, "STabName", "text");
		clickByJSE(bySTabClose, "Active Secondary Tab: " + strSTabName + " - Close");
	}

	public Boolean switchToPage(String strPageTitle) throws Exception
	{
		Boolean bPageTitleSwitch = false;
		System.out.println("switchToPage: " + strPageTitle);
		String strTmpPageTitle1 = "";
		try
		{
			strTmpPageTitle1 = getSFPageTitle();
		}
		catch (Exception e)
		{
			// Nothing
		}
		if (strTmpPageTitle1.equalsIgnoreCase(strPageTitle))
		{
			bPageTitleSwitch = true;
		}
		else
		{
			ArrayList<String> availableWindows = new ArrayList<String>(driver.getWindowHandles());
			int iWindowcount = availableWindows.size();
			System.out.println("iWindowcount:" + iWindowcount);
			for (int i = 0; i < iWindowcount; i++)
			{
				String strHandle = availableWindows.get(i);
				driver.switchTo().window(strHandle);
				String strTmpPageTitle = getSFPageTitle();
				System.out.println(i + " PageTitle: " + strTmpPageTitle);
				if ((strTmpPageTitle.toLowerCase().trim()).startsWith(strPageTitle.toLowerCase().trim()))
				{
					bPageTitleSwitch = true;
					break;
				}
			} // for Loop
			if (bPageTitleSwitch)
			{
				System.out.println("Switched to Page: " + strPageTitle);
				report.updateTestLog("switchToPage", "Switched to Page: " + strPageTitle, Status.PASS);
			}
			else
			{
				report.updateTestLog("switchToPage", "Page: " + strPageTitle + " not Found", Status.FAIL);
			}
		}
		return bPageTitleSwitch;
	} // End of switchToPage method

	public void switchToLastOpenedPage() throws Exception
	{
		int iWindowcount = 0;
		try
		{
			ArrayList<String> availableWindows = new ArrayList<String>(driver.getWindowHandles());
			iWindowcount = availableWindows.size();
			String strHandle = availableWindows.get(iWindowcount - 1);
			driver.getWebDriver().switchTo().window(strHandle);
			System.out.println("switchToLastOpenedPage");
			driver.getWebDriver().manage().window().maximize();
			String strPageTitle = getSFPageTitle();
			report.updateTestLog("switchToLastPage", "switching to Last Window -" + iWindowcount + ": " + strPageTitle, Status.PASS);
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			report.updateTestLog("switchToLastPage", "Error in switching to Last Window: " + iWindowcount, Status.FAIL);
		}
	}

	public void clicksalesForceLHNavigationArrow(By by, String strElementLoc) throws Exception
	{
		driver.switchTo().parentFrame();
		By navigatorTab_Arrow = By.xpath("//div[@id='navigatortab']//div[contains(@class,'servicedesk-navigator')]//td[@class='x-btn-mc']");
		if (verifyElementPresent(navigatorTab_Arrow, "navigatorTab_Arrow"))
		{
			WebElement element = driver.findElement(navigatorTab_Arrow);
			int iHeight = element.getSize().getHeight(); // To get the Height of
															// the Element
			Actions action = new Actions(driver.getWebDriver());
			action.moveToElement(element).moveByOffset(110, iHeight / 2).click().build().perform();
		}
	}

	public void mouseClickJScript(By by, String strElementDesc)
	{
		try
		{
			if (isElementVisible(by, strElementDesc))
			{
				WebElement HoverElement = driver.findElement(by);
				JavascriptExecutor je = (JavascriptExecutor) driver;
				// Mouse move to Element
				String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover',true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
				je.executeScript(mouseOverScript, HoverElement);
				// Mouse click on the Element
				mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('click',true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onclick');}";
				je.executeScript(mouseOverScript, HoverElement);
				System.out.println("mouseClickJScript: Element Clicked: " + strElementDesc);
				report.updateTestLog("mouseClickJScript", "Element Clicked: " + strElementDesc, Status.DONE);
			}
			else
			{
				System.out.println("Element was not visible to MouseHover " + "\n");
				report.updateTestLog("mouseClickJScript", "Element was not visible to MouseHover: " + strElementDesc, Status.FAIL);
			}
		}
		catch (StaleElementReferenceException e)
		{
			System.out.println("Element " + strElementDesc + "is not attached to the page document" + e.getStackTrace());
		}
		catch (NoSuchElementException e)
		{
			System.out.println("Element " + strElementDesc + " was not found in DOM" + e.getStackTrace());
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Error occurred while hovering" + e.getStackTrace());
		}
	}// end

	public void mouseClickJScript(WebElement HoverElement, String strElementDesc)
	{
		try
		{
			if (isElementPresent(HoverElement, strElementDesc))
			{
				// WebElement HoverElement = driver.findElement(by);
				JavascriptExecutor je = (JavascriptExecutor) driver;
				// Mouse move to Element
				String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover',true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
				je.executeScript(mouseOverScript, HoverElement);
				// Mouse click on the Element
				mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('click',true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onclick');}";
				je.executeScript(mouseOverScript, HoverElement);
				System.out.println("mouseClickJScript: Element Clicked: " + strElementDesc);
				report.updateTestLog("mouseClickJScript", "Element Clicked: " + strElementDesc, Status.DONE);
			}
			else
			{
				System.out.println("Element was not visible to MouseHover " + "\n");
				report.updateTestLog("mouseClickJScript", "Element was not visible to MouseHover: " + strElementDesc, Status.FAIL);
			}
		}
		catch (StaleElementReferenceException e)
		{
			System.out.println("Element " + strElementDesc + "is not attached to the page document" + e.getStackTrace());
		}
		catch (NoSuchElementException e)
		{
			System.out.println("Element " + strElementDesc + " was not found in DOM" + e.getStackTrace());
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Error occurred while hovering" + e.getStackTrace());
		}
	}// end

	/**
	 * Description: Get data from the specified Element.
	 * 
	 * @param By
	 *            byLocator - Locator of the Element in By Class format
	 * @param String
	 *            strElementDesc - Description of the Element
	 * @param String
	 *            strAttribute - Attribute related to the Element
	 * @param return
	 *            String - return the data got from the Element
	 * @throws Exception
	 */
	public String getData(By by, String strElementDesc, String strAttribute) throws Exception
	{
		// System.out.println("Get Attribute: " + strAttribute + " data, from
		// Element: " + strElementDesc);
		String strAttributeValue = "Error";
		strAttribute = strAttribute.toLowerCase();
		if (isElementPresent(by, strElementDesc))
		{
			// highLightElement(by,strElementDesc);
			if (strAttribute.equalsIgnoreCase("Text") || strAttribute.equalsIgnoreCase("dcr-text"))
			{
				strAttributeValue = driver.findElement(by).getText().trim();
			}
			else if (strAttribute.trim().equalsIgnoreCase("select"))
			{
				strAttributeValue = new Select(driver.findElement(by)).getFirstSelectedOption().getText();
			}
			else
			{
				try
				{
					strAttributeValue = driver.findElement(by).getAttribute(strAttribute).trim();
				}
				catch (Exception e)
				{
					// report.updateTestLog("getData", "strAttribute: " +
					// strAttribute + " not Present in " + strElementDesc ,
					// Status.FAIL);
				}
			}
			// report.updateTestLog("getData", strAttributeValue + ", Attribute:
			// " + strAttribute + " data, from Element: " + strElementDesc ,
			// Status.DONE);
		}
		else
		{
			report.updateTestLog("getData", "Element not Present: " + strElementDesc, Status.FAIL);
		}
		return strAttributeValue;
	}// getData

	public String getData(WebElement element, String strElementDesc, String strAttribute) throws Exception
	{
		// System.out.println("Get Attribute: " + strAttribute + " data, from
		// Element: " + strElementDesc);
		String strAttributeValue = null;
		strAttribute = strAttribute.toLowerCase();
		if (isElementPresent(element, strElementDesc))
		{
			if (strAttribute.equalsIgnoreCase("Text"))
			{
				strAttributeValue = element.getText().trim();
			}
			else
			{
				try
				{
					strAttributeValue = element.getAttribute(strAttribute).trim();
				}
				catch (Exception e)
				{
					report.updateTestLog("getData", "strAttribute: " + strAttribute + " not Present in " + strElementDesc, Status.FAIL);
				}
			}
			// report.updateTestLog("getData", strAttributeValue + ", Attribute:
			// " + strAttribute + " data, from Element: " + strElementDesc ,
			// Status.DONE);
		}
		else
		{
			report.updateTestLog("getData", "Element not Present: " + strElementDesc, Status.FAIL);
		}
		return strAttributeValue;
	}// getData

	public void hoverAction(By by, String strElementDesc) throws Exception
	{
		if (isElementPresent(by, strElementDesc))
		{
			WebElement elementToClick = driver.findElement(by);
			// highLightElement(element);
			Actions action = new Actions(driver.getWebDriver());
			action.moveToElement(elementToClick).build().perform();
			System.out.println("Hover to Element : " + strElementDesc);
			report.updateTestLog("Hover to Element", "Hover to Element: " + strElementDesc, Status.DONE);
		}
		else
		{
			report.updateTestLog("Hover to Element", "Element not Present: " + strElementDesc, Status.FAIL);
		}
	}

	public void hoverAction(WebElement element1, WebElement element2) throws Exception
	{
		if (isElementPresent(element1, "element1"))
		{
			// WebElement elementToClick = driver.findElement(by);
			highLightElement(element1, "");
			if (isElementPresent(element2, "element2"))
			{
				Actions action = new Actions(driver.getWebDriver());
				action.moveToElement(element1).moveToElement(element2);
				System.out.println("Hover to Element : ");
				report.updateTestLog("Hover to Element", "Hover to Element: " + "strElementDesc", Status.DONE);
			}
			else
			{
				report.updateTestLog("Hover to Element2", "Element not Present: " + "strElementDesc", Status.FAIL);
			}
		}
		else
		{
			report.updateTestLog("Hover to Element1", "Element not Present: " + "strElementDesc", Status.FAIL);
		}
	}

	public void actionClick(By by, String strElementDesc) throws Exception
	{
		if (isElementVisible(by, strElementDesc))
		{
			WebElement elementToClick = driver.findElement(by);
			highLightElement(by, strElementDesc);
			Actions action = new Actions(driver.getWebDriver());
			action.moveToElement(elementToClick).click(elementToClick).build().perform();
			System.out.println("Element Clicked: " + strElementDesc);
			waitForSeconds(1);
			report.updateTestLog("hoverActionClick", "Element Clicked: <b>" + strElementDesc + "</b>", Status.PASS);
			driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		}
		else
		{
			report.updateTestLog("Verify the button <b>" + strElementDesc + "</b> is clicked", "Unable to click the button <b>" + strElementDesc + "</b>", Status.FAIL);
			System.out.println("The Element: " + strElementDesc + " is not present and so element not clicked");
		}

	}

	public void actionClick_WithoutScreenshot(By by, String strElementDesc) throws Exception
	{
		if (isElementVisible(by, strElementDesc))
		{
			WebElement elementToClick = driver.findElement(by);
			highLightElement(by, strElementDesc);
			Actions action = new Actions(driver.getWebDriver());
			action.moveToElement(elementToClick).click(elementToClick).build().perform();
			System.out.println("Element Clicked: " + strElementDesc);
			report.updateTestLog("hoverActionClick", "Element Clicked: <b>" + strElementDesc + "</b>", Status.DONE);
			driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			waitForSeconds(1);
		}
		else
			report.updateTestLog("Verify the button <b>" + strElementDesc + "</b> is clicked", "Unable to click the button <b>" + strElementDesc + "</b>", Status.FAIL);
	}

	public void actionToEnterText(By by, String strElementDesc, String text) throws Exception
	{
		if (isElementVisible(by, strElementDesc))
		{
			WebElement elementToClick = driver.findElement(by);
			highLightElement(by, strElementDesc);
			Actions action = new Actions(driver.getWebDriver());
			action.moveToElement(elementToClick).click(elementToClick).build().perform();
			driver.findElement(by).sendKeys(text);
			System.out.println("Element Clicked: " + strElementDesc);
			report.updateTestLog("hoverActionClick", "Element Clicked: " + strElementDesc, Status.PASS);
			waitForSeconds(1);
		}
		else
		{
			report.updateTestLog("hoverActionClick", "Element not Present: " + strElementDesc, Status.FAIL);
		}
	}

	public void actionToChooseDropdown(By by, String optionToBeSelected, String strElementDesc) throws Exception
	{
		if (isElementVisible(by, strElementDesc))
		{
			WebElement elementToClick = driver.findElement(by);
			//highLightElement(by, strElementDesc);
			Actions action = new Actions(driver.getWebDriver());
			action.moveToElement(elementToClick).click(elementToClick).build().perform();
			actionClick(By.xpath("//a[text()='" + optionToBeSelected + "']"), "Status chosen");
			System.out.println("Element Clicked: " + strElementDesc);
			report.updateTestLog("hoverActionClick", "Element Clicked: " + strElementDesc, Status.PASS);
		}
		else
		{
			report.updateTestLog("hoverActionClick", "Element not Present: " + strElementDesc, Status.FAIL);
		}
	}

	public void actionClick(WebElement elementToClick, String strElementDesc) throws Exception
	{
		if (isElementPresent(elementToClick, strElementDesc))
		{
			// highLightElement(element);
			Actions action = new Actions(driver.getWebDriver());
			action.moveToElement(elementToClick).click(elementToClick).build().perform();
			System.out.println("Element Clicked: " + strElementDesc);
			report.updateTestLog("hoverActionClick", "Element Clicked: " + strElementDesc, Status.DONE);
		}
		else
		{
			report.updateTestLog("hoverActionClick", "Element not Present: " + strElementDesc, Status.FAIL);
		}
	}

	public void actionDoubleClick(By by, String strElementDesc) throws Exception
	{
		if (isElementPresent(by, strElementDesc))
		{
			try
			{
				// Actions action = new
				// Actions(driver.getWebDriver()).doubleClick(element);
				highLightElement(by, strElementDesc);
				WebElement element = driver.findElement(by);
				Actions action = new Actions(driver.getWebDriver()).moveToElement(element).doubleClick(element);
				action.build().perform();
				System.out.println("Double clicked the element: " + strElementDesc);
				report.updateTestLog("actionDoubleClick", "Element: <b>" + strElementDesc + "</b> is double clicked.", Status.DONE);
			}
			catch (StaleElementReferenceException e)
			{
				System.out.println("Element: " + strElementDesc + " is not attached to the page document " + e.getStackTrace());
			}
			catch (NoSuchElementException e)
			{
				System.out.println("Element: " + strElementDesc + " was not found in DOM " + e.getStackTrace());
			}
			catch (Exception e)
			{
				System.out.println("Element: " + strElementDesc + " was not clickable " + e.getStackTrace());
			}
		}
		else
		{
			report.updateTestLog("actionDoubleClick", "Element not Present: " + strElementDesc, Status.FAIL);
		}
	}

	public void actionDoubleClick(WebElement element, String strElementDesc) throws Exception
	{
		if (isElementPresent(element, strElementDesc))
		{
			try
			{
				// Actions action = new
				// Actions(driver.getWebDriver()).doubleClick(element);
				Actions action = new Actions(driver.getWebDriver()).moveToElement(element).doubleClick(element);
				action.build().perform();
				System.out.println("Double clicked the element: " + strElementDesc);
				report.updateTestLog("actionDoubleClick", "Element: <b>" + strElementDesc + "</b> is double clicked.", Status.PASS);
			}
			catch (StaleElementReferenceException e)
			{
				System.out.println("Element: " + strElementDesc + " is not attached to the page document " + e.getStackTrace());
			}
			catch (NoSuchElementException e)
			{
				System.out.println("Element: " + strElementDesc + " was not found in DOM " + e.getStackTrace());
			}
			catch (Exception e)
			{
				System.out.println("Element: " + strElementDesc + " was not clickable " + e.getStackTrace());
			}
		}
		else
		{
			report.updateTestLog("actionDoubleClick", "Element not Present: " + strElementDesc, Status.FAIL);
		}
	}

	public void actionRightClick(By by, String strElementDesc) throws Exception
	{
		if (isElementVisible(by, strElementDesc))
		{
			WebElement elementToClick = driver.findElement(by);
			highLightElement(by, strElementDesc);
			Actions action = new Actions(driver.getWebDriver());
			action.moveToElement(elementToClick).contextClick(elementToClick).build().perform();
			System.out.println("Element Clicked: " + strElementDesc);
			report.updateTestLog("actionRightClick", "Element right clicked: " + strElementDesc, Status.DONE);
		}
		else
		{
			report.updateTestLog("actionRightClick", "Element not Present: " + strElementDesc, Status.FAIL);
		}
	}

	public void actionRightClick(WebElement elementToClick, String strElementDesc) throws Exception
	{
		if (isElementPresent(elementToClick, strElementDesc))
		{
			// highLightElement(element);
			Actions action = new Actions(driver.getWebDriver());
			action.moveToElement(elementToClick).contextClick(elementToClick).build().perform();
			System.out.println("Element Clicked: " + strElementDesc);
			report.updateTestLog("actionRightClick", "Element right clicked: " + strElementDesc, Status.DONE);
		}
		else
		{
			report.updateTestLog("actionRightClick", "Element not Present: " + strElementDesc, Status.FAIL);
		}
	}

	public Boolean waitUntilAttrubuteContains(By byLocator, String strElementDesc, String strAttribute, String strExpectedValue, int iWaitTime) throws Exception
	{
		Boolean bWait = false;
		// if(isElementVisible(byLocator, strElementDesc)){
		if (isElementPresent(byLocator, strElementDesc))
		{
			try
			{
				WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), iWaitTime);
				wait.until(ExpectedConditions.attributeContains(byLocator, strAttribute, strExpectedValue));
				bWait = true;
			}
			catch (Exception e)
			{
				// e.printStackTrace();
			}
		}
		return bWait;
	}

	public Boolean waitUntilElementContains(By byLocator, String strElementDesc, String strAttribute, String strExpectedValue, int iWaitTime) throws Exception
	{
		Boolean bWait = false;
		// if(isElementVisible(byLocator, strElementDesc)){
		if (isElementPresent(byLocator, strElementDesc))
		{
			if (strAttribute.equalsIgnoreCase("text"))
			{
				int iWaitCount = (iWaitTime * 1000) / 100;
				for (int i = 1; i <= iWaitCount; i++)
				{
					String strTxt = driver.findElement(byLocator).getText().trim();
					if (strTxt.equals(strExpectedValue))
					{
						bWait = true;
						break;
					}
					else
					{
						waitForMilliSeconds(100);
					}
				} // for loop
					// if(!bWait){
					// report.updateTestLog("waitUntilAttrubuteContains",
					// strElementDesc + " - Expexted Text: " + strExpectedValue
					// + " not found.", Status.FAIL);
					// }
			}
			else
			{
				try
				{
					WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), iWaitTime);
					wait.until(ExpectedConditions.attributeContains(byLocator, strAttribute, strExpectedValue));
					bWait = true;
				}
				catch (Exception e)
				{
					// e.printStackTrace();
				}
			}
		}
		return bWait;
	}

	public Boolean waitForPageToOpenAfterLinkClick2(By byLink, String strLinkDesc) throws Exception
	{
		Boolean bPageOpened = false;
		ArrayList<String> availableWindows = new ArrayList<String>(driver.getWindowHandles());
		int iCurrentWindowCount = availableWindows.size();
		int iWindowcountAfter = 0;
		System.out.println("iCurrentWindowCount:" + iCurrentWindowCount);
		if (verifyElementVisible(byLink, strLinkDesc))
		{
			// actionClick(byLink, strLinkDesc);
			clickByJSE(byLink, strLinkDesc);
			waitForSeconds(5);
			for (int j = 1; j <= 20; j++)
			{
				availableWindows = new ArrayList<String>(driver.getWindowHandles());
				iWindowcountAfter = availableWindows.size();
				System.out.println(j + " - iWindowcountAfter: " + iWindowcountAfter);
				if (iCurrentWindowCount == iWindowcountAfter)
				{
					waitForSeconds(2);
				}
				else
				{
					break;
				}
			} // for Loop
			try
			{
				String strHandle = availableWindows.get(iWindowcountAfter - 1);
				driver.switchTo().window(strHandle);
				System.out.println("Switched to Page: " + strHandle);
				driver.manage().window().maximize();
				// driver.navigate().refresh();
				System.out.println("Page maximised");
				// String strNewPageTitle = getSFPageTitle();
				// System.out.println("Switched to Page: " + strNewPageTitle);
				report.updateTestLog("Verify New Page is opened", "On Clicking the link:" + strLinkDesc + ", New Page is opened.", Status.PASS);
				System.out.println("Switched to Page: New Page is opened.");
				bPageOpened = true;
				System.out.println("bPageOpened: " + bPageOpened);
			}
			catch (Exception e)
			{
				// report.updateTestLog("Verify New Page is opened", "On
				// Clicking the link:" +strLinkDesc + ", New Page: not is
				// opened.", Status.FAIL);
				// report.updateTestLog("Verify New Page is opened", " Report
				// Error: " + e.getLocalizedMessage(), Status.DONE);
				report.updateTestLog("Verify New Page is opened", " Report Error: " + e.getLocalizedMessage(), Status.FAIL, false);
				System.out.println("Issue with New Page :" + e.getLocalizedMessage());
				driver.close();
			}
		}
		return bPageOpened;
	}// waitForPageToOpenAfterLinkClick2

	public Boolean waitForPageToOpenAfterLinkClick(By byLink, String strLinkDesc) throws Exception
	{
		Boolean bPageOpened = false;
		ArrayList<String> availableWindows = new ArrayList<String>(driver.getWindowHandles());
		int iCurrentWindowCount = availableWindows.size();
		System.out.println("iCurrentWindowCount:" + iCurrentWindowCount);
		if (verifyElementVisible(byLink, strLinkDesc))
		{
			actionClick(byLink, strLinkDesc);
			for (int j = 1; j <= 20; j++)
			{
				availableWindows = new ArrayList<String>(driver.getWindowHandles());
				int iWindowcountAfter = availableWindows.size();
				System.out.println(j + "iWindowcountAfter: " + iWindowcountAfter);
				if (iCurrentWindowCount == iWindowcountAfter)
				{
					waitForSeconds(2);
				}
				else
				{
					try
					{
						String strHandle = availableWindows.get(iWindowcountAfter - 1);
						driver.switchTo().window(strHandle);
						driver.manage().window().maximize();
						System.out.println("Switched to Page: " + getSFPageTitle());
						String strNewPageTitle = getSFPageTitle();
						report.updateTestLog("Verify New Page is opened", "On Clicking the link:" + strLinkDesc + ", New Page: " + strNewPageTitle + " is opened.", Status.PASS);
						bPageOpened = true;
						break;
					}
					catch (Exception e)
					{
						// e.printStackTrace();
						System.out.println("Issue with New Page" + "new Window is not opened after clicking the link:" + strLinkDesc);
						report.updateTestLog("Verify New Page is opened", "On Clicking the link:" + strLinkDesc + ", New Page: not is opened.", Status.FAIL);
					}
				}
			} // for Loop
		}
		return bPageOpened;
	}// waitForPageToOpenAfterLinkClick

	public int getColNumFromLinkLetTable(String strTableName, String strColName) throws Exception
	{
		int iColNo = -1;
		strColName = strColName.trim();
		String strLinkLetTableXPath = "//td[@class='pbTitle']//*[starts-with(text(),'" + strTableName + "')]//ancestor::table/parent::div/following-sibling::div/table";
		By byHeaderTableCols = By.xpath(strLinkLetTableXPath + "/tbody/tr[@class='headerRow']/th");
		if (isElementVisible(byHeaderTableCols, "HeaderTableCols - " + strTableName))
		{
			List<WebElement> eHeaderTableCols = driver.findElements(byHeaderTableCols);
			int iHeaderTableColsCount = eHeaderTableCols.size();
			for (int iCol = 1; iCol <= iHeaderTableColsCount; iCol++)
			{
				By byHeaderTableCol = By.xpath(strLinkLetTableXPath + "/tbody/tr[@class='headerRow']/th[" + iCol + "]");
				// System.out.println(iCol + "iCol" + strLinkLetTableXPath +
				// "/tbody/tr[@class='headerRow']/th[" + iCol + "]");
				// String strHeaderTableColName = cf.getData(byHeaderTableCol, i
				// + "HeaderTableCol", "title");
				String strHeaderTableColName = getData(byHeaderTableCol, iCol + "HeaderTableCol", "Text");
				if (strHeaderTableColName.equalsIgnoreCase(strColName))
				{
					iColNo = iCol;
					highLightElement(byHeaderTableCol, iCol + "HeaderTableColName: " + strHeaderTableColName);
					break;
				}
			}
		}
		else
		{
			report.updateTestLog("getColNumFromLinkLetTable", "Table:" + strTableName + " not found in the Page.", Status.FAIL);
		}
		return iColNo;
	}

	public int getRowCountFromLinkLetTable(String strTableName) throws Exception
	{
		int iLinkLetTableRowsCount = -1;
		String strLinkLetTableXPath = "//td[@class='pbTitle']//*[starts-with(text(),'" + strTableName + "')]//ancestor::table/parent::div/following-sibling::div/table";
		By byLinkLetTable = By.xpath(strLinkLetTableXPath);
		if (isElementVisible(byLinkLetTable, "LinkLetTable - " + strTableName))
		{
			By byLinkLetTableRows = By.xpath(strLinkLetTableXPath + "/tbody/tr");
			List<WebElement> eTableRows = driver.findElements(byLinkLetTableRows);
			iLinkLetTableRowsCount = eTableRows.size();
			// System.out.println(strTableName + "RowCount: " +
			// iLinkLetTableRowsCount);
			String strRowClass = eTableRows.get(0).getAttribute("class");
			if (strRowClass.equalsIgnoreCase("headerRow"))
			{
				--iLinkLetTableRowsCount;
			}
		} // LinkLetTable present
		return iLinkLetTableRowsCount;
	}// getRowCountFromLinkLetTable method
		// Added By Kusuma

	public int rowscountexpanded(String strTableName) throws Exception
	{
		int iLinkLetTableRowsCount = -1;
		// String strLinkLetTableXPath =
		// "//div[@class='bPageTitle']//*[starts-with(text(),'"+strTableName+"')]/ancestor::td//table";
		String strLinkLetTableXPath = "//div[@class='bPageTitle']//*[starts-with(text(),'" + strTableName + "')]/ancestor::td//table";
		By byLinkLetTable = By.xpath(strLinkLetTableXPath);
		if (isElementVisible(byLinkLetTable, "LinkLetTable - " + strTableName))
		{
			By byLinkLetTableRows = By.xpath(strLinkLetTableXPath + "/tbody/tr");
			List<WebElement> eTableRows = driver.findElements(byLinkLetTableRows);
			iLinkLetTableRowsCount = eTableRows.size();
			// System.out.println(strTableName + "RowCount: " +
			// iLinkLetTableRowsCount);
			String strRowClass = eTableRows.get(0).getAttribute("class");
			if (strRowClass.equalsIgnoreCase("headerRow"))
			{
				--iLinkLetTableRowsCount;
			}
		} // LinkLe
		else
			report.updateTestLog("Related List", "Unable to find related list", Status.FAIL);
		return iLinkLetTableRowsCount;
	}

	public String getClickDataFromTable(String strTableType, String strTableName, String strColName, int iRowNum, String strClickGetSearch) throws Exception
	{
		String strRowColData = null;
		int iColNo = -1;
		String strTableXPath = getTableXPath(strTableType, strTableName);
		By byTable = By.xpath(strTableXPath);
		Boolean bTableFound = false;
		if (isElementVisible(byTable, strTableType + " Table: " + strTableName))
		{
			bTableFound = true;
		}
		else
		{// bTableFound
			report.updateTestLog("get Rowcount From Table: " + strTableName, "<b>" + strTableType + " Table: " + strTableName + " not found in the page." + "</b>", Status.FAIL);
		}
		if (bTableFound)
		{
			// get column num from Header Table
			By byHeaderTableCols = By.xpath(strTableXPath + "/tbody/tr[1]/th");// get
																				// column
																				// names
																				// from
																				// Header
																				// Table
			iColNo = getColNumFromTable(byHeaderTableCols, strTableName, strColName);
			if (iColNo != -1)
			{
				int iTableRowsCount = getRowCountFromTable(strTableType, strTableName) + 1;
				System.out.println(strTableName + "RowCount: " + iTableRowsCount);
				if (iRowNum >= 1 && iRowNum <= iTableRowsCount)
				{
					By byRowColData = By.xpath(strTableXPath + "/tbody/tr[" + (iRowNum) + "]/*[" + iColNo + "]");
					highLightElement(byRowColData, iRowNum + strColName);
					strRowColData = getData(byRowColData, iRowNum + strColName, "Text");
					if (strClickGetSearch.equalsIgnoreCase("Click"))
					{
						By byRowColDataWithLink = By.xpath(strTableXPath + "/tbody/tr[" + (iRowNum) + "]/*[" + iColNo + "]//a");
						if (isElementPresent(byRowColDataWithLink))
						{
							clickByJSE(byRowColDataWithLink, strTableName + " - " + strColName + " - RowColData[" + iRowNum + ":" + iColNo + "]:" + strRowColData);
						}
						else
						{
							clickByJSE(byRowColData, strTableName + " - " + strColName + " - RowColData[" + iRowNum + ":" + iColNo + "]: " + strRowColData);
						}
					}
				}
				else
				{// error
					if (iRowNum < 2)
					{
						report.updateTestLog("getDataFromTable", "Invalid Row no.: " + iRowNum + " No recodrs found in the Table. " + strTableName, Status.FAIL);
					}
					else
					{
						report.updateTestLog("getDataFromTable", "Invalid Row no.: " + iRowNum + ". RowNo is greater than " + iTableRowsCount + " found in the Table: " + strTableName, Status.FAIL);
					}
				} // error
			}
			else
			{
				report.updateTestLog("getDataFromTable", "Column Name:" + strColName + " not found in the Table: " + strTableName, Status.FAIL);
			}
		}
		return strRowColData;
	}// ClickDataFromTable method

	public String clickDataFromLinkLetTable(String strTableName, String strColName, int iRowNum) throws Exception
	{
		String strRowColData = getClickDataFromTable("LinkLet", strTableName, strColName, iRowNum, "Click");
		return strRowColData;
	}// ClickDataFromLinkLetTable method

	public String getDataFromLinkLetTable(String strTableName, String strColName, int iRowNum) throws Exception
	{
		String strRowColData = getClickDataFromTable("LinkLet", strTableName, strColName, iRowNum, "Get");
		return strRowColData;
	}// getDataFromLinkLetTable method

	public String clickLinkFromLinkLetTable(String strTableName, String strColName, String strColData) throws Exception
	{
		// String strRowColData2 =
		// clickOrGetDataFromLinkLetTable(strTableName,strColName, strColData,
		// "","Click");
		String strRowColData2 = verifyGetClickDataFromLinkLetTable(strTableName, strColName, strColData, "", "", "Click");
		return strRowColData2;
	}

	public String clickLinkFromLinkLetTable(String strTableName, String strColName, String strColData, String strColNameDataToRetrive) throws Exception
	{
		// String strRowColData2 =
		// clickOrGetDataFromLinkLetTable(strTableName,strColName, strColData,
		// strColNameDataToRetrive,"Click");
		String strRowColData2 = verifyGetClickDataFromLinkLetTable(strTableName, strColName, strColData, strColNameDataToRetrive, "Click", "Linklet");
		return strRowColData2;
	}

	public String getDataFromLinkLetTable(String strTableName, String strColName, String strColData, String strColNameDataToRetrive) throws Exception
	{
		// String strRowColData2 =
		// clickOrGetDataFromLinkLetTable(strTableName,strColName, strColData,
		// strColNameDataToRetrive,"Get");
		String strRowColData2 = verifyGetClickDataFromLinkLetTable(strTableName, strColName, strColData, strColNameDataToRetrive, "", "Get");
		return strRowColData2;
	}

	public String searchDataFromLinkLetTable(String strTableName, String strColName, String strColData) throws Exception
	{
		// String strRowColData2 =
		// clickOrGetDataFromLinkLetTable(strTableName,strColName, strColData,
		// "","Verify");
		String strRowColData2 = verifyGetClickDataFromLinkLetTable(strTableName, strColName, strColData, "", "", "Search");
		return strRowColData2;
	}

	public String getDataFromLinkLetTable(String strTableName, String strColName, String strColData, String strColNameDataToRetrive, String strColNameDataToRetriveValue) throws Exception
	{
		// String strRowColData2 =
		// clickOrGetDataFromLinkLetTable(strTableName,strColName, strColData,
		// "","Verify");
		String strRowColData2 = verifyGetClickDataFromLinkLetTable(strTableName, strColName, strColData, strColNameDataToRetrive, strColNameDataToRetriveValue, "Get");
		return strRowColData2;
	}

	public String verifyDataFromLinkLetTable(String strTableName, String strColName, String strColData, String strColNameDataToRetrive, String strColNameDataToRetriveValue) throws Exception
	{
		// String strRowColData2 =
		// clickOrGetDataFromLinkLetTable(strTableName,strColName, strColData,
		// "","Verify");
		String strRowColData2 = verifyGetClickDataFromLinkLetTable(strTableName, strColName, strColData, strColNameDataToRetrive, strColNameDataToRetriveValue, "Verify");
		return strRowColData2;
	}

	public String verifyDataFromLinkLetTable_deleteRecord(String strTableName, String strColName, String strColData, String strColNameDataToRetrive, String strColNameDataToRetriveValue) throws Exception
	{
		// String strRowColData2 =
		// clickOrGetDataFromLinkLetTable(strTableName,strColName, strColData,
		// "","Verify");
		String strRowColData2 = verifyGetClickDataFromLinkLetTable_deleteRecord(strTableName, strColName, strColData, strColNameDataToRetrive, strColNameDataToRetriveValue, "Verify");
		return strRowColData2;
	}

	public String verifyDataFromListTable(String strTableName, String strColName, String strColData, String strColNameDataToRetrive, String strColNameDataToRetriveValue) throws Exception
	{
		// String strRowColData2 =
		// clickOrGetDataFromLinkLetTable(strTableName,strColName, strColData,
		// "","Verify");
		String strRowColData2 = verifyGetClickSearchDataFromTable(strTableName, strColName, strColData, "", "", "Verify", "List");
		return strRowColData2;
	}

	public String getDataFromListTable(String strTableName, String strColName, String strColData, String strColNameDataToRetrive, String strColNameDataToRetriveValue) throws Exception
	{
		// String strRowColData2 =
		// clickOrGetDataFromLinkLetTable(strTableName,strColName, strColData,
		// "","Verify");
		String strRowColData2 = verifyGetClickSearchDataFromTable(strTableName, strColName, strColData, "", "", "Get", "List");
		return strRowColData2;
	}

	public String clickDataFromListTable(String strTableName, String strColName, String strColData, String strColNameDataToRetrive, String strColNameDataToRetriveValue) throws Exception
	{
		// String strRowColData2 =
		// clickOrGetDataFromLinkLetTable(strTableName,strColName, strColData,
		// "","Verify");
		String strRowColData2 = verifyGetClickSearchDataFromTable(strTableName, strColName, strColData, "", "", "Click", "List");
		return strRowColData2;
	}

	public String verifyGetClickDataFromLinkLetTable(String strTableName, String strColName, String strColData, String strColNameDataToRetrive, String strColNameDataToRetriveValue, String strClickGetSearch) throws Exception
	{
		String strReturnData = verifyGetClickSearchDataFromTable(strTableName, strColName, strColData, strColNameDataToRetrive, strColNameDataToRetriveValue, strClickGetSearch, "LinkLet");
		return strReturnData;
	}// verifyDataFromLinkLetTable

	public String verifyGetClickDataFromLinkLetTable_deleteRecord(String strTableName, String strColName, String strColData, String strColNameDataToRetrive, String strColNameDataToRetriveValue, String strClickGetSearch) throws Exception
	{
		String strReturnData = verifyGetClickSearchDataFromTable_deleteRecord(strTableName, strColName, strColData, strColNameDataToRetrive, strColNameDataToRetriveValue, strClickGetSearch, "LinkLet");
		return strReturnData;
	}// verifyDataFromLinkLetTable

	public void showLinkLetRecords(String strTableName) throws Exception
	{
		clickSFLinkLet(strTableName);
		while (isSFLinkLetRecordCountMore(strTableName))
		{
			By byShowMore = By.xpath("//td[@class='pbTitle']//*[text()='" + strTableName + "']//ancestor::table/parent::div/following-sibling::div/div[@class='pShowMore']/a[contains(text(),'Show')][contains(text(),'more')]");
			if (isElementVisible(byShowMore, strTableName + " - ShowMore", 5))
			{
				highLightElement(byShowMore, strTableName + " - ShowMore");
				clickByJSE(byShowMore, strTableName + " - ShowMore");
				waitForSeconds(1);
			}
			// isElementNotVisible(byShowMore,strTableName + " - ShowMore",10);
		}
	}// showLinkLetRecords
		/////////////////////////////////////////////////////////////

	public String verifyGetClickSearchDataFromTableOneColumn(String strTableName, String strColName, String strColData, String strClickGetSearch, String strTableType) throws Exception
	{
		String strRowColData = null, strReturnData = null;
		int iRowIndex = -1, iRowNoDataToRetrieve = 0, iRowNoDataToRetrieveIndex = 0;
		if (strColData.startsWith("##"))
		{
			strColData = strColData.replace("##", "");
			try
			{
				iRowNoDataToRetrieve = Integer.parseInt(strColData);
			}
			catch (Exception e)
			{
				// e.printStackTrace();
			}
		}
		String strTableXPath = getTableXPath(strTableType, strTableName);
		By byLinkLetTable = By.xpath(strTableXPath);
		Boolean bTableFound = false;
		if (isElementVisible(byLinkLetTable, "LinkLetTable - " + strTableName))
		{
			bTableFound = true;
		}
		else
		{// bTableFound
			report.updateTestLog(strClickGetSearch + " data in " + strTableName, "<b>" + strTableType + " Table: " + strTableName + " not found in the page." + "</b>", Status.FAIL);
		}
		int iColNo = -1;
		// By byHeaderTableCols = By.xpath(strLinkLetTableXPath +
		// "/tbody/tr[@class='headerRow']/th");//get column names from Header
		// Table
		By byHeaderTableCols = By.xpath(strTableXPath + "/tbody/tr[1]/th");// get
																			// column
																			// names
																			// from
																			// Header
																			// Table
		if (bTableFound)
		{
			//////////// Show more ///////////////////////////////////
			if (strTableType.equalsIgnoreCase("LinkLet"))
			{
				scrollToElement(byLinkLetTable, "LinkLetTable: " + strTableName, 200);
				if (isSFLinkLetRecordCountMore(strTableName))
				{
					By byShowMore = By.xpath("//td[@class='pbTitle']//*[text()='" + strTableName + "']//ancestor::table/parent::div/following-sibling::div/div[@class='pShowMore']/a[contains(text(),'Show')][contains(text(),'more')]");
					clickLink(byShowMore, strTableName + " - ShowMore");
					isElementNotVisible(byShowMore, strTableName + " - ShowMore", 10);
				}
			}
			iColNo = getColNumFromTable(byHeaderTableCols, strTableName, strColName);
			if (iColNo != -1)
			{
				By byTableRows = By.xpath(strTableXPath + "/tbody/tr");
				Boolean bRowCol_Found = false;
				int iTableRowsCount = getRowcountFromTable(byTableRows, "TableRows");
				if (strTableType.equalsIgnoreCase("Calendar"))
				{
					// Row count includes header row also in the table
				}
				else
				{
					iTableRowsCount++; /// Row count includes data row only and
										/// so + 1 required
				}
				for (int iRow = 2; iRow <= iTableRowsCount; iRow++)
				{ // data starts from 2nd row
					By byRowColData = By.xpath(strTableXPath + "/tbody/tr[" + iRow + "]/*[" + iColNo + "]");
					strRowColData = getData(byRowColData, iRow + strColName, "Text");
					// highLightElement(byRowColData, iRow + strColName);
					if (strColData == null || strColData.isEmpty() || strColData.equalsIgnoreCase(""))
					{
						// return the first matching data
						iRowIndex = iRow;
						scrollToElement(byRowColData, strRowColData, 200);
						report.updateTestLog(strClickGetSearch + " data in " + strTableName, "<b>[" + strColName + ": " + strColData + "] found in the Row#: " + iRowIndex + "<b> is found in the Table: " + strTableName + "</b>", Status.PASS);
						strReturnData = strRowColData;
						bRowCol_Found = true;
						/// break;
					}
					else
					{// strColName is not empty
						++iRowNoDataToRetrieveIndex;
						if (iRowNoDataToRetrieve >= 1 && iRowNoDataToRetrieve == iRowNoDataToRetrieveIndex)
						{
							iRowIndex = iRowNoDataToRetrieveIndex + 1; // Since
																		// 1st
																		// row
																		// is
																		// Header
																		// row
							scrollToElement(byRowColData, strRowColData, 200);
							report.updateTestLog(strClickGetSearch + " data in " + strTableName, "<b>[" + strColName + ": " + strColData + "] found in the Row#: " + iRowIndex + " in the Table: " + strTableName + "</b>", Status.PASS);
							strReturnData = strRowColData;
							bRowCol_Found = true;
							// break;
						}
						else if (strRowColData.equalsIgnoreCase(strColData))
						{
							iRowIndex = iRow;
							strReturnData = strRowColData;
							scrollToElement(byRowColData, strRowColData, 200);
							report.updateTestLog(strClickGetSearch + " data in " + strTableName, "<b>[" + strColName + ": " + strColData + "] found in the Row#: " + iRowIndex + " <b> is found in the Table: " + strTableName + "</b>", Status.PASS);
							bRowCol_Found = true;
						}
					} //// strColName is not empty
					if (bRowCol_Found)
					{
						if (strClickGetSearch.equalsIgnoreCase("Click"))
						{
							By byRowColDataLink = By.xpath(strTableXPath + "/tbody/tr[" + iRowIndex + "]/*[" + iColNo + "]//a");
							if (isElementPresent(byRowColDataLink))
							{
								highLightElement(byRowColDataLink, "RowColDataLink");
								clickByJSE(byRowColDataLink, strTableName + " - " + strColName + " - RowColData[" + iRowIndex + ":" + iColNo + "]:" + strRowColData);
							}
							else
							{
								clickByJSE(byRowColData, strTableName + " - " + strColName + " - RowColData[" + iRowIndex + ":" + iColNo + "]: " + strRowColData);
							}
						} // if Click
						break;
					} // bRowCol_Found
				} // for loop
				if (!strClickGetSearch.equalsIgnoreCase("Get"))
				{
					if (iRowIndex > 1)
					{// data starts from 2nd row
						if (strReturnData == null && !strClickGetSearch.equalsIgnoreCase("Search"))
						{// if search or Get just return null or data only
							if (!bRowCol_Found)
							{
								report.updateTestLog(strClickGetSearch + " data in " + strTableName, "<b>[" + strColName + ": " + strColData + "] not found in the Table: " + strTableName + "</b>", Status.FAIL);
							}
						}
					}
					else
					{
						if (iTableRowsCount == 0)
						{
							report.updateTestLog(strClickGetSearch + " data in " + strTableName, "<b> No data not found in the Table: " + strTableName + "</b>", Status.FAIL);
						}
						else
						{
							report.updateTestLog(strClickGetSearch + " data in " + strTableName, "<b>" + strColName + ":" + strColData + " data not found in the Table: " + strTableName + "</b>", Status.FAIL);
						}
					} // (iRowIndex > 0)
				} // if(!strClickGetSearch.equalsIgnoreCase("Get")){
			}
			else
			{
				report.updateTestLog(strClickGetSearch + " data in " + strTableName, "<b>Column Name:" + strColName + " not found in the Table: " + strTableName + "</b>", Status.FAIL);
			} // (iColNo != -1)
		} // bTableFound
		return strReturnData;
	}// verifyGetClickSearchDataFromTable

	public String selectCheckBoxDataFromTableOneColumn(String strTableName, String strColName, String strColData, String strClickGetSearch, String strTableType) throws Exception
	{
		String strRowColData = null, strReturnData = null;
		int iRowIndex = -1, iRowNoDataToRetrieve = 0, iRowNoDataToRetrieveIndex = 0;
		if (strColData.startsWith("##"))
		{
			strColData = strColData.replace("##", "");
			try
			{
				iRowNoDataToRetrieve = Integer.parseInt(strColData);
			}
			catch (Exception e)
			{
				// e.printStackTrace();
			}
		}
		String strTableXPath = getTableXPath(strTableType, strTableName);
		By byLinkLetTable = By.xpath(strTableXPath);
		Boolean bTableFound = false;
		if (isElementVisible(byLinkLetTable, "LinkLetTable - " + strTableName))
		{
			bTableFound = true;
		}
		else
		{// bTableFound
			report.updateTestLog(strClickGetSearch + " data in " + strTableName, "<b>" + strTableType + " Table: " + strTableName + " not found in the page." + "</b>", Status.FAIL);
		}
		int iColNo = -1;
		By byHeaderTableCols = By.xpath(strTableXPath + "/tbody/tr[@class='headerRow']/th");// get
																							// column
																							// names
																							// from
																							// Header
																							// Table
		if (bTableFound)
		{
			//////////// Show more ///////////////////////////////////
			if (strTableType.equalsIgnoreCase("LinkLet"))
			{
				scrollToElement(byLinkLetTable, "LinkLetTable: " + strTableName, 200);
				if (isSFLinkLetRecordCountMore(strTableName))
				{
					By byShowMore = By.xpath("//td[@class='pbTitle']//*[text()='" + strTableName + "']//ancestor::table/parent::div/following-sibling::div/div[@class='pShowMore']/a[contains(text(),'Show')][contains(text(),'more')]");
					clickLink(byShowMore, strTableName + " - ShowMore");
					isElementNotVisible(byShowMore, strTableName + " - ShowMore", 10);
				}
			}
			iColNo = getColNumFromTable(byHeaderTableCols, strTableName, strColName);
			if (iColNo != -1)
			{
				By byLinkLetTableRows = By.xpath(strTableXPath + "/tbody/tr");
				Boolean bRowCol_Found = false;
				int iLinkLetTableRowsCount = getRowcountFromTable(byLinkLetTableRows, "LinkLetTableRows") + 1; /// since
																												/// header
																												/// row
																												/// also
																												/// included
																												/// in
																												/// the
																												/// table
				for (int iRow = 2; iRow <= iLinkLetTableRowsCount; iRow++)
				{ // data starts from 2nd row
					By byRowColData = By.xpath(strTableXPath + "/tbody/tr[" + iRow + "]/*[" + iColNo + "]");
					strRowColData = getData(byRowColData, iRow + strColName, "Text");
					// highLightElement(byRowColData, iRow + strColName);
					if (strColData == null || strColData.isEmpty() || strColData.equalsIgnoreCase(""))
					{
						// return the first matching data
						report.updateTestLog(strClickGetSearch + " data from " + strTableName, "<b>[" + strColName + ": " + strColData + "] found in the Row#: " + iRowIndex + "<b> is found in the Table: " + strTableName + "</b>", Status.PASS);
						strReturnData = strRowColData;
						bRowCol_Found = true;
						iRowIndex = iRow;
						/// break;
					}
					else
					{// strColName is not empty
						++iRowNoDataToRetrieveIndex;
						if (iRowNoDataToRetrieve >= 1 && iRowNoDataToRetrieve == iRowNoDataToRetrieveIndex)
						{
							report.updateTestLog(strClickGetSearch + " data from " + strTableName, "<b>[" + strColName + ": " + strColData + "] found in the Row#: " + iRowIndex + " in the Table: " + strTableName + "</b>", Status.PASS);
							strReturnData = strRowColData;
							bRowCol_Found = true;
							iRowIndex = iRowNoDataToRetrieveIndex + 1; // Since
																		// 1st
																		// row
																		// is
																		// Header
																		// row
																		// break;
						}
						else if (strRowColData.equalsIgnoreCase(strColData))
						{
							report.updateTestLog(strClickGetSearch + " data from " + strTableName, "<b>[" + strColName + ": " + strColData + "] found in the Row#: " + iRowIndex + " <b> is found in the Table: " + strTableName + "</b>", Status.PASS);
							bRowCol_Found = true;
						}
					} //// strColName is not empty
					if (bRowCol_Found)
					{
						if (strClickGetSearch.equalsIgnoreCase("Click"))
						{
							By byRowColDataLink = By.xpath(strTableXPath + "/tbody/tr[" + iRowIndex + "]/*[" + iColNo + "]//a");
							if (isElementPresent(byRowColDataLink))
							{
								clickByJSE(byRowColDataLink, strTableName + " - " + strColName + " - RowColData[" + iRowIndex + ":" + iColNo + "]:" + strRowColData);
							}
							else
							{
								clickByJSE(byRowColData, strTableName + " - " + strColName + " - RowColData[" + iRowIndex + ":" + iColNo + "]: " + strRowColData);
							}
						}
						else if (strClickGetSearch.equalsIgnoreCase("Checkbox") || strClickGetSearch.equalsIgnoreCase("Select"))
						{
							By byRowColDataCheckbox = By.xpath(strTableXPath + "/tbody/tr[" + iRowIndex + "]/*[1]//input[@type='checkbox']");
							if (isElementPresent(byRowColDataCheckbox))
							{
								clickByJSE(byRowColDataCheckbox, strTableName + " - " + strColName + " - RowColData[" + iRowIndex + ":" + iColNo + "]:" + strRowColData);
							}
							else
							{
								report.updateTestLog(strClickGetSearch + " data from " + strTableName, "<b>[" + strColName + ": " + strColData + "] - checkbox not found in the Table: " + strTableName + "</b>", Status.FAIL);
							}
						}
						break;
					} // bRowCol_Found
				} // for loop
				if (!strClickGetSearch.equalsIgnoreCase("Get"))
				{
					if (iRowIndex > 1)
					{// data starts from 2nd row
						if (strReturnData == null && !strClickGetSearch.equalsIgnoreCase("Search"))
						{// if search or Get just return null or data only
							if (!bRowCol_Found)
							{
								report.updateTestLog(strClickGetSearch + " data from " + strTableName, "<b>[" + strColName + ": " + strColData + "] not found in the Table: " + strTableName + "</b>", Status.FAIL);
							}
						}
					}
					else
					{
						if (iLinkLetTableRowsCount == 0)
						{
							report.updateTestLog(strClickGetSearch + " data from " + strTableName, "<b> No data not found in the Table: " + strTableName + "</b>", Status.FAIL);
						}
						else
						{
							report.updateTestLog(strClickGetSearch + " data from " + strTableName, "<b>" + strColName + ":" + strColData + " data not found in the Table: " + strTableName + "</b>", Status.FAIL);
						}
					} // (iRowIndex > 0)
				} // if(!strClickGetSearch.equalsIgnoreCase("Get")){
			}
			else
			{
				report.updateTestLog(strClickGetSearch + " data from " + strTableName, "<b>Column Name:" + strColName + " not found in the Table: " + strTableName + "</b>", Status.FAIL);
			} // (iColNo != -1)
		} // bTableFound
		return strReturnData;
	}// verifyGetClickSearchDataFromTable

	public int getRowNumDataFromTable(String strTableType, String strTableName, String strColName, String strColData) throws Exception
	{
		String strRowColData = null;
		int iReturnRowNum = -1;
		String strTableXPath = getTableXPath(strTableType, strTableName);
		By byTable = By.xpath(strTableXPath);
		Boolean bTableFound = false;
		if (isElementVisible(byTable, strTableType + " Table: " + strTableName))
		{
			bTableFound = true;
		}
		else
		{// bTableFound
			report.updateTestLog("Get Row # from " + strTableName, "<b>" + strTableType + " Table: " + strTableName + " not found in the page." + "</b>", Status.FAIL);
		}
		int iColNo = -1;
		By byHeaderTableCols = By.xpath(strTableXPath + "/tbody/tr[@class='headerRow']/th");// get
																							// column
																							// names
																							// from
																							// Header
																							// Table
		if (bTableFound)
		{
			//////////// Show more ///////////////////////////////////
			expandLinkLetTable(strTableName);
			iColNo = getColNumFromTable(byHeaderTableCols, strTableName, strColName);
			if (iColNo != -1)
			{
				By byTableRows = By.xpath(strTableXPath + "/tbody/tr");
				int iTableRowsCount = getRowcountFromTable(byTableRows, strTableType + " Table: " + strTableName) + 1; /// since
																														/// header
																														/// row
																														/// also
																														/// included
																														/// in
																														/// the
																														/// table
				for (int iRow = 2; iRow <= iTableRowsCount; iRow++)
				{ // data starts from 2nd row
					By byRowColData = By.xpath(strTableXPath + "/tbody/tr[" + iRow + "]/*[" + iColNo + "]");
					strRowColData = getData(byRowColData, iRow + strColName, "text");
					if (strRowColData.equalsIgnoreCase(strColData))
					{
						highLightElement(byRowColData, iRow + strColName);
						iReturnRowNum = iRow;
						break;
					}
				} // for loop
				if (iReturnRowNum > 1)
				{
					report.updateTestLog("Get Row # from " + strTableType + " Table: " + strTableName, "<b>[" + strColName + ": " + strColData + "] found in Row #: " + iReturnRowNum + " the Table: " + strTableName + "</b>", Status.DONE);
				}
				else
				{
					report.updateTestLog("Get Row # from " + strTableType + " Table: " + strTableName, "<b>[" + strColName + ": " + strColData + "] not found in the Table: " + strTableName + "</b>", Status.FAIL);
				}
			}
			else
			{
				report.updateTestLog("Get Row # from " + strTableType + " Table: " + strTableName, "<b>Column Name:" + strColName + " not found in the Table: " + strTableName + "</b>", Status.FAIL);
			} // (iColNo != -1)
		} // bTableFound
		return iReturnRowNum;
	}//

	public int getRowNumCheckBoxDataFromLinkLetTable(String strTableName, String strColName, String strColData) throws Exception
	{
		String strRowColData = null;
		int iReturnRowNum = -1;
		String strTableXPath = getTableXPath("LinkLet", strTableName);
		By byLinkLetTable = By.xpath(strTableXPath);
		Boolean bTableFound = false;
		if (isElementVisible(byLinkLetTable, "LinkLetTable - " + strTableName))
		{
			bTableFound = true;
		}
		else
		{// bTableFound
			report.updateTestLog("Get Row # from " + strTableName, "<b>" + " LinkLet Table: " + strTableName + " not found in the page." + "</b>", Status.FAIL);
		}
		int iColNo = -1;
		By byHeaderTableCols = By.xpath(strTableXPath + "/tbody/tr[@class='headerRow']/th");// get
																							// column
																							// names
																							// from
																							// Header
																							// Table
		if (bTableFound)
		{
			//////////// Show more ///////////////////////////////////
			expandLinkLetTable(strTableName);
			iColNo = getColNumFromTable(byHeaderTableCols, strTableName, strColName);
			if (iColNo != -1)
			{
				By byLinkLetTableRows = By.xpath(strTableXPath + "/tbody/tr");
				int iLinkLetTableRowsCount = getRowcountFromTable(byLinkLetTableRows, "LinkLetTableRows") + 1; /// since
																												/// header
																												/// row
																												/// also
																												/// included
																												/// in
																												/// the
																												/// table
				for (int iRow = 2; iRow <= iLinkLetTableRowsCount; iRow++)
				{ // data starts from 2nd row
					By byRowColData = By.xpath(strTableXPath + "/tbody/tr[" + iRow + "]/*[" + iColNo + "]//img");
					strRowColData = getData(byRowColData, iRow + strColName, "title");
					// highLightElement(byRowColData, iRow + strColName);
					if (strRowColData.equalsIgnoreCase(strColData))
					{
						iReturnRowNum = iRow;
						break;
					}
				} // for loop
				if (iReturnRowNum > 0)
				{
					report.updateTestLog("Get Row # from " + strTableName, "<b>[" + strColName + ": " + strColData + "] found in Row #: " + iReturnRowNum + " the Table: " + strTableName + "</b>", Status.DONE);
				}
				else
				{
					report.updateTestLog("Get Row # from " + strTableName, "<b>[" + strColName + ": " + strColData + "] not found in the Table: " + strTableName + "</b>", Status.FAIL);
				}
			}
			else
			{
				report.updateTestLog("Get Row # from " + strTableName, "<b>Column Name:" + strColName + " not found in the Table: " + strTableName + "</b>", Status.FAIL);
			} // (iColNo != -1)
		} // bTableFound
		return iReturnRowNum;
	}//

	public String verifyGetClickSearchDataFromTable(String strTableName, String strColName, String strColData, String strColNameDataToRetrive, String strColNameDataToRetriveValue, String strClickGetSearch, String strTableType) throws Exception
	{
		String strRowColData = null, strRowColData2 = null, strReturnData = null;
		int iRowIndex = -1, iRowNoDataToRetrieve = 0, iRowNoDataToRetrieveIndex = 0;
		if (strColNameDataToRetriveValue.startsWith("##"))
		{
			strColNameDataToRetriveValue = strColNameDataToRetriveValue.replace("##", "");
			try
			{
				iRowNoDataToRetrieve = Integer.parseInt(strColNameDataToRetriveValue);
			}
			catch (Exception e)
			{
				// e.printStackTrace();
			}
		}
		String strTableXPath = getTableXPath(strTableType, strTableName);
		By byTable = By.xpath(strTableXPath);
		Boolean bTableFound = false;
		if (isElementVisible(byTable, strTableType + " Table - " + strTableName))
		{
			bTableFound = true;
		}
		else
		{// bTableFound
			report.updateTestLog(strClickGetSearch + " data in " + strTableName, "<b>" + strTableType + " Table: " + strTableName + " not found in the page." + "</b>", Status.FAIL);
		}
		int iColNo = -1;
		// By byHeaderTableCols = By.xpath(strTableXPath +
		// "/tbody/tr[@class='headerRow']/th");//get column names from Header
		// Table
		By byHeaderTableCols = By.xpath(strTableXPath + "/tbody/tr[1]/th");// get
																			// column
																			// names
																			// from
																			// Header
																			// Table
		if (bTableFound)
		{
			scrollToElement(byTable, strTableType + " Table: " + strTableName, 200);
			//////////// Show more ///////////////////////////////////
			if (strTableType.equalsIgnoreCase("LinkLet"))
			{
				Boolean bShowMore = true;
				while (bShowMore)
				{
					if (isSFLinkLetRecordCountMore(strTableName))
					{
						By byShowMore = By.xpath("//td[@class='pbTitle']//*[text()='" + strTableName + "']//ancestor::table/parent::div/following-sibling::div/div[@class='pShowMore']/a[contains(text(),'Show')][contains(text(),'more')]");
						clickLink(byShowMore, strTableName + " - ShowMore");
						// isElementNotVisible(byShowMore,strTableName + " -
						// ShowMore",10);
						waitForSeconds(3);
					}
					else
					{
						bShowMore = false;
					}
				}
			}
			// iColNo = getColNumFromLinkLetTable(strTableName, strColName);
			iColNo = getColNumFromTable(byHeaderTableCols, strTableName, strColName);
			if (iColNo != -1)
			{
				By byTableRows = By.xpath(strTableXPath + "/tbody/tr");
				// List<WebElement> eLinkLetTableRows =
				// driver.findElements(byLinkLetTableRows);
				Boolean bRowCol1_Found = false, bRowCol2_Found = false;
				int iTableRowsCount = getRowcountFromTable(byTableRows, "LinkLetTableRows"); /// since
																								/// header
																								/// row
																								/// also
																								/// included
																								/// in
																								/// the
																								/// table
				if (strTableType.equalsIgnoreCase("Calendar") || strTableType.equalsIgnoreCase("Search"))
				{
					// Row count includes header row also in the table
				}
				else
				{
					iTableRowsCount++; /// Row count includes data row only and
										/// so + 1 required
				}
				for (int iRow = 2; iRow <= iTableRowsCount; iRow++)
				{ // data starts from 2nd row
					By byRowColData = By.xpath(strTableXPath + "/tbody/tr[" + iRow + "]/*[" + iColNo + "]");
					strRowColData = getData(byRowColData, iRow + strColName, "Text");
					if (strRowColData.equalsIgnoreCase(strColData))
					{
						bRowCol1_Found = true;
						iRowIndex = iRow;
						strRowColData2 = strColData;
						strReturnData = strColData;
						highLightElement(byRowColData, iRow + strColName);
						if (strColNameDataToRetrive == null || strColNameDataToRetrive.isEmpty() || strColNameDataToRetrive.equalsIgnoreCase(""))
						{
							report.updateTestLog(strClickGetSearch + " data in " + strTableName, "<b>[" + strColName + ": " + strColData + "] found in the Row#: " + iRowIndex + " from the Table: " + strTableName + "</b>", Status.PASS);
							System.out.println(strClickGetSearch + " data in " + strTableName + ", [" + strColName + ": " + strColData + "] found in the Row#: " + iRowIndex + " from the Table: " + strTableName);
							if (strClickGetSearch.equalsIgnoreCase("Click"))
							{
								By byRowColDataWithLink = By.xpath(strTableXPath + "/tbody/tr[" + iRow + "]/*[" + iColNo + "]//a");
								if (isElementPresent(byRowColDataWithLink))
								{
									clickByJSE(byRowColDataWithLink, strTableName + " - " + strColName + " - RowColData[" + iRowIndex + ":" + iColNo + "]:" + strColData);
								}
								else
								{
									clickByJSE(byRowColData, strTableName + " - " + strColName + " - RowColData[" + iRowIndex + ":" + iColNo + "]: " + strColData);
								}
							} // if Click
							break;
						}
						else
						{// strColNameDataToRetrive is not Empty
							strRowColData2 = null;
							strReturnData = null;
							int iColNo2 = -1;
							// iColNo2 = getColNumFromLinkLetTable(strTableName,
							// strColNameDataToRetrive);
							iColNo2 = getColNumFromTable(byHeaderTableCols, strTableName, strColNameDataToRetrive);
							if (iColNo2 != -1)
							{
								By byRowColData2 = By.xpath(strTableXPath + "/tbody/tr[" + iRowIndex + "]/*[" + iColNo2 + "]");
								highLightElement(byRowColData2, iRowIndex + strColName);
								strRowColData2 = getData(byRowColData2, iRowIndex + strColName, "Text");
								if (strColNameDataToRetriveValue == null || strColNameDataToRetriveValue.isEmpty() || strColNameDataToRetriveValue.equalsIgnoreCase(""))
								{
									// return the first matching data
									report.updateTestLog(strClickGetSearch + " data in " + strTableName, "<b>[" + strColName + ": " + strColData + "] found in the Row#: " + iRowIndex + " and its corresponding [" + strColNameDataToRetrive + ": " + strRowColData2 + "]<b> is found in the Table: " + strTableName + "</b>", Status.PASS);
									strReturnData = strRowColData2;
									bRowCol2_Found = true;
									// break;
								}
								else
								{// strColNameDataToRetriveValue is not empty
									++iRowNoDataToRetrieveIndex;
									if (iRowNoDataToRetrieve >= 1 && iRowNoDataToRetrieve == iRowNoDataToRetrieveIndex)
									{
										report.updateTestLog(strClickGetSearch + " data in " + strTableName, "<b>[" + strColName + ": " + strColData + "] found in the Row#: " + iRowIndex + " and its corresponding [" + strColNameDataToRetrive + ": " + strRowColData2 + "]<b> is found in the Table: " + strTableName + "</b>", Status.PASS);
										strReturnData = strRowColData2;
										bRowCol2_Found = true;
										// break;
									}
									else if (strRowColData2.equalsIgnoreCase(strColNameDataToRetriveValue))
									{
										strReturnData = strRowColData2;
										report.updateTestLog(strClickGetSearch + " data in " + strTableName, "<b>[" + strColName + ": " + strColData + "] found in the Row#: " + iRowIndex + " and its corresponding [" + strColNameDataToRetrive + ": " + strColNameDataToRetriveValue + "]<b> is found in the Table: " + strTableName + "</b>", Status.PASS);
										bRowCol2_Found = true;
									} // (strRowColData2.equalsIgnoreCase(strColNameDataToRetriveValue)
								} //// strColNameDataToRetriveValue is not empty
								if (bRowCol2_Found)
								{
									if (strClickGetSearch.equalsIgnoreCase("Click"))
									{
										By byRowColDataLink = By.xpath(strTableXPath + "/tbody/tr[" + iRowIndex + "]/*[" + iColNo2 + "]//a");
										if (isElementPresent(byRowColDataLink))
										{
											clickByJSE(byRowColDataLink, strTableName + " - " + strColNameDataToRetrive + " - RowColData[" + iRowIndex + ":" + iColNo2 + "]:" + strRowColData2);
										}
										else
										{
											clickByJSE(byRowColData2, strTableName + " - " + strColNameDataToRetrive + " - RowColData[" + iRowIndex + ":" + iColNo2 + "]: " + strRowColData2);
										}
									} // if Click
									break;
								} // bRowCol2_Found
							}
							else
							{// (iColNo2 != -1)
								report.updateTestLog(strClickGetSearch + " data in " + strTableName, "<b>Column Name:" + strColNameDataToRetrive + " not found in the Table: " + strTableName + "</b>", Status.FAIL);
							}
						} // strColNameDataToRetrive is not empty
					} // strRowColData.equalsIgnoreCase(strColData)
				} // for loop
				if (!strClickGetSearch.equalsIgnoreCase("Get"))
				{
					if (iRowIndex > 1)
					{// data starts from 2nd row
						if (strReturnData == null && !strClickGetSearch.equalsIgnoreCase("Search"))
						{// if search or Get just return null or data only
							if (bRowCol1_Found && !bRowCol2_Found)
							{
								report.updateTestLog(strClickGetSearch + " data in " + strTableName, "<b>[" + strColName + ": " + strColData + "] found in the Row#: " + iRowIndex + " and but its corresponding [" + strColNameDataToRetrive + ": " + strRowColData2 + "]<b> is not found in the Table: " + strTableName + "</b>", Status.FAIL);
							}
							else
							{
								report.updateTestLog(strClickGetSearch + " data in " + strTableName, "<b>[" + strColName + ": " + strColData + "] not found in the Table: " + strTableName + "</b>", Status.FAIL);
							}
						}
					}
					else
					{
						if (iTableRowsCount == 0)
						{
							report.updateTestLog(strClickGetSearch + " data in " + strTableName, "<b> No data not found in the Table: " + strTableName + "</b>", Status.FAIL);
						}
						else
						{
							report.updateTestLog(strClickGetSearch + " data in " + strTableName, "<b>" + strColName + ":" + strColData + " data not found in the Table: " + strTableName + "</b>", Status.FAIL);
						}
					} // (iRowIndex > 0)
				} // if(!strClickGetSearch.equalsIgnoreCase("Get")){
			}
			else
			{
				report.updateTestLog(strClickGetSearch + " data in " + strTableName, "<b>Column Name:" + strColName + " not found in the Table: " + strTableName + "</b>", Status.FAIL);
			} // (iColNo != -1)
		} // bTableFound
		return strReturnData;
	}// verifyDataFromLinkLetTable
		/////////////////////////
		/* To check if a deleted record is not present in the related list */

	public String verifyGetClickSearchDataFromTable_deleteRecord(String strTableName, String strColName, String strColData, String strColNameDataToRetrive, String strColNameDataToRetriveValue, String strClickGetSearch, String strTableType) throws Exception
	{
		String strRowColData = null, strRowColData2 = null, strReturnData = null;
		int iRowIndex = -1, iRowNoDataToRetrieve = 0, iRowNoDataToRetrieveIndex = 0;
		if (strColNameDataToRetriveValue.startsWith("##"))
		{
			strColNameDataToRetriveValue = strColNameDataToRetriveValue.replace("##", "");
			try
			{
				iRowNoDataToRetrieve = Integer.parseInt(strColNameDataToRetriveValue);
			}
			catch (Exception e)
			{
				// e.printStackTrace();
			}
		}
		String strTableXPath = getTableXPath(strTableType, strTableName);
		By byTable = By.xpath(strTableXPath);
		Boolean bTableFound = false;
		if (isElementVisible(byTable, strTableType + " Table - " + strTableName))
		{
			bTableFound = true;
		}
		else
		{// bTableFound
			report.updateTestLog(strClickGetSearch + " data in " + strTableName, "<b>" + strTableType + " Table: " + strTableName + " not found in the page." + "</b>", Status.FAIL);
		}
		int iColNo = -1;
		// By byHeaderTableCols = By.xpath(strTableXPath +
		// "/tbody/tr[@class='headerRow']/th");//get column names from Header
		// Table
		By byHeaderTableCols = By.xpath(strTableXPath + "/tbody/tr[1]/th");// get
																			// column
																			// names
																			// from
																			// Header
																			// Table
		if (bTableFound)
		{
			scrollToElement(byTable, strTableType + " Table: " + strTableName, 200);
			//////////// Show more ///////////////////////////////////
			if (strTableType.equalsIgnoreCase("LinkLet"))
			{
				Boolean bShowMore = true;
				while (bShowMore)
				{
					if (isSFLinkLetRecordCountMore(strTableName))
					{
						By byShowMore = By.xpath("//td[@class='pbTitle']//*[text()='" + strTableName + "']//ancestor::table/parent::div/following-sibling::div/div[@class='pShowMore']/a[contains(text(),'Show')][contains(text(),'more')]");
						clickLink(byShowMore, strTableName + " - ShowMore");
						// isElementNotVisible(byShowMore,strTableName + " -
						// ShowMore",10);
						waitForSeconds(3);
					}
					else
					{
						bShowMore = false;
					}
				}
			}
			// iColNo = getColNumFromLinkLetTable(strTableName, strColName);
			iColNo = getColNumFromTable_deleteRecord(byHeaderTableCols, strTableName, strColName);
			if (iColNo != -1)
			{
				By byTableRows = By.xpath(strTableXPath + "/tbody/tr");
				// List<WebElement> eLinkLetTableRows =
				// driver.findElements(byLinkLetTableRows);
				Boolean bRowCol1_Found = false, bRowCol2_Found = false;
				int iTableRowsCount = getRowcountFromTable(byTableRows, "LinkLetTableRows"); /// since
																								/// header
																								/// row
																								/// also
																								/// included
																								/// in
																								/// the
																								/// table
				if (strTableType.equalsIgnoreCase("Calendar") || strTableType.equalsIgnoreCase("Search"))
				{
					// Row count includes header row also in the table
				}
				else
				{
					iTableRowsCount++; /// Row count includes data row only and
										/// so + 1 required
				}
				for (int iRow = 2; iRow <= iTableRowsCount; iRow++)
				{ // data starts from 2nd row
					By byRowColData = By.xpath(strTableXPath + "/tbody/tr[" + iRow + "]/*[" + iColNo + "]");
					strRowColData = getData(byRowColData, iRow + strColName, "Text");
					if (strRowColData.equalsIgnoreCase(strColData))
					{
						bRowCol1_Found = true;
						iRowIndex = iRow;
						strRowColData2 = strColData;
						strReturnData = strColData;
						highLightElement(byRowColData, iRow + strColName);
						if (strColNameDataToRetrive == null || strColNameDataToRetrive.isEmpty() || strColNameDataToRetrive.equalsIgnoreCase(""))
						{
							report.updateTestLog(strClickGetSearch + " data in " + strTableName, "<b>[" + strColName + ": " + strColData + "] found in the Row#: " + iRowIndex + " from the Table: " + strTableName + "</b>", Status.PASS);
							System.out.println(strClickGetSearch + " data in " + strTableName + ", [" + strColName + ": " + strColData + "] found in the Row#: " + iRowIndex + " from the Table: " + strTableName);
							if (strClickGetSearch.equalsIgnoreCase("Click"))
							{
								By byRowColDataWithLink = By.xpath(strTableXPath + "/tbody/tr[" + iRow + "]/*[" + iColNo + "]//a");
								if (isElementPresent(byRowColDataWithLink))
								{
									clickByJSE(byRowColDataWithLink, strTableName + " - " + strColName + " - RowColData[" + iRowIndex + ":" + iColNo + "]:" + strColData);
								}
								else
								{
									clickByJSE(byRowColData, strTableName + " - " + strColName + " - RowColData[" + iRowIndex + ":" + iColNo + "]: " + strColData);
								}
							} // if Click
							break;
						}
						else
						{// strColNameDataToRetrive is not Empty
							strRowColData2 = null;
							strReturnData = null;
							int iColNo2 = -1;
							// iColNo2 = getColNumFromLinkLetTable(strTableName,
							// strColNameDataToRetrive);
							iColNo2 = getColNumFromTable_deleteRecord(byHeaderTableCols, strTableName, strColNameDataToRetrive);
							if (iColNo2 != -1)
							{
								By byRowColData2 = By.xpath(strTableXPath + "/tbody/tr[" + iRowIndex + "]/*[" + iColNo2 + "]");
								highLightElement(byRowColData2, iRowIndex + strColName);
								strRowColData2 = getData(byRowColData2, iRowIndex + strColName, "Text");
								if (strColNameDataToRetriveValue == null || strColNameDataToRetriveValue.isEmpty() || strColNameDataToRetriveValue.equalsIgnoreCase(""))
								{
									// return the first matching data
									report.updateTestLog(strClickGetSearch + " data in " + strTableName, "<b>[" + strColName + ": " + strColData + "] found in the Row#: " + iRowIndex + " and its corresponding [" + strColNameDataToRetrive + ": " + strRowColData2 + "]<b> is found in the Table: " + strTableName + "</b>", Status.PASS);
									strReturnData = strRowColData2;
									bRowCol2_Found = true;
									// break;
								}
								else
								{// strColNameDataToRetriveValue is not empty
									++iRowNoDataToRetrieveIndex;
									if (iRowNoDataToRetrieve >= 1 && iRowNoDataToRetrieve == iRowNoDataToRetrieveIndex)
									{
										report.updateTestLog(strClickGetSearch + " data in " + strTableName, "<b>[" + strColName + ": " + strColData + "] found in the Row#: " + iRowIndex + " and its corresponding [" + strColNameDataToRetrive + ": " + strRowColData2 + "]<b> is found in the Table: " + strTableName + "</b>", Status.PASS);
										strReturnData = strRowColData2;
										bRowCol2_Found = true;
										// break;
									}
									else if (strRowColData2.equalsIgnoreCase(strColNameDataToRetriveValue))
									{
										strReturnData = strRowColData2;
										report.updateTestLog(strClickGetSearch + " data in " + strTableName, "<b>[" + strColName + ": " + strColData + "] found in the Row#: " + iRowIndex + " and its corresponding [" + strColNameDataToRetrive + ": " + strColNameDataToRetriveValue + "]<b> is found in the Table: " + strTableName + "</b>", Status.PASS);
										bRowCol2_Found = true;
									} // (strRowColData2.equalsIgnoreCase(strColNameDataToRetriveValue)
								} //// strColNameDataToRetriveValue is not empty
								if (bRowCol2_Found)
								{
									if (strClickGetSearch.equalsIgnoreCase("Click"))
									{
										By byRowColDataLink = By.xpath(strTableXPath + "/tbody/tr[" + iRowIndex + "]/*[" + iColNo2 + "]//a");
										if (isElementPresent(byRowColDataLink))
										{
											clickByJSE(byRowColDataLink, strTableName + " - " + strColNameDataToRetrive + " - RowColData[" + iRowIndex + ":" + iColNo2 + "]:" + strRowColData2);
										}
										else
										{
											clickByJSE(byRowColData2, strTableName + " - " + strColNameDataToRetrive + " - RowColData[" + iRowIndex + ":" + iColNo2 + "]: " + strRowColData2);
										}
									} // if Click
									break;
								} // bRowCol2_Found
							}
							else
							{// (iColNo2 != -1)
								report.updateTestLog(strClickGetSearch + " data in " + strTableName, "<b>Column Name:" + strColNameDataToRetrive + " not found in the Table: " + strTableName + "</b>", Status.FAIL);
							}
						} // strColNameDataToRetrive is not empty
					} // strRowColData.equalsIgnoreCase(strColData)
				} // for loop
				if (!strClickGetSearch.equalsIgnoreCase("Get"))
				{
					if (iRowIndex > 1)
					{// data starts from 2nd row
						if (strReturnData == null && !strClickGetSearch.equalsIgnoreCase("Search"))
						{// if search or Get just return null or data only
							if (bRowCol1_Found && !bRowCol2_Found)
							{
								report.updateTestLog(strClickGetSearch + " data in " + strTableName, "<b>[" + strColName + ": " + strColData + "] found in the Row#: " + iRowIndex + " and but its corresponding [" + strColNameDataToRetrive + ": " + strRowColData2 + "]<b> is not found in the Table: " + strTableName + "</b>", Status.PASS);
							}
							else
							{
								report.updateTestLog(strClickGetSearch + " data in " + strTableName, "<b>[" + strColName + ": " + strColData + "] not found in the Table: " + strTableName + "</b>", Status.PASS);
							}
						}
					}
					else
					{
						if (iTableRowsCount == 0)
						{
							report.updateTestLog(strClickGetSearch + " data in " + strTableName, "<b> No data not found in the Table: " + strTableName + "</b>", Status.PASS);
						}
						else
						{
							report.updateTestLog(strClickGetSearch + " data in " + strTableName, "<b>" + strColName + ":" + strColData + " data not found in the Table: " + strTableName + "</b>", Status.PASS);
						}
					} // (iRowIndex > 0)
				} // if(!strClickGetSearch.equalsIgnoreCase("Get")){
			}
			else
			{
				report.updateTestLog(strClickGetSearch + " data in " + strTableName, "<b>Column Name:" + strColName + " not found in the Table: " + strTableName + "</b>", Status.PASS);
			} // (iColNo != -1)
		} // bTableFound
		return strReturnData;
	}// verifyDataFromLinkLetTable
		/////////////////////////

	public String getTableXPath(String strTableType, String strTableName)
	{
		String strTableXPath = null;
		switch (strTableType.toLowerCase())
		{
			case "linklet" : // Related List Table
				strTableXPath = "//td[@class='pbTitle']//*[text()='" + strTableName + "']//ancestor::table/parent::div/following-sibling::div/table";
				break;
			case "list" : // Go To List
				strTableXPath = "//*[@class='bPageTitle']//*[text()='" + strTableName + "']/following::div[@class='pbBody']/table";
				break;
			case "search" : // MyAccounts - Genera Search
				strTableXPath = "//td[@class='pbTitle']//*[contains(text(),'" + strTableName + "')]//ancestor::table/parent::div/following-sibling::div/table";
				break;
			case "calendar" :
				strTableXPath = "//div[@id='calendar']//table[@id='vodDayViewTbl']";
				break;
			case "vodresultset" :
				strTableXPath = "//table[@id='vodResultSet']";
				break;
			default :
				report.updateTestLog("Get Table XPath for " + strTableName, "<b>" + strTableType + " is not valid. Valid Table types are linkLet, list, search or calendar." + "</b>", Status.FAIL);
		}
		return strTableXPath;
	}

	public String clickOnRowColFromLinkLetTable(String strTableName, int iRowIndex, int iColIndex) throws Exception
	{
		String strRowColData = null;
		int iLinkLetTableRowsCount = -1, iHeaderTableColCount = -1;
		String strLinkLetTableXPath = "//td[@class='pbTitle']//*[starts-with(text(),'" + strTableName + "')]//ancestor::table/parent::div/following-sibling::div/table";
		By byLinkLetTable = By.xpath(strLinkLetTableXPath);
		if (isElementVisible(byLinkLetTable, "LinkLetTable - " + strTableName))
		{
			// Get Row Count
			By byLinkLetTableRows = By.xpath(strLinkLetTableXPath + "/tbody/tr");
			iLinkLetTableRowsCount = driver.findElements(byLinkLetTableRows).size();
			if (iLinkLetTableRowsCount > 0)
			{
				// Get Column Count
				By byHeaderTableCols = By.xpath(strLinkLetTableXPath + "/tbody/tr[@class='headerRow']/th");// get
																											// column
																											// names
																											// from
																											// Header
																											// Table
				iHeaderTableColCount = driver.findElements(byHeaderTableCols).size();
				if (iColIndex <= iHeaderTableColCount && iColIndex > 0 && iRowIndex <= iLinkLetTableRowsCount && iRowIndex > 0)
				{
					String strRowColDataXPath = strLinkLetTableXPath + "/tbody/tr[" + (iRowIndex) + "]/*[" + iColIndex + "]";
					By byRowColData = By.xpath(strRowColDataXPath);
					if (verifyElementVisible(byRowColData, "RowColData[" + iRowIndex + ":" + iColIndex + "]"))
					{
						strRowColData = getData(byRowColData, "RowColData[" + (iRowIndex) + ":" + iColIndex + "]", "Text");
						// highLightElement(byRowColData, "RowColData["
						// +iRowIndex + ":" + iColIndex + "]");
						if (isElementPresent(By.xpath(strRowColDataXPath + "//a")))
						{
							clickByJSE(By.xpath(strRowColDataXPath + "//a"), "RowColData[" + iRowIndex + ":" + iColIndex + "]");
						}
						else
						{
							clickByJSE(byRowColData, "RowColData[" + iRowIndex + ":" + iColIndex + "]");
						}
					}
				}
				else
				{
					if (iColIndex > iHeaderTableColCount || iColIndex <= 0)
					{
						report.updateTestLog("clickOnRowColFromTable", "Table Col Count: " + iHeaderTableColCount + "," + "iColIndex:" + iColIndex + " is out of bound in the Table: " + strTableName, Status.FAIL);
					}
					if (iRowIndex > iLinkLetTableRowsCount || iRowIndex <= 0)
					{
						report.updateTestLog("clickOnRowColFromTable", "Table Row Count: " + iLinkLetTableRowsCount + "," + "iRowIndex:" + iRowIndex + " is out of bound in the Table: " + strTableName, Status.FAIL);
					}
				}
			}
			else
			{
				report.updateTestLog("clickOnRowColFromTable", "Table Row Count is " + iHeaderTableColCount + ",No record found in the Table: " + strTableName, Status.FAIL);
			}
		} // Table found byLinkLetTable
		return strRowColData;
	}// clickOnRowColFromLinkLetTable method

	public void waitForSeconds(long iSeconds)
	{
		try
		{
			TimeUnit.SECONDS.sleep(iSeconds);
		}
		catch (InterruptedException e)
		{
			report.updateTestLog("Wait For", "Wait For " + iSeconds + " seconds", Status.FAIL);
			e.printStackTrace();
		}
	}

	/**
	 * Description: Pause the Test Execution for a specified amount of time
	 * 
	 * @param long
	 *            milliSeconds - wait time in millseconds
	 */
	public void waitForMilliSeconds(long iMilliSeconds)
	{
		try
		{
			Thread.sleep(iMilliSeconds);
			TimeUnit.MILLISECONDS.sleep(iMilliSeconds);
		}
		catch (InterruptedException e)
		{
			report.updateTestLog("Wait For", "Wait For " + iMilliSeconds + " mill Seconds", Status.FAIL);
			e.printStackTrace();
		}
	}

	public void clickSFButton(String strButtonTxt) throws Exception
	{
		clickSFButton(strButtonTxt, "button");
	}

	public void clickSFButton2(String strButtonTxt, String strButtonType) throws Exception
	{
		try
		{
			clickSFButton2(strButtonTxt, strButtonType);
		}
		catch (InvocationTargetException e)
		{
			waitForSeconds(3);
			clickSFButton2(strButtonTxt, strButtonType);
		}
	}

	public void clickSFButton(String strButtonTxt, String strButtonType) throws Exception
	{
		clickSFButton("", strButtonTxt, strButtonType);
	}

	public void clickSFButton(String strSectionName, String strButtonTxt, String strButtonType) throws Exception
	{
		strButtonTxt = strButtonTxt.trim();
		strButtonType = strButtonType.trim();
		By byPageButton = getSFElementXPath(strSectionName, strButtonTxt, strButtonType);
		if (byPageButton != null)
		{
			clickButton(byPageButton, strButtonTxt);
		}
		if (strButtonTxt.equalsIgnoreCase("Save") || strButtonTxt.equalsIgnoreCase("Submit"))
		{
			isElementNotVisible(byPageButton, strButtonTxt + " - Button", 10);
			By byLoadingSpinner = By.xpath("//div[@class='spinner-mini ng-scope']");
			isElementNotVisible(byLoadingSpinner, "Loading Spinner", 20);
		}
	}

	public void setSFData(String strElementDesc, String strElementType, String strInputData) throws Exception
	{
		setSFData("", strElementDesc, strElementType, strInputData);
	}// setDataInSF() method

	public void setSFData(String strElementTable, String strElementDesc, String strElementType, String strInputData) throws Exception
	{
		// strElementType : Select, TextBox, Input, textarea, Date, Time, lookup
		By by = getSFElementXPath(strElementTable, strElementDesc, strElementType);
		if (verifyElementVisible(by, strElementDesc))
		{
			switch (strElementType.toLowerCase())
			{
				case "dcr-select" :
				case "select" :
					selectData(by, strElementDesc, strInputData);
					break;
				case "dcr-textbox" :
				case "textbox" :
					setData(by, strElementDesc, strInputData);
					break;
				case "dcr-input" :
				case "input" :
					setData(by, strElementDesc, strInputData);
					break;
				case "dcr-textarea" :
				case "textarea" :
					setData(by, strElementDesc, strInputData);
					break;
				case "date" :
					setData(by, strElementDesc, strInputData);
					break;
				case "time" :
					setData(by, strElementDesc, strInputData);
					break;
				case "lookup" :
					setData(by, strElementDesc, strInputData);
					break;
				case "lookupinput" :
					setData(by, strElementDesc, strInputData);
					break;
				case "dcr-multiselect" :
				case "multiselect" :
					multiSelectData(by, strElementDesc, strInputData);
					break;
				default :
					report.updateTestLog("setDataInSF", "Error: ElementType: " + strElementType + " is unknown", Status.FAIL);
			}// switch
		} // verifyElement
	}// setSFData() method

	public String getSFData(String strElementDesc, String strElementType, String strAttribute) throws Exception
	{
		String strGetData = null;
		By by = getSFElementXPath(strElementDesc, strElementType);
		String strElementXPath = "//*[normalize-space(text())='" + strElementDesc + "']/following::td[1]";
		if (strElementType.equalsIgnoreCase("Select"))
		{
			strElementXPath = strElementXPath + "//select";
			if (isElementVisible(by, strElementDesc))
			{
				highLightElement(by, strElementDesc);
				Select comboBox = new Select(driver.findElement(by));
				strGetData = comboBox.getFirstSelectedOption().getText();
			}
		}
		else
		{
			highLightElement(by, strElementDesc);
			strGetData = getData(by, strElementDesc, strAttribute);
		}
		return strGetData;
	}

	public String getSFData(String strTableName, String strElementDesc, String strElementType, String strAttribute) throws Exception
	{
		String strGetData = null;
		By by = getSFElementXPath(strTableName, strElementDesc, strElementType);// (strElementDesc,
																				// strElementType);
		if (strElementType.equalsIgnoreCase("Select"))
		{
			// String strElementXPath = "//*[normalize-space(text())='" +
			// strElementDesc + "']/following::td[1]//select";
			if (isElementVisible(by, strElementDesc))
			{
				highLightElement(by, strElementDesc);
				Select comboBox = new Select(driver.findElement(by));
				strGetData = comboBox.getFirstSelectedOption().getText();
			}
		}
		else if (strElementType.equalsIgnoreCase("multiselect"))
		{
			if (isElementVisible(by, strElementDesc))
			{
				highLightElement(by, strElementDesc);
				Select comboBox = new Select(driver.findElement(by));
				List<WebElement> comboBoxOptions = comboBox.getOptions();
				for (WebElement comboOption : comboBoxOptions)
				{
					String strOption = comboOption.getText();
					if (!strOption.trim().equalsIgnoreCase(""))
						strGetData += strOption + ";";
				}
			}
		}
		else
		{
			highLightElement(by, strElementDesc);
			strGetData = getData(by, strElementDesc, strAttribute);
		}
		return strGetData;
	}

	public void clickSFLink(String strElementDesc, String strElementType) throws Exception
	{
		// strElementType :TelephoneDial, Lookup
		// strElementDesc: Case Owner
		By by = getSFElementXPath(strElementDesc, strElementType);
		clickLink(by, strElementDesc);
	}

	public void clickSFElement(String strElementDesc, String strElementType) throws Exception
	{
		By by = getSFElementXPath(strElementDesc, strElementType);
		highLightElement(by, strElementDesc);
		clickByJSE(by, strElementDesc);
	}

	public By getSFElementXPath(String strElementDesc, String strElementType) throws Exception
	{
		By by = getSFElementXPath("", strElementDesc, strElementType);
		return by;
	}

	public By getSFElementXPath(String strElementTable, String strElementDesc, String strElementType) throws Exception
	{
		By by = null;
		strElementTable = strElementTable.trim();
		strElementDesc = strElementDesc.trim();
		String strElementXPath = "", strElement2XPath = "", strElement3XPath = "";
		String strTableXPath = "", strTable2XPath = "", strTable3XPath = "";
		Boolean bElementTableFound = true;
		if (!strElementTable.equalsIgnoreCase(""))
		{
			strTableXPath = "//div[contains(@class,'pbSubheader')]//*[text()='" + strElementTable + "']/following::div[1]";
			strElementXPath = "//div[contains(@class,'pbSubheader')]//*[text()='" + strElementTable + "']/following::div[1]";
			strTable2XPath = "//td[contains(@class,'pbTitle')]//*[text()='" + strElementTable + "']";
			strElement2XPath = "//td[contains(@class,'pbTitle')]//*[text()='" + strElementTable + "']";
			strTable3XPath = "//td[contains(@class,'pbTitle')]//*[text()='" + strElementTable + "']/ancestor::table[1]/parent::div/following-sibling::div[@class='pbBody']";
			strElement3XPath = "//td[contains(@class,'pbTitle')]//*[text()='" + strElementTable + "']/ancestor::table[1]/parent::div/following-sibling::div[@class='pbBody']";
			if (!isElementVisible(By.xpath(strElementXPath), strElementTable, 10))
			{
				strTableXPath = strTable3XPath;
				strElementXPath = strElement3XPath;
				if (!isElementVisible(By.xpath(strElementXPath), strElementTable, 10))
				{
					strTableXPath = strTable2XPath;
					strElementXPath = strElement2XPath;
					if (!isElementVisible(By.xpath(strElementXPath), strElementTable, 10))
					{
						report.updateTestLog("setDataInSF", "Error: ElementTable: " + strElementTable + " is not Found in the Page", Status.FAIL);
						bElementTableFound = false;
					}
				}
			}
		}
		if (bElementTableFound)
		{
			strElementXPath = strElementXPath + "//tr//*[normalize-space(text())='" + strElementDesc + "']/following::td[1]"; //// removed
																																//// /div
																																//// as
																																//// is
																																//// not
																																//// there
																																//// in
																																//// links
			switch (strElementType.toLowerCase())
			{
				case "link" :
					// strElementXPath = strElementXPath + "//a";
					strElementXPath = strTableXPath + "//*[contains(@class,'labelCol')]/*[normalize-space(text())='" + strElementDesc + "']/following::div[1]//veev-field//a";
					driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
					if (driver.findElements(By.xpath(strElementXPath)).size() == 0)
					{
						strElementXPath = strTableXPath + "//td[contains(@class,'labelCol')][normalize-space(text())='" + strElementDesc + "']/following::td[1]//a";
					}
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
					break;
				case "listbutton" :
					strElementXPath = "//div[@class='listButtons']//input[@*='" + strElementDesc + "']";
					break;
				case "listbuttons" :
					strElementXPath = "//div[@class='listButtons']//input[@*='" + strElementDesc + "']";
					break;
				case "pbbutton" :
					strElementXPath = "//td[@class='pbButton' or @class='pbButton ' or @id='topButtonRow']//input[@*='" + strElementDesc + "']";
					break;
				case "pbbuttonb" :
					strElementXPath = "//td[@class='pbButtonb' or @class='pbButtonb ' or @id='bottomButtonRow']//input[@*='" + strElementDesc + "']";
					break;
				case "button" :
				case "submit" :
					strElementXPath = "//input[@type='button' or @type='submit'][@*='" + strElementDesc + "']";
					break;
				case "telephonedial" :
					strElementXPath = strElementXPath + "//img[contains(@title,'Click to dial')]";
					break;
				case "lookupicon" :
					// strElementXPath = strElementTable +
					// "//tr//*[normalize-space(text())='" + strElementDesc +
					// "']/following::td[1]//a/img[@class='lookupIcon']";
					strElementXPath = strTableXPath + "//a[contains(@title,'" + strElementDesc + " Lookup')]";
					break;
				case "lookup" :
				case "lookupinput" :
					strElementXPath = strElementXPath + "//span[@class='lookupInput']//input";
					break;
				case "lookupimage" :
					strElementXPath = strElementXPath + "//span[@class='lookupInput']//a";
					break;
				case "error" :
					strElementXPath = "//div[@class='pbBody']/div[@class='pbError']";
					break;
				//////////////////////////////////////// SetData XPaths
				//////////////////////////////////////// ////////////////////////////////////////////////////////////////////////////
				case "select" :
					strElementXPath = strTableXPath + "//*[contains(@class,'labelCol')]//*[contains(normalize-space(text()),'" + strElementDesc + "')]/following::select";
					break;
				case "textbox" :
				case "input" :
					strElementXPath = strElementXPath + "//input[@type='text']";
					break;
				case "hiddeninput" :
					strElementXPath = strElementXPath + "//input[@type='hidden']";
					break;
				case "textarea" :
					strElementXPath = strElementXPath + "//textarea[@type='text']";
					break;
				case "radio" :
					strElementXPath = strElementXPath + "//input[@type='radio']";
					break;
				case "checkbox" :
					strElementXPath = strElementXPath + "//input[@type='checkbox']";
					break;
				case "checkmark" :
					strElementXPath = strElementXPath + "//div/img";
					break;
				case "date" :
					strElementXPath = strElementXPath + "//span//input";
					break;
				case "time" :
					strElementXPath = strElementXPath + "//span/span/input";
					break;
				case "email" :
					strElementXPath = strElementXPath + "//a";
					break;
				case "pagedescription" :
				case "pagedesc" :
					// strElementXPath =
					// "//div[@class='content']/h2[@class='pageDescription']";
					strElementXPath = "//div[contains(@class,'content')]/h2[contains(@class,'pageDescription')]";// For
																													// Veeva
																													// Change
					break;
				case "dateformat" :
					strElementXPath = strElementXPath + "//span[@class='dateFormat']/a";
					break;
				case "pagetype" :
					// strElementXPath =
					// "//div[@class='content']/h1[@class='pageType']";
					strElementXPath = "//div[contains(@class,'content')]/h1[contains(@class,'pageType')]";// For
																											// Veeva
																											// Change
					break;
				case "pagetitle" :
				case "pagetitleicon" :
					// strElementXPath =
					// "//div[@class='content']/h1[@class='pageType']";
					strElementXPath = "//div[contains(@class,'content')]/img[@class='pageTitleIcon']";
					break;
				case "heading" :
					strElementXPath = "//h3[normalize-space(text())='" + strElementDesc + "']";
					break;
				case "header links" :
					strElementXPath = strTableXPath + "//following::*[contains(@class,'labelCol')][normalize-space(text())='" + strElementDesc + "']/following-sibling::td//a";
					break;
				case "header labels" :
					strElementXPath = strTableXPath + "//following::*[contains(@class,'labelCol')][normalize-space(text())='" + strElementDesc + "']/following-sibling::td//div";
					break;
				case "header help" :
					strElementXPath = strTableXPath + "//following::*[contains(@class,'labelCol')]//span[@class='helpButton'][normalize-space(text())='" + strElementDesc + "']/following::td[contains(@class,'dataCol')]/div";
					break;
				case "multiselect" :
					strElementXPath = strTableXPath + "//*[contains(@class,'labelCol')]//*[contains(normalize-space(text()),'" + strElementDesc + "')]/following::select[@multiple='']";
					break;
				case "text" :
					driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
					strElementXPath = strTableXPath + "//*[contains(@class,'labelCol')]/*[normalize-space(text())='" + strElementDesc + "']/following::td[1][contains(@class,'dataCol')]";
					if (driver.findElements(By.xpath(strElementXPath)).size() == 0)
					{
						strElementXPath = strTableXPath + "//*[contains(@class,'labelCol')]/*[normalize-space(text())='" + strElementDesc + "']/following::div[1]//veev-field";
						if (driver.findElements(By.xpath(strElementXPath)).size() == 0)
						{
							strElementXPath = strTableXPath + "//td[contains(@class,'labelCol')][normalize-space(text())='" + strElementDesc + "']/following::td[1][contains(@class,'data')]";
						}
					}
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
					break;
				case "labeltext" :
					driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
					strElementXPath = strTableXPath + "//label[.='" + strElementDesc + "']";
					break;
				case "non-editable" :
					strElementXPath = strTableXPath + "//*[contains(@class,'labelCol')]/*[normalize-space(text())='" + strElementDesc + "']/parent::div/following-sibling::div//veev-field";
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
					break;
				case "dcr-text" :
					strElementXPath = "//div[contains(@class,'pbSubheader')]//*[text()='" + strElementTable + "']/following::table[1]//tr//*[normalize-space(text())='" + strElementDesc + "']/following::td[1]//div";
					break;
				case "dcr-select" :
					strElementXPath = "//div[contains(@class,'pbSubheader')]//*[text()='" + strElementTable + "']/following::table[1]//tr//*[normalize-space(text())='" + strElementDesc + "']/following::td[1]//select";
					break;
				case "dcr-textbox" :
					strElementXPath = "//div[contains(@class,'pbSubheader')]//*[text()='" + strElementTable + "']/following::table[1]//tr//*[normalize-space(text())='" + strElementDesc + "']/following::td[1]//input[@type='text']";
					break;
				case "dcr-textarea" :
					strElementXPath = "//div[contains(@class,'pbSubheader')]//*[text()='" + strElementTable + "']/following::table[1]//tr//*[normalize-space(text())='" + strElementDesc + "']/following::td[1]//textarea";
					break;
				case "dcr-multiselect" :
					strElementXPath = "//div[contains(@class,'pbSubheader')]//*[text()='" + strElementTable + "']/following::table[1]//*[contains(@class,'labelCol')]//*[contains(normalize-space(text()),'" + strElementDesc + "')]/following::select[@multiple='']";
					break;
				default :
					// report.updateTestLog("clickSFElement", "Error: " +
					// strElementType + " with Element Desc: " + strElementDesc
					// + " is unknown", Status.FAIL);
			}
			switch (strElementDesc.toLowerCase())
			{
				case "case owner" :
					strElementXPath = "//tr//*[normalize-space(text())='" + strElementDesc + "']/following::td[1]//span/a[2]";
					break;
				case "owner" :
					strElementXPath = "//tr//*[normalize-space(text())='" + strElementDesc + "']/following::td[1]//span/a[2]";
					break;
				case "Case Number:" :
					strElementXPath = "//*[text()='Case Number:']/following::*[1]";
					break;
				case "Case Number" :
					strElementXPath = "//*[text()='Case Number:']/following::*[1]";
					break;
				default :
					// No Aciton
			}
			by = By.xpath(strElementXPath);
		} // bElementTableFound
		return by;
	}

	public void clickSFCheckBox(String strCheckBoxDesc, String strCheckBoxPosition, Boolean bCheckBoxStatusToSet) throws Exception
	{
		// strCheckBoxStatus: True, False
		String strXPath = "";
		if (strCheckBoxPosition.equalsIgnoreCase("Following"))
		{
			strXPath = "//*[normalize-space(text())='" + strCheckBoxDesc + "']/following::td[1]//input[@type='checkbox']";
		}
		else if (strCheckBoxPosition.equalsIgnoreCase("Preceding"))
		{
			strXPath = "//*[normalize-space(text())='" + strCheckBoxDesc + "']/preceding-sibling::input[@type='checkbox']";
		}
		By byCheckBox = By.xpath(strXPath);
		if (verifyElementVisible(byCheckBox, strCheckBoxDesc))
		{
			highLightElement(byCheckBox, strCheckBoxDesc);
			String strAppCBStatus = getData(byCheckBox, strCheckBoxDesc, "checked");
			System.out.println(strCheckBoxDesc + " - strAppCBStatus: " + strAppCBStatus);
			if (strAppCBStatus.equalsIgnoreCase("checked") && bCheckBoxStatusToSet)
			{
				// Expected to Check the Check box and current status is Checked
				// and so skip check box click,
			}
			else if (!strAppCBStatus.equalsIgnoreCase("checked") && !bCheckBoxStatusToSet)
			{
				// Expected to UnCheck the Check box and current status is
				// UnChecked and so skip check box click,
			}
			else
			{
				// clickByJSE(byCheckBox, strCheckBoxDesc);
				String strExpected_Status = "Checked";
				if (!bCheckBoxStatusToSet)
				{
					strExpected_Status = "UnChecked";
				}
				try
				{
					driver.findElement(byCheckBox).click();
					report.updateTestLog("Verify the Checkbox: <b>" + strCheckBoxDesc + "</b> is " + strExpected_Status, "The Checkbox: <b>" + strCheckBoxDesc + "</b> is " + strExpected_Status, Status.DONE);
					System.out.println("The Checkbox: " + strCheckBoxDesc + " is clicked");
				}
				catch (Exception e)
				{
					report.updateTestLog("Verify the Checkbox: <b>" + strCheckBoxDesc + "</b> is " + strExpected_Status, "The Checkbox: <b>" + strCheckBoxDesc + "</b> is not " + strExpected_Status, Status.DONE);
					System.out.println("The Checkbox: " + strCheckBoxDesc + " is " + strExpected_Status);
				}
			}
		} // byCheckBox
	}

	public void verifyCheckBoxStatus(String strCheckBoxDesc, String strCheckBoxPosition, Boolean bCheckBoxStatusToVerify) throws Exception
	{
		verifyCheckBoxStatus(strCheckBoxDesc, strCheckBoxPosition, bCheckBoxStatusToVerify, true);
	}

	public void verifyCheckBoxStatus(String strCheckBoxDesc, String strCheckBoxPosition, Boolean bCheckBoxStatusToVerify, Boolean bScreenshot) throws Exception
	{
		// strCheckBoxStatus: Check, UnCheck
		String strXPath = "";
		String strCheckImgXPath = "";
		if (strCheckBoxPosition.equalsIgnoreCase("Following"))
		{
			strXPath = "//*[text()='" + strCheckBoxDesc + "']/following::td[1]//input[@type='checkbox']";
			strCheckImgXPath = "//*[@class='labelCol'][normalize-space(text())='" + strCheckBoxDesc + "']/following::img[1][@class='checkImg']";
		}
		else if (strCheckBoxPosition.equalsIgnoreCase("Preceding"))
		{
			strXPath = "//*[text()='" + strCheckBoxDesc + "']/preceding-sibling::input[@type='checkbox']";
			strCheckImgXPath = "//*[@class='labelCol'][normalize-space(text())='" + strCheckBoxDesc + "']/preceding::img[1][@class='checkImg']";
		}
		By byCheckBox = By.xpath(strCheckImgXPath);
		Boolean bCheckImg = true;
		if (!isElementVisible(byCheckBox, strCheckBoxDesc, 2))
		{
			byCheckBox = By.xpath(strXPath);
			bCheckImg = false;
		}
		if (verifyElementVisible(byCheckBox, strCheckBoxDesc))
		{
			highLightElement(byCheckBox, strCheckBoxDesc);
			String strAppCBStatus = "";
			if (bCheckImg)
			{
				strAppCBStatus = getData(byCheckBox, strCheckBoxDesc, "title");
			}
			else
			{
				strAppCBStatus = getData(byCheckBox, strCheckBoxDesc, "checked");
			}
			// System.out.println(strCheckBoxDesc + " - strAppCBStatus: " +
			// strAppCBStatus);
			if ((strAppCBStatus.equalsIgnoreCase("checked") || strAppCBStatus.equalsIgnoreCase("true")) && bCheckBoxStatusToVerify)
			{
				if (bScreenshot)
				{
					report.updateTestLog("verifyCheckBoxStatus", "CheckBox: " + strCheckBoxDesc + " is in checked status as expected.", Status.PASS);
				}
				else
				{
					report.updateTestLog("verifyCheckBoxStatus", "CheckBox: " + strCheckBoxDesc + " is in checked status as expected.", Status.DONE);
				}
				System.out.println("CheckBox: " + strCheckBoxDesc + " is in checked status as expected.");
			}
			else if (!(strAppCBStatus.equalsIgnoreCase("checked") || strAppCBStatus.equalsIgnoreCase("true")) && !bCheckBoxStatusToVerify)
			{
				if (bScreenshot)
				{
					report.updateTestLog("verifyCheckBoxStatus", "CheckBox: " + strCheckBoxDesc + " is in UnChecked status as expected.", Status.PASS);
				}
				else
				{
					report.updateTestLog("verifyCheckBoxStatus", "CheckBox: " + strCheckBoxDesc + " is in UnChecked status as expected.", Status.DONE);
				}
				System.out.println("CheckBox: " + strCheckBoxDesc + " is in UnChecked status as expected.");
			}
			else
			{
				report.updateTestLog("verifyCheckBoxStatus", "CheckBox: " + strCheckBoxDesc + " - Status is " + strAppCBStatus + " which is not expected Status:" + bCheckBoxStatusToVerify, Status.FAIL);
				System.out.println("CheckBox: " + strCheckBoxDesc + " - Status is " + strAppCBStatus + " which is not expected Status:" + bCheckBoxStatusToVerify);
			}
		} // byCheckBox
	}

	// 12. Method for TimeLapse
	public void timeLapse(CraftDriver driver, int value, TimeUnit unit)
	{
		driver.manage().timeouts().implicitlyWait(Allocator.timeout, TimeUnit.SECONDS);
		try
		{
			driver.manage().timeouts().implicitlyWait(value, unit);
		}
		catch (Exception e)
		{
		}
		driver.manage().timeouts().implicitlyWait(Allocator.timeout, TimeUnit.SECONDS);
	}

	public void implicitWait(int value, TimeUnit timeUnit)
	{
		try
		{
			driver.manage().timeouts().implicitlyWait(value, timeUnit);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void pageLoadTimeout(int value, TimeUnit timeUnit)
	{
		try
		{
			driver.manage().timeouts().pageLoadTimeout(value, timeUnit);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void setScriptTimeout(int value, TimeUnit timeUnit)
	{
		try
		{
			driver.manage().timeouts().setScriptTimeout(value, timeUnit);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void verifyPresenceofElement(By by, String Desc, String passMessage, String failMessage, int iScrollPixel) throws Exception
	{
		// driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		Boolean bElementPresent = isElementPresent(by, Desc);
		try
		{
			if (bElementPresent)
			{
				// ((JavascriptExecutor)
				// driver).executeScript("arguments[0].scrollIntoView();",
				// driver.findElement(by));
				scrollToElement(by, Desc, iScrollPixel);
				report.updateTestLog(Desc, passMessage, Status.PASS);
			}
			else
			{
				// ((JavascriptExecutor)
				// driver).executeScript("arguments[0].scrollIntoView();",
				// driver.findElement(by));
				report.updateTestLog(Desc, failMessage, Status.FAIL);
			}
		}
		catch (Exception e)
		{
			report.updateTestLog(Desc, failMessage, Status.FAIL);
		}
		// driver.manage().timeouts().implicitlyWait(Allocator.timeout,
		// TimeUnit.SECONDS);
	}

	public void verifyPresenceofElement(By by, String Desc, String passMessage, String failMessage) throws Exception
	{
		// driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		Boolean bElementPresent = isElementVisible(by, Desc);
		try
		{
			if (bElementPresent)
			{
				// ((JavascriptExecutor)
				// driver).executeScript("arguments[0].scrollIntoView();",
				// driver.findElement(by));
				scrollElementToMiddlePage(by, Desc);
				report.updateTestLog(Desc, passMessage, Status.PASS);
				System.out.println(Desc + ": " + passMessage);
			}
			else
			{
				// ((JavascriptExecutor)
				// driver).executeScript("arguments[0].scrollIntoView();",
				// driver.findElement(by));
				report.updateTestLog(Desc, failMessage, Status.FAIL);
				System.out.println(Desc + ": " + failMessage);
			}
		}
		catch (Exception e)
		{
			report.updateTestLog(Desc, failMessage, Status.FAIL);
			System.out.println(Desc + ": " + failMessage);
		}
	}

	public void verifyElementPresent(By by, String Desc, String passMessage, String failMessage) throws Exception
	{
		verifyElementPresent(by, Desc, passMessage, failMessage, true);
	}

	public void verifyElementPresent(By by, String Desc, String passMessage, String failMessage, Boolean bTakeScreenshotOnPass) throws Exception
	{
		// driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		Boolean bElementPresent = isElementPresent(by, Desc);
		try
		{
			if (bElementPresent)
			{
				if (bTakeScreenshotOnPass)
				{
					report.updateTestLog(Desc, passMessage, Status.PASS);
				}
				else
				{
					report.updateTestLog(Desc, passMessage, Status.DONE);
				}
			}
			else
			{
				report.updateTestLog(Desc, failMessage, Status.FAIL);
			}
		}
		catch (Exception e)
		{
			report.updateTestLog(Desc, failMessage, Status.FAIL);
		}
	}

	public void verifyElementVisible(By by, String Desc, String passMessage, String failMessage) throws Exception
	{
		verifyElementPresent(by, Desc, passMessage, failMessage, true);
	}

	public void verifyElementVisible(By by, String Desc, String passMessage, String failMessage, Boolean bTakeScreenshotOnPass) throws Exception
	{
		// driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		Boolean bElementPresent = isElementVisible(by, Desc);
		try
		{
			if (bElementPresent)
			{
				if (bTakeScreenshotOnPass)
				{
					report.updateTestLog(Desc, passMessage, Status.PASS);
				}
				else
				{
					report.updateTestLog(Desc, passMessage, Status.DONE);
				}
			}
			else
			{
				report.updateTestLog(Desc, failMessage, Status.FAIL);
			}
		}
		catch (Exception e)
		{
			report.updateTestLog(Desc, failMessage, Status.FAIL);
		}
	}

	public void verifyElementInPage(By Path, String description)
	{
		Boolean validate = false;
		try
		{
			WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), 20);
			wait.until(ExpectedConditions.visibilityOfElementLocated(Path));
			validate = true;
		}
		catch (Exception e)
		{
			validate = false;
		}
		if (validate == true)
			report.updateTestLog("Verify able to see the element " + description + "", "Able to see the element " + description + "", Status.PASS);
		else
			report.updateTestLog("Verify able to see the element " + description + "", "Unable to see the element " + description + "", Status.FAIL);
	}

	public Boolean verifyElementInPage(By by, String strElementDesc, String strReportStepName, String strPassDesc, String strFailDesc) throws Exception
	{
		return verifyElementInPage(by, strElementDesc, strReportStepName, strPassDesc, strFailDesc, true);
	}

	public Boolean verifyElementInPage(By by, String strElementDesc, String strReportStepName, String strReportPassDesc, String strReportFailDesc, Boolean bTakeScreenshotOnPass) throws Exception
	{
		Boolean bElementVisible = isElementVisible(by, strElementDesc);
		try
		{
			if (bElementVisible)
			{
				if (bTakeScreenshotOnPass)
				{
					report.updateTestLog(strReportStepName, strReportPassDesc, Status.PASS);
				}
				else
				{
					report.updateTestLog(strReportStepName, strReportPassDesc, Status.DONE);
				}
			}
			else
			{
				report.updateTestLog(strReportStepName, strReportFailDesc, Status.FAIL);
			}
		}
		catch (Exception e)
		{
			report.updateTestLog(strReportStepName, strReportFailDesc, Status.FAIL);
		}
		return bElementVisible;
	}

	public Boolean verifyActualExpectedData(By by, String strElementDesc, String strAttribute, String strExpectedData, String strVerifyOption, Boolean bTakeScreenshotOnPass) throws Exception
	{
		Boolean bElementVisible = isElementVisible(by, strElementDesc);
		if (bElementVisible)
		{
			String strActualData = getData(by, strElementDesc, strAttribute);
			verifyActualExpectedData(strElementDesc, strActualData, strExpectedData, strVerifyOption, bTakeScreenshotOnPass);
		}
		return bElementVisible;
	}

	public void switchToWindow(String value) throws Exception
	{
		Set<String> handles = driver.getWindowHandles();
		Iterator<String> it = handles.iterator();
		String parent = it.next();
		String child = it.next();
		try
		{
			if (value.equalsIgnoreCase("child"))
			{
				driver.switchTo().window(child);
				System.out.println(value + " window is opened");
			}
			else if (value.equalsIgnoreCase("parent"))
			{
				driver.switchTo().window(parent);
			}
		}
		catch (Exception e)
		{
			report.updateTestLog("Switch Window", "Window is not present", Status.PASS);
		}
	}

	public void openNewTab2() throws InterruptedException
	{
		((JavascriptExecutor) driver.getWebDriver()).executeScript("window.open();");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		TimeUnit.SECONDS.sleep(5);
	}

	public void moveToElement(WebDriver sc, WebElement element)
	{
		try
		{
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
			Thread.sleep(10);
		}
		catch (Exception e)
		{
		}
	}

	public void switchtoNewTab(int tabNumber)
	{
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		ArrayList<String> tab1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tab1.get(tabNumber));
		driver.manage().timeouts().implicitlyWait(Allocator.timeout, TimeUnit.SECONDS);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public int getColNumFromTable(By byHeaderTableCols, String strTableDesc, String strColName) throws Exception
	{
		int iColNo = -1;
		strColName = strColName.trim();
		Boolean bColName = false;
		isElementVisible(byHeaderTableCols, "HeaderTableCols - " + strTableDesc);
		// By byHeaderTableCols = By.xpath(strXPath);
		if (isElementPresent(byHeaderTableCols, "HeaderTableCols - " + strTableDesc))
		{ // isElementVisible return false
			List<WebElement> eHeaderTableCols = driver.findElements(byHeaderTableCols);
			int iHeaderTableColsCount = eHeaderTableCols.size();
			for (int iCol = 0; iCol < iHeaderTableColsCount; iCol++)
			{
				// By byHeaderTableCol = By.xpath(strXPath + "[" + i + "]");
				// String strHeaderTableColName = getData(byHeaderTableCol, i +
				// "HeaderTableCol", "title");
				String strHeaderTableColName = getData(eHeaderTableCols.get(iCol), iCol + "HeaderTableCol", "text");
				if (strHeaderTableColName.toLowerCase().equals(strColName.toLowerCase()))
				{
					iColNo = iCol + 1;
					bColName = true;
					// highLightElement(eHeaderTableCols.get(iCol), iCol +
					// "HeaderTableCol");
					break;
				}
			}
			if (!bColName)
			{
				report.updateTestLog("getColNumFromTable", "<b>ColName: " + strColName + " not found in the " + "Table:" + strTableDesc + "</b>", Status.FAIL);
				System.out.println("getColNumFromTable - ColName: " + strColName + " not found in the " + "Table:" + strTableDesc);
			}
		}
		else
		{
			report.updateTestLog("getColNumFromTable", "<b>Table:" + strTableDesc + " not found in the Page.</b>", Status.FAIL);
			System.out.println("getColNumFromTable - Table:" + strTableDesc + " not found in the Page");
		}
		return iColNo;
	}

	public int getColNumFromTable_deleteRecord(By byHeaderTableCols, String strTableDesc, String strColName) throws Exception
	{
		int iColNo = -1;
		strColName = strColName.trim();
		Boolean bColName = false;
		isElementVisible(byHeaderTableCols, "HeaderTableCols - " + strTableDesc);
		// By byHeaderTableCols = By.xpath(strXPath);
		if (isElementPresent(byHeaderTableCols, "HeaderTableCols - " + strTableDesc))
		{ // isElementVisible return false
			List<WebElement> eHeaderTableCols = driver.findElements(byHeaderTableCols);
			int iHeaderTableColsCount = eHeaderTableCols.size();
			for (int iCol = 0; iCol < iHeaderTableColsCount; iCol++)
			{
				// By byHeaderTableCol = By.xpath(strXPath + "[" + i + "]");
				// String strHeaderTableColName = getData(byHeaderTableCol, i +
				// "HeaderTableCol", "title");
				String strHeaderTableColName = getData(eHeaderTableCols.get(iCol), iCol + "HeaderTableCol", "text");
				if (strHeaderTableColName.equalsIgnoreCase(strColName))
				{
					iColNo = iCol + 1;
					bColName = true;
					// highLightElement(eHeaderTableCols.get(iCol), iCol +
					// "HeaderTableCol");
					break;
				}
			}
			if (!bColName)
			{
				report.updateTestLog("getColNumFromTable", "<b>ColName: " + strColName + " not found in the " + "Table:" + strTableDesc + "</b>", Status.PASS);
				System.out.println("getColNumFromTable - ColName: " + strColName + " not found in the " + "Table:" + strTableDesc);
			}
		}
		else
		{
			report.updateTestLog("getColNumFromTable", "<b>Table:" + strTableDesc + " not found in the Page.</b>", Status.PASS);
			System.out.println("getColNumFromTable - Table:" + strTableDesc + " not found in the Page");
		}
		return iColNo;
	}

	public String getColumnNameFromTable(By byHeaderTableCols, String strTableDesc, int iColIndex) throws Exception
	{
		String strColName = null;
		// By byHeaderTableCols = By.xpath(strXPath);
		if (isElementPresent(byHeaderTableCols, "HeaderTableCols - " + strTableDesc))
		{// isElementVisible returns false
			List<WebElement> eHeaderTableCols = driver.findElements(byHeaderTableCols);
			int iHeaderTableColsCount = eHeaderTableCols.size();
			if (iHeaderTableColsCount > iColIndex && iColIndex > 0)
			{
				strColName = getData(eHeaderTableCols.get(iColIndex - 1), iColIndex + "HeaderTableCol", "title");
			}
			else
			{
				report.updateTestLog("getColumnNameFromTable", "Table:" + strTableDesc + " TableColsCount is " + iHeaderTableColsCount + " and Column Index: " + iColIndex + " is not within the range.", Status.FAIL);
			}
		}
		else
		{
			report.updateTestLog("getColumnNameFromTable", "Table:" + strTableDesc + " not found in the Page.", Status.FAIL);
		}
		return strColName;
	}

	public int getRowcountFromTable(String strTableDesc) throws Exception
	{
		int iScrollerTableRowsCount = -1;
		By byFormTable = By.xpath("//form//div[@class='listBody']");
		if (isElementPresent(byFormTable, "FormTable - " + strTableDesc))
		{
			By byScrollerTableRows = By.xpath("//form//div[@class='listBody']//div[@class='x-grid3-scroller']/div/div");
			List<WebElement> eScrollerTableRows = driver.findElements(byScrollerTableRows);
			iScrollerTableRowsCount = eScrollerTableRows.size();
			if (iScrollerTableRowsCount < 1)
			{
				// report.updateTestLog("getRowcountFromTable", "RowCount:" +
				// iScrollerTableRowsCount + " found in the Table: " +
				// strTableDesc, Status.FAIL);
			}
		}
		return iScrollerTableRowsCount;
	}

	public int getRowCountFromTable(String strTableType, String strTableName) throws Exception
	{
		int iTableRowsCount = -1;
		String strTableXPath = getTableXPath(strTableType, strTableName);
		By byTable = By.xpath(strTableXPath);
		Boolean bTableFound = false;
		if (isElementVisible(byTable, strTableType + " Table: " + strTableName))
		{
			bTableFound = true;
		}
		else
		{// bTableFound
			report.updateTestLog("get Rowcount From Table: " + strTableName, "<b>" + strTableType + " Table: " + strTableName + " not found in the page." + "</b>", Status.FAIL);
		}
		if (bTableFound)
		{
			By byTableRows = By.xpath(strTableXPath + "/tbody/tr");
			List<WebElement> eTableRows = driver.findElements(byTableRows);
			iTableRowsCount = eTableRows.size(); // As Header row is to be
													// considered.
			String strRowClass = getData(eTableRows.get(0), "Table Row class Value", "class");
			if (strRowClass != null && strRowClass.equalsIgnoreCase("headerRow"))
			{
				--iTableRowsCount;
			}
		}
		return iTableRowsCount;
	}

	public int getRowcountFromTable(By byTableRows, String strTableDesc) throws Exception
	{
		int iTableRowsCount = -1;
		if (isElementPresent(byTableRows, "Table - " + strTableDesc))
		{
			List<WebElement> eTableRows = driver.findElements(byTableRows);
			iTableRowsCount = eTableRows.size();
			// String strRowClass = eTableRows.get(0).getAttribute("class");
			String strRowClass = getData(eTableRows.get(0), "Table Row class Value", "class");
			if (strRowClass != null && strRowClass.equalsIgnoreCase("headerRow"))
			{
				--iTableRowsCount;
			}
			// if(iTableRowsCount < 1){
			// report.updateTestLog("getRowcountFromTable", "RowCount:" +
			// iTableRowsCount + " found in the Table: " + strTableDesc,
			// Status.FAIL);
			// }
		}
		return iTableRowsCount;
	}

	public String getDataFromTable(String strTableName, int iRowNo, String strColName) throws Exception
	{
		String strRowColData = null;
		By byFormTable = By.xpath("//form//div[@class='listBody']");
		if (isElementVisible(byFormTable, "FormTable - " + strTableName))
		{
			// get column names from Header Table
			By byHeaderTableCols = By.xpath("//form//div[@class='listBody']//div[@class='x-grid3-header']/div/div/table/thead/tr/td/div");
			int iColNo = getColNumFromTable(byHeaderTableCols, strTableName, strColName);
			if (iColNo != -1)
			{
				By byScrollerTableRows = By.xpath("//form//div[@class='listBody']//div[@class='x-grid3-scroller']/div/div");
				List<WebElement> eScrollerTableRows = driver.findElements(byScrollerTableRows);
				int iScrollerTableRowsCount = eScrollerTableRows.size();
				if (iRowNo <= iScrollerTableRowsCount)
				{
					By byRowColData = By.xpath("//form//div[@class='listBody']//div[@class='x-grid3-scroller']/div/div[" + iRowNo + "]/table/tbody/tr/td[" + iColNo + "]/div");
					// highLightElement(byRowColData, iRowNo + strColName);
					strRowColData = getData(byRowColData, iRowNo + strColName, "Text");
					report.updateTestLog("Get " + " data in " + strTableName, "<b>" + strColName + ": " + strRowColData + " found in the Row#: " + iRowNo + " from the Table: " + strTableName + "</b>", Status.PASS);
					System.out.println("Get " + " data in " + strTableName + ", " + strColName + ": " + strRowColData + " found in the Row#: " + iRowNo + " from the Table: " + strTableName);
				}
			}
			else
			{
				report.updateTestLog("getFormTableData", "Column Name:" + strColName + " not found in the Table: " + strTableName, Status.FAIL);
			}
		}
		return strRowColData;
	}

	public String getDataFromTable(String strTableDesc, String strColName, String strColData, String strColNameDataToRetrive) throws Exception
	{
		String strRowColData = null, strRowColData2 = null;;
		int iRowIndex = 0;
		By byFormTable = By.xpath("//form//div[@class='listBody']");
		if (isElementVisible(byFormTable, "FormTable - " + strTableDesc))
		{
			// get column names from Header Table
			By byHeaderTableCols = By.xpath("//form//div[@class='listBody']//div[@class='x-grid3-header']/div/div/table/thead/tr/td/div");
			int iColNo = getColNumFromTable(byHeaderTableCols, strTableDesc, strColName);
			int iColNo2 = getColNumFromTable(byHeaderTableCols, strTableDesc, strColNameDataToRetrive);
			if (iColNo != -1 && iColNo2 != -1)
			{
				By byScrollerTableRows = By.xpath("//form//div[@class='listBody']//div[@class='x-grid3-scroller']/div/div");
				List<WebElement> eScrollerTableRows = driver.findElements(byScrollerTableRows);
				int iScrollerTableRowsCount = eScrollerTableRows.size();
				for (int iRow = 1; iRow <= iScrollerTableRowsCount; iRow++)
				{
					By byRowColData = By.xpath("//form//div[@class='listBody']//div[@class='x-grid3-scroller']/div/div[" + iRow + "]/table/tbody/tr/td[" + iColNo + "]/div");
					strRowColData = getData(byRowColData, iRow + strColName, "Text");
					if (strRowColData.equalsIgnoreCase(strColData))
					{
						iRowIndex = iRow;
						// highLightElement(byRowColData, iRow + strColName);
						break;
					}
				}
				if (iRowIndex > 0)
				{
					By byRowColData2 = By.xpath("//form//div[@class='listBody']//div[@class='x-grid3-scroller']/div/div[" + iRowIndex + "]/table/tbody/tr/td[" + iColNo2 + "]");
					// highLightElement(byRowColData2, iRowIndex + strColName);
					strRowColData2 = getData(byRowColData2, iRowIndex + strColName, "Text");
				}
				else
				{
					if (iRowIndex == 0)
					{
						report.updateTestLog("getFormTableData", " No data found in the Table: " + strTableDesc, Status.FAIL);
					}
					else
					{
						report.updateTestLog("getFormTableData", strColName + ":" + strColData + " data not found in the Table: " + strTableDesc, Status.FAIL);
					}
				}
			}
			else
			{
				if (iColNo == -1)
				{
					report.updateTestLog("getFormTableData", "Column Name:" + strColName + " not found in the Table: " + strTableDesc, Status.FAIL);
				}
				else
				{
					report.updateTestLog("getFormTableData", "Column Name:" + strColNameDataToRetrive + " not found in the Table: " + strTableDesc, Status.FAIL);
				}
			}
		}
		return strRowColData2;
	}

	public boolean clickLinkFromTable(String strTableDesc, String strColName, String strColData) throws Exception
	{
		String strRowColData = null;
		Boolean bColDataFound = false;
		By byFormTable = By.xpath("//form//div[@class='listBody']");
		if (isElementVisible(byFormTable, "FormTable - " + strTableDesc))
		{
			// get column names from Header Table
			By byHeaderTableCols = By.xpath("//form//div[@class='listBody']//div[@class='x-grid3-header']/div/div/table/thead/tr/td/div");
			int iColNo = getColNumFromTable(byHeaderTableCols, strTableDesc, strColName);
			if (iColNo != -1)
			{
				By byScrollerTableRows = By.xpath("//form//div[@class='listBody']//div[@class='x-grid3-scroller']/div/div");
				List<WebElement> eScrollerTableRows = driver.findElements(byScrollerTableRows);
				int iScrollerTableRowsCount = eScrollerTableRows.size();
				for (int iRow = 1; iRow <= iScrollerTableRowsCount; iRow++)
				{
					By byRowColData = By.xpath("//form//div[@class='listBody']//div[@class='x-grid3-scroller']/div/div[" + iRow + "]/table/tbody/tr/td[" + iColNo + "]/div");
					strRowColData = getData(byRowColData, iRow + strColName, "Text");
					if (strRowColData.equalsIgnoreCase(strColData))
					{
						highLightElement(byRowColData, iRow + strColName + ": " + strColData);
						clickLink(byRowColData, strColName + ": " + strColData);
						bColDataFound = true;
						break;
					}
				}
				if (!bColDataFound)
				{
					report.updateTestLog("clickLinkFromTable", strColName + ": " + strColData + " is not found in the Table: " + strTableDesc, Status.FAIL);
				}
			}
			else
			{
				report.updateTestLog("clickLinkFromTable", "Column Name:" + strColName + " not found in the Table: " + strTableDesc, Status.FAIL);
			}
		}
		return bColDataFound;
	}

	public void mouseHover(By by, String strElementDesc, int iWaitTime)
	{
		try
		{
			if (isElementPresent(by, strElementDesc, iWaitTime))
			{
				WebElement HoverElement = driver.findElement(by);
				JavascriptExecutor je = (JavascriptExecutor) driver;
				// Mouse move to Element
				String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover',true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
				je.executeScript(mouseOverScript, HoverElement);
			}
		}
		catch (Exception e)
		{
		}
	}

	public void mouseHover(WebElement element, String strElementDesc, int iWaitTime)
	{
		try
		{
			if (isElementPresent(element, strElementDesc))
			{
				WebElement HoverElement = element;
				JavascriptExecutor je = (JavascriptExecutor) driver;
				// Mouse move to Element
				String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover',true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
				je.executeScript(mouseOverScript, HoverElement);
			}
		}
		catch (Exception e)
		{
		}
	}

	public void actionHover(By by, String strElementDesc, int iWaitTime)
	{
		try
		{
			if (isElementVisible(by, strElementDesc, iWaitTime))
			{
				WebElement elementToClick = driver.findElement(by);
				highLightElement(elementToClick, strElementDesc);
				Actions action = new Actions(driver.getWebDriver());
				action.moveToElement(elementToClick).click(elementToClick).build().perform();
			}
		}
		catch (Exception e)
		{
		}
	}

	public void actionHover(WebElement element, String strElementDesc, int iWaitTime)
	{
		try
		{
			if (isElementPresent(element, strElementDesc))
			{
				// WebElement elementToClick = driver.findElement(by);
				highLightElement(element, strElementDesc);
				Actions action = new Actions(driver.getWebDriver());
				action.moveToElement(element).build().perform();
			}
		}
		catch (Exception e)
		{
		}
	}

	public void dragAndDrop(WebElement sourceElement, WebElement destinationElement)
	{
		try
		{
			System.out.println("dragAndDrop 1");
			if (sourceElement.isDisplayed() && destinationElement.isDisplayed())
			{
				Actions action = new Actions(driver.getWebDriver());
				System.out.println("dragAndDrop 2");
				action.dragAndDrop(sourceElement, destinationElement).build().perform();
				// action.clickAndHold(target)
				System.out.println("dragAndDrop 3");
			}
			else
			{
				System.out.println("Element was not displayed to drag");
			}
		}
		catch (StaleElementReferenceException e)
		{
			System.out.println("Element with " + sourceElement + "or" + destinationElement + "is not attached to the page document " + e.getStackTrace());
		}
		catch (NoSuchElementException e)
		{
			System.out.println("Element " + sourceElement + "or" + destinationElement + " was not found in DOM " + e.getStackTrace());
		}
		catch (Exception e)
		{
			System.out.println("Error occurred while performing drag and drop operation " + e.getStackTrace());
		}
		System.out.println("dragAndDrop4");
	}

	///
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public void timeLapse(int value)
	{
		try
		{
			TimeUnit.SECONDS.sleep(value);;
		}
		catch (Exception e)
		{
		}
	}

	public void handleAuthenticationLogin(String strAuth_UserName, String strAuth_Password)
	{
		String strAlertMsg = isAlertPresent();
		if (strAlertMsg != null)
		{
			try
			{
				// String strAlertMsg = driver.switchTo().alert().getText();
				driver.switchTo().alert();
				// Selenium-WebDriver Java Code for entering Username & Password
				// as below:
				driver.findElement(By.id("userID")).sendKeys(strAuth_UserName);
				driver.findElement(By.id("password")).sendKeys(strAuth_Password);
				driver.switchTo().alert().accept();
				driver.switchTo().defaultContent();
				// System.out.println("AlertMsg: " + strAlertMsg);
				// System.out.println("Accepted the alert successfully.");
			}
			catch (Throwable e)
			{
				System.err.println("Error came while waiting for the alert popup. " + e.getMessage());
			}
		}
		else
		{
			report.updateTestLog("isAlertPresent", "Alert is not Present", Status.FAIL);
		}
	}

	public void verifyOptionsPresentinSelectBox(By bySelect, String strElementDesc, String strCase_TaskType_Options)
	{
		Select dropDown = new Select(driver.findElement(bySelect));
		List<WebElement> eOptions = dropDown.getOptions();
		List<String> strOptionsList = new ArrayList<String>();
		int iOptionCount = dropDown.getOptions().size();
		System.out.println("Combobox  Option Count: " + iOptionCount);
		for (int i = 0; i < iOptionCount; i++)
		{
			String strOption = eOptions.get(i).getText().trim();
			strOptionsList.add(strOption);
		} // for loop
		String[] strArray = null;
		if (strCase_TaskType_Options.contains(";"))
		{
			strArray = strCase_TaskType_Options.split(";");
		}
		int iArraySize = strArray.length;
		System.out.println("Array Length: " + iArraySize);
		int iOptionFoundCount = 0, iOptionNotFoundCount = 0;
		String strOptionsFound = "", strOptionsNotFound = "";
		for (int i = 0; i < iArraySize; i++)
		{
			String strArrayValue = strArray[i].trim();
			if (strOptionsList.contains(strArrayValue))
			{
				++iOptionFoundCount;
				if (strOptionsFound.equals(""))
				{
					strOptionsFound = strArrayValue;
				}
				else
				{
					strOptionsFound = strOptionsFound + "," + strArrayValue;
				}
			}
			else
			{
				++iOptionNotFoundCount;
				if (strOptionsNotFound.equals(""))
				{
					strOptionsNotFound = strArrayValue;
				}
				else
				{
					strOptionsNotFound = strOptionsNotFound + "," + strArrayValue;
				}
			}
		}
		System.out.println("Combobox  Option iOptionFoundCount: " + iOptionFoundCount);
		System.out.println("Combobox  Option iOptionNotFoundCount: " + iOptionNotFoundCount);
		if (iArraySize == iOptionFoundCount)
		{
			report.updateTestLog("verifyOptionsPresentinSelectBox <b> " + strElementDesc + "</b>", "All options (" + iOptionFoundCount + ") are present. Options present in Combo Box are <b>" + strOptionsFound + "</b>", Status.PASS);
			System.out.println("All options are present. Options present in Combo Box: " + strOptionsFound);
		}
		else
		{
			if (iArraySize == iOptionNotFoundCount)
			{
				report.updateTestLog("verifyOptionsPresentinSelectBox <b> " + strElementDesc + "</b>", "All options are not present. Options present in Combo Box are <b>" + strOptionsFound + "</b> and Options not  present in Combo Box: <<b>" + strOptionsNotFound + "</b>", Status.FAIL);
				System.out.println("verifyOptionsPresentinSelectBox " + strElementDesc + ", All options are not present. Options not  present in Combo Box are " + strOptionsNotFound);
			}
			else
			{
				report.updateTestLog("verifyOptionsPresentinSelectBox <b> " + strElementDesc + "</b>", iOptionFoundCount + " options present in Combo Box are <b>" + strOptionsFound + "</b> and " + iOptionNotFoundCount + " Options not  present in Combo Box are <b>" + strOptionsNotFound + "</b>", Status.FAIL);
				System.out.println("verifyOptionsPresentinSelectBox " + strElementDesc + ", " + iOptionFoundCount + " options present in Combo Box are " + strOptionsFound + " and " + iOptionNotFoundCount + " Options not  present in Combo Box are " + strOptionsNotFound);
			}
		}
	}

	public String selectARandomOptionFromComboBox(By bySelect, String strElementDesc) throws Exception
	{
		int iOptionCount = -1;
		String strRandomOption = null;
		if (isElementVisible(bySelect, strElementDesc))
		{
			Select dropDown = new Select(driver.findElement(bySelect));
			List<WebElement> eOptions = dropDown.getOptions();
			iOptionCount = eOptions.size();
			int iRandomOption = getRandomNumber(0, iOptionCount - 1);
			if (eOptions.get(iRandomOption).getText().trim().equalsIgnoreCase("--None--"))
				++iRandomOption;
			strRandomOption = eOptions.get(iRandomOption).getText();
			selectData(bySelect, strElementDesc, strRandomOption);
			System.out.println("Options present in the Combobox:" + strElementDesc);
		}
		else
		{
			report.updateTestLog("selectARandomOptionFromComboBox", "The Combo Box: <b>" + strElementDesc + "</b> is not present", Status.FAIL);
			System.out.println("selectARandomOptionFromComboBox:  The Combo Box: <b>" + strElementDesc + "</b> is not present");
		}
		return strRandomOption;
	}

	public String getSFPageTitle()
	{
		String strCurrentPageTitle = null;
		Boolean bPageTitleNotFound = true;
		while (bPageTitleNotFound)
		{
			strCurrentPageTitle = driver.getTitle();
			if (strCurrentPageTitle.toLowerCase().contains("loading..."))
			{
				waitForSeconds(3);
			}
			else
			{
				bPageTitleNotFound = false;
			}
		}
		return strCurrentPageTitle;
	}

	public void waitforSFPageLoad(By by, String strElementDesc) throws Exception
	{
		int iverifyRound = 1;
		for (int i = 1; i <= iverifyRound; i++)
		{
			if (isElementVisible(by, strElementDesc))
			{
				i = 100;
			}
		}
	}// waitforSFPageLoad

	public void deleteAllMailsFromMailinator() throws Exception
	{
		driver.switchTo().defaultContent();
		By byInbox = By.xpath("//div[@class='lb-container']//span[@id='inbox_button']/i");
		clickByJSE(byInbox, "Mailinator Inbox");
		String strXPath = "//section[@class='single_mail']//ul[@id='inboxpane']/li";
		By byInboxPane = By.xpath(strXPath);
		isElementVisible(byInboxPane, "Mailinator InboxPane");
		int iEmailCount = driver.findElements(By.xpath(strXPath)).size();
		System.out.println("iEmailCount: " + iEmailCount);
		for (int iEmail = 1; iEmail <= iEmailCount; iEmail++)
		{
			// delete mail
			By byMailSelectCheckBox = By.xpath(strXPath + "[" + iEmail + "]//i[contains(@id,'checkoff')]");
			clickByJSE(byMailSelectCheckBox, iEmail + " - Mail Checkbox");
		} // for loop
		By byMailDelete = By.xpath("//span[@title='Delete Emails']/i[@id='trash_but']");
		clickByJSE(byMailDelete, "byMailDelete");
		// waitForSeconds(3);
		// iEmailCount = driver.findElements(By.xpath(strXPath)).size();
		// System.out.println("iEmailCount: " + iEmailCount);
	}

	public String getRandomNumber(int charLength)
	{
		String strNum = String.valueOf(charLength < 1 ? 0 : new Random().nextInt((9 * (int) Math.pow(10, charLength - 1)) - 1) + (int) Math.pow(10, charLength - 1));
		return strNum;
	}

	public int getRandomNumber(int min, int max)
	{
		return (int) Math.floor(Math.random() * (max - min + 1)) + min;
	}

	public synchronized void robotClick(By by, String strElementDesc) throws Exception
	{
		if (isElementVisible(by, strElementDesc))
		{
			WebElement element = driver.findElement(by);
			Point p = element.getLocation();
			int x = p.getX();
			int y = p.getY();
			Dimension d = element.getSize();
			int h = d.getHeight();// To get the Height of the Element
			int w = d.getWidth();// To get the Width of the Element
			System.out.println("robotClick:  (X,Y): " + (x + (w / 2)) + " , " + (y + (h / 2)));
			highLightElement(element, strElementDesc);
			try
			{
				Robot robot = new Robot();
				robot.mouseMove(x + (w / 2), y + (h / 2) + 100);
				robot.delay(200); // delay is to make code wait for mentioned
									// milliseconds before executing next step
				robot.mousePress(InputEvent.BUTTON1_DOWN_MASK); // press left
																// click
				robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK); // release
																	// left
																	// click
				robot.delay(200);
				report.updateTestLog("Verify the Element <b>" + strElementDesc + "</b> is clicked", "The button <b>" + strElementDesc + "</b> is clicked", Status.DONE);
				System.out.println("robotClick:  Robot click on Element: " + strElementDesc);
			}
			catch (AWTException e)
			{
				System.out.println("robotClick:  Error in Robot click on Element: " + strElementDesc);
			}
		}
		else
		{
			report.updateTestLog("Verify the Element <b>" + strElementDesc + "</b> is clicked", "The Element <b>" + strElementDesc + "</b> is not present", Status.FAIL);
			System.out.println(strElementDesc + " is not present in the Page");
		}
	}// robotClick

	public void openUrlInNewTab(String strUrl) throws Exception
	{
		// To open Link in new Tab
		// String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,"t");
		// driver.findElement(By.linkText("urlLink")).sendKeys(selectLinkOpeninNewTab);
		switchToParentFrame();
		// driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +
		// "t");
		((JavascriptExecutor) driver.getWebDriver()).executeScript("window.open('about:blank','_blank');");
		switchToLastOpenedPage();
		driver.getWebDriver().get(strUrl);
		report.updateTestLog("openUrlInNewTab", "<b>Url: " + strUrl + " is opened in new Tab.</b>", Status.DONE);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public void unKnownErrorPopup() throws Exception
	{
		By byErrorPopup = By.xpath("//div[contains(@class,'x-window-plain')][contains(@style,'visibility: visible;')]//div[@class='x-tool x-tool-close x-tool-close-over']");
		if (isElementVisible(byErrorPopup, "byErrorPopup", 3))
		{
			clickByJSE(byErrorPopup, "byErrorPopup");
		}
	}

	public String multiSelectList(String strElementDesc) throws Exception
	{
		String strSelected = multiSelectList(strElementDesc, "");
		return strSelected;
	}

	public String multiSelectList(String strElementDesc, String strSelectValue) throws Exception
	{
		String strSelected = null;
		String strElementXPath = "//tr//*[normalize-space(text())='" + strElementDesc + "']/following::td[1]//table";
		if (isElementVisible(By.xpath(strElementXPath), strElementDesc))
		{
			// String strOptionXPath = strElementXPath +
			// "//select[contains(@id,'_unselected')]//option[text()='" +
			// strSelectValue + "']";
			if (strSelectValue.equals(""))
			{
				By byOptions = By.xpath(strElementXPath + "//select[contains(@id,'_unselected')]//option");
				int iOptionsCount = driver.findElements(byOptions).size();
				int iRandomNum = getRandomNumber(1, iOptionsCount);
				strSelectValue = driver.findElements(byOptions).get(iRandomNum - 1).getText();
			}
			By Option = By.xpath(strElementXPath + "//select[contains(@id,'_unselected')]//option[text()='" + strSelectValue + "']");
			if (isElementPresent(Option, strElementDesc + " - " + strSelectValue))
			{
				// scrollToElement(By.xpath(strOptionXPath), strElementDesc + "
				// - " + strSelectValue);
				actionDoubleClick(Option, strElementDesc + " - " + strSelectValue);
				String strChoosenXPath = strElementXPath + "//select[contains(@id,'_selected')]//option[text()='" + strSelectValue + "']";
				By byOption_Selected = By.xpath(strChoosenXPath);
				if (isElementVisible(byOption_Selected, strElementDesc + " Choosen: " + strSelectValue))
				{
					strSelected = strSelectValue;;
				}
				else
				{
					report.updateTestLog("multiSelectList: " + strElementDesc, strElementDesc + ": " + strSelectValue + " not selected", Status.FAIL);
				}
			}
		}
		return strSelected;
	}

	/**
	 * Description: verify Actual and Expected Data
	 * 
	 * @param String
	 *            strVerifyElementDesc - Description of the Element to be
	 *            verified.
	 * @param String
	 *            strActualData - Actual Data present in the Application page
	 * @param String
	 *            strExpectedData - Actual Data present in the Application page
	 * @throws Exception
	 */
	public Boolean verifyActualExpectedData(String strVerifyElementDesc, String strActualData, String strExpectedData) throws Exception
	{
		Boolean bVerify = verifyActualExpectedData(strVerifyElementDesc, strActualData, strExpectedData, "Exact", true);
		return bVerify;
	}

	/**
	 * Description: verify Actual and Expected Data
	 * 
	 * @param String
	 *            strVerifyElementDesc - Description of the Element to be
	 *            verified.
	 * @param String
	 *            strActualData - Actual Data present in the Application page
	 * @param String
	 *            strExpectedData - Actual Data present in the Application page
	 * @param String
	 *            strVerifyOption - Verify Options (Exact,Partial)
	 * @throws Exception
	 */
	public void verifyActualExpectedData(String strVerifyElementDesc, String strActualData, String strExpectedData, String strVerifyOption) throws Exception
	{
		verifyActualExpectedData(strVerifyElementDesc, strActualData, strExpectedData, strVerifyOption, true);
	}

	/**
	 * Description: verify Actual and Expected Data
	 * 
	 * @param String
	 *            strVerifyElementDesc - Description of the Element to be
	 *            verified.
	 * @param String
	 *            strActualData - Actual Data present in the Application page
	 * @param String
	 *            strExpectedData - Actual Data present in the Application page
	 * @param String
	 *            strVerifyOption - Verify Options (Exact,Partial)
	 * @param Boolean
	 *            bTakeScreenshotOnPass - TakeScreenshotOnPass (true or false).
	 *            true - Take Screenshot on Pass condition
	 * @return
	 * @throws Exception
	 */
	public void verifyActualExpectedData3(String strVerifyElementDesc, String strActualData, String strExpectedData1, String strExpectedData2, String strVerifyOption, Boolean bTakeScreenshotOnPass) throws Exception
	{
		// Verify
		if (compareActualExpectedData(strActualData, strExpectedData1, strVerifyOption))
		{
			if (bTakeScreenshotOnPass)
			{
				report.updateTestLog("Verify Element", strVerifyElementDesc + ", Actual Value: (" + strActualData + ") matches with Expected Value: (" + strExpectedData1 + ")", Status.PASS);
			}
			else
			{
				report.updateTestLog("Verify Element: " + strVerifyElementDesc, " Actual Value: (" + strActualData + ") matches with Expected Value: (" + strExpectedData1 + ")", Status.DONE);
			}
		}
		else
		{
			if (compareActualExpectedData(strActualData, strExpectedData2, strVerifyOption))
			{
				if (bTakeScreenshotOnPass)
				{
					report.updateTestLog("Verify Element: " + strVerifyElementDesc, " Actual Value: (" + strActualData + ") matches with Expected Value: (" + strExpectedData2 + ")", Status.PASS);
				}
				else
				{
					report.updateTestLog("Verify Element: " + strVerifyElementDesc, " Actual Value: (" + strActualData + ") matches with Expected Value: (" + strExpectedData2 + ")", Status.DONE);
				}
			}
		}
	}

	public Boolean compareActualExpectedData(String strActualData, String strExpectedData, String strVerifyOption) throws Exception
	{
		// Verify
		Boolean bVerify = false;
		switch (strVerifyOption.toLowerCase())
		{
			case "exact" :
			case "equal" :
			case "equals" :
				if (strActualData != null && strActualData.equals(strExpectedData))
				{
					bVerify = true;
				}
				break;
			case "equalsignorecase" :
				if (strActualData != null && strActualData.equalsIgnoreCase(strExpectedData))
				{
					bVerify = true;
				}
				break;
			case "endswith" :
				if (strActualData != null && strActualData.endsWith(strExpectedData))
				{
					bVerify = true;
				}
				break;
			case "startswith" :
				if (strActualData != null && strActualData.startsWith(strExpectedData))
				{
					bVerify = true;
				}
				break;
			case "partial" :
			case "contains" :
				if (strActualData != null && strActualData.contains(strExpectedData))
				{
					bVerify = true;
				}
				break;
			default :
				throw new Exception("Unknown Verify Option type '" + strVerifyOption + "'.Verify Option type allowed are 'Exact','Equal',Equals," + " EqualsIgnoreCase or 'Partial'.");
		}
		return bVerify;
	}// compareActualExpectedData() method

	public Boolean verifyActualExpectedData(String strVerifyElementDesc, String strActualData, String strExpectedData, String strVerifyOption, Boolean bTakeScreenshotOnPass) throws Exception
	{
		Boolean bVerify = compareActualExpectedData(strActualData, strExpectedData, strVerifyOption);
		if (bVerify)
		{
			System.out.println("Verify Element: " + strVerifyElementDesc + ", Actual Value: (" + strActualData + ") matches with Expected Value: (" + strExpectedData + ")");
			if (bTakeScreenshotOnPass)
			{
				report.updateTestLog("Verify Element: " + strVerifyElementDesc, "Actual Value: (" + strActualData + ") matches with Expected Value: (" + strExpectedData + ")", Status.PASS);
			}
			else
			{
				report.updateTestLog("Verify Element: " + strVerifyElementDesc, "Actual Value: (" + strActualData + ") matches with Expected Value: (" + strExpectedData + ")", Status.DONE);
			}
		}
		else
		{
			System.out.println("Verify Element: " + strVerifyElementDesc + ", Actual Value: (" + strActualData + ") not matches with Expected Value: (" + strExpectedData + ")");
			report.updateTestLog("Verify Element: " + strVerifyElementDesc, "Actual Value: (" + strActualData + ") not matches with Expected Value: (" + strExpectedData + ")", Status.FAIL);
		} // bVerify
		return bVerify;
	}

	/**
	 * Description: verify Actual and Expected data of numbers
	 * 
	 * @param String
	 *            strVerifyElementDesc - Description of the Element to be
	 *            verified.
	 * @param String
	 *            iActualData - Actual Data present in the Application page
	 * @param String
	 *            iExpectedData - Actual Data present in the Application page
	 * @throws Exception
	 */
	public void verifyActualExpectedNumber(String strVerifyElementDesc, int iActualData, int iExpectedData)
	{
		// Verify
		if (iActualData == iExpectedData)
		{
			System.out.println("Verify Element: " + strVerifyElementDesc + ", Actual Value: (" + iActualData + ") matches with Expected Value: (" + iExpectedData + ")");
			report.updateTestLog("Verify Element", strVerifyElementDesc + ", Actual Value: (" + iActualData + ") matches with Expected Value: (" + iExpectedData + ")", Status.PASS);
		}
		else
		{
			System.out.println("Verify Element: " + strVerifyElementDesc + ", Actual Value: (" + iActualData + ") not matches with Expected Value: (" + iExpectedData + ")");
			report.updateTestLog("Verify Element", strVerifyElementDesc + ", Actual Value: (" + iActualData + ") not matches with Expected Value: (" + iExpectedData + ")", Status.FAIL);
		}
	}

	///////////////////////////////////// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	public void clickByJSE2(By by, String strElementDesc) throws Exception
	{
		Boolean bElementPresent = isElementPresent(by, strElementDesc);
		if (bElementPresent)
		{
			try
			{
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].click();", driver.findElement(by));
				report.updateTestLog("Verify the button <b>" + strElementDesc + "</b> is clicked", "The button <b>" + strElementDesc + "</b> is clicked", Status.DONE);
			}
			catch (NoSuchElementException exc)
			{
				report.updateTestLog("Verify the button <b>" + strElementDesc + "</b> is clicked", "The button <b>" + strElementDesc + "</b> is not present", Status.FAIL);
			}
			catch (WebDriverException wdexc)
			{
				driver.findElement(by).click();
				report.updateTestLog("Verify the button <b>" + strElementDesc + "</b> is clicked", "The button <b>" + strElementDesc + "</b> is clicked", Status.DONE);
			}
			catch (Exception exc)
			{
			}
		}
		else
		{
			report.updateTestLog("Verify the button <b>" + strElementDesc + "</b> is clicked", "The button <b>" + strElementDesc + "</b> is not present", Status.FAIL);
		}
	}

	public String getDataFromLinkLetTableexpanded(String strTableName, String strColName, int iRowNo) throws Exception
	{
		String strRowColData = null;
		String strLinkLetTableXPath = "//div[@class='bPageTitle']//*[starts-with(text(),'" + strTableName + "')]/ancestor::td//table";
		By byLinkLetTable = By.xpath(strLinkLetTableXPath);
		if (isElementVisible(byLinkLetTable, "LinkLetTable - " + strTableName))
		{
			// get column names from Header Table
			By byHeaderTableCols = By.xpath(strLinkLetTableXPath + "/tbody/tr[@class='headerRow']/th");
			int iColNo = getColNumFromTable(byHeaderTableCols, strTableName, strColName);
			if (iColNo != -1)
			{
				By byLinkLetTableRows = By.xpath(strLinkLetTableXPath + "/tbody/tr");
				List<WebElement> eLinkLetTableRows = driver.findElements(byLinkLetTableRows);
				int iLinkLetTableRowsCount = eLinkLetTableRows.size();
				System.out.println(strTableName + "RowCount: " + iLinkLetTableRowsCount);
				if (iRowNo >= 2 && iRowNo <= iLinkLetTableRowsCount)
				{
					By byRowColData = By.xpath(strLinkLetTableXPath + "/tbody/tr[" + iRowNo + "]/*[" + iColNo + "]");
					highLightElement(byRowColData, iRowNo + strColName);
					strRowColData = getData(byRowColData, iRowNo + strColName, "Text");
				}
				else
				{// error
					if (iRowNo < 2)
					{
						report.updateTestLog("getDataFromLinkLetTable", "Invalid Row no.: " + iRowNo + " No recodrs found in the Table. " + strTableName, Status.FAIL);
					}
					else
					{
						report.updateTestLog("getDataFromLinkLetTable", "Invalid Row no.: " + iRowNo + ". RowNo is greater than " + iLinkLetTableRowsCount + " found in the Table: " + strTableName, Status.FAIL);
					}
				} // error
			}
			else
			{
				report.updateTestLog("getDataFromLinkLetTable", "Column Name:" + strColName + " not found in the Table: " + strTableName, Status.FAIL);
			}
		} // LinkLetTable present
		return strRowColData;
	}// get

	public void verifyValueAutopopulated(By by, String strElementDesc, String strAttribute) throws Exception
	{
		try
		{
			moveToElement(driver.findElement(by));
			String DefaultName = getData(by, strElementDesc, strAttribute);
			if (!(DefaultName.isEmpty() || DefaultName.equalsIgnoreCase("")))
			{
				report.updateTestLog("Verify the value is autopopulated in " + strElementDesc, "Value is auto populated in " + strElementDesc, Status.PASS);
			}
			else
			{
				report.updateTestLog("Verify the value is autopopulated in " + strElementDesc, "Value is not auto populated in " + strElementDesc, Status.FAIL);
			}
		}
		catch (Exception e)
		{
		}
	}

	public String getDataFromLinkLetTableCheckbox(String strTableName, String strColName, String strColData, String strColNameDataToRetrive) throws Exception
	{
		String strRowColData = null, strRowColData2 = null;
		int iRowIndex = 0;
		String strLinkLetTableXPath = "//td[@class='pbTitle']//*[starts-with(text(),'" + strTableName + "')]//ancestor::table/parent::div/following-sibling::div/table";
		By byLinkLetTable = By.xpath(strLinkLetTableXPath);
		if (isElementVisible(byLinkLetTable, "LinkLetTable - " + strTableName))
		{
			By byHeaderTableCols = By.xpath(strLinkLetTableXPath + "/tbody/tr[@class='headerRow']/th");// get
																										// column
																										// names
																										// from
																										// Header
																										// Table
			int iColNo = getColNumFromTable(byHeaderTableCols, strTableName, strColName);
			int iColNo2 = getColNumFromTable(byHeaderTableCols, strTableName, strColNameDataToRetrive);
			if (iColNo != -1 && iColNo2 != -1)
			{
				By byLinkLetTableRows = By.xpath(strLinkLetTableXPath + "/tbody/tr");
				List<WebElement> eLinkLetTableRows = driver.findElements(byLinkLetTableRows);
				int iLinkLetTableRowsCount = eLinkLetTableRows.size();
				for (int iRow = 2; iRow <= iLinkLetTableRowsCount; iRow++)
				{
					By byRowColData = By.xpath(strLinkLetTableXPath + "/tbody/tr[" + iRow + "]/*[" + iColNo + "]");
					strRowColData = getData(byRowColData, iRow + strColName, "text");
					if (strRowColData.equalsIgnoreCase(strColData))
					{
						iRowIndex = iRow;
						highLightElement(byRowColData, iRow + strColName);
						break;
					}
				}
				if (iRowIndex > 0)
				{ // Second Column Data to get
					By byRowColData2 = By.xpath(strLinkLetTableXPath + "/tbody/tr[" + iRowIndex + "]/*[" + iColNo2 + "]/img");//////// Checkbox
					highLightElement(byRowColData2, iRowIndex + strColName);
					strRowColData2 = getData(byRowColData2, iRowIndex + strColNameDataToRetrive, "title");
				}
				else
				{
					if (iLinkLetTableRowsCount <= 1)
					{
						report.updateTestLog("getDataFromLinkLetTableCheckbox", " No data found in the Table: " + strTableName, Status.FAIL);
					} /*
						 * else{ report.updateTestLog(
						 * "getDataFromLinkLetTableCheckbox", strColName + ":" +
						 * strColData + " data not found in the Table: " +
						 * strTableName, Status.FAIL); }
						 */
				}
			}
			else
			{
				if (iColNo == -1)
				{
					report.updateTestLog("getDataFromLinkLetTableCheckbox", "Column Name:" + strColName + " not found in the Table: " + strTableName, Status.FAIL);
				}
				else
				{
					report.updateTestLog("getDataFromLinkLetTableCheckbox", "Column Name:" + strColNameDataToRetrive + " not found in the Table: " + strTableName, Status.FAIL);
				}
			}
		}
		return strRowColData2;
	}

	public String USTimeZoneTimeFormatForBellNotification()
	{
		Date date = new Date();
		/* Specifying the format */
		DateFormat requiredFormat = new SimpleDateFormat("MM/dd");
		DateFormat requiredFormat1 = new SimpleDateFormat("HH:mm");
		DateFormat requiredFormat2 = new SimpleDateFormat("a");
		/* Setting the Timezone */
		TimeZone timeZone = TimeZone.getTimeZone("US/Eastern");
		requiredFormat.setTimeZone(timeZone);
		requiredFormat1.setTimeZone(timeZone);
		requiredFormat2.setTimeZone(timeZone);
		/* Picking the day value in the required Format */
		String strCurrentDay = requiredFormat.format(date).toLowerCase();
		String strCurrentTime = requiredFormat1.format(date).toLowerCase();
		// String strCurrentDay2 = requiredFormat2.format(date).toLowerCase();
		Boolean isZerofound = strCurrentTime.startsWith("0");
		if (isZerofound)
		{
			strCurrentTime = strCurrentTime.replaceFirst("0", "");
		}
		String StrCurrentTime1 = (String) strCurrentTime.substring(0, 2);
		String strDatevale = strCurrentDay + " " + "at" + " " + StrCurrentTime1;
		System.out.println(strDatevale);
		return strDatevale;
	}

	public String trimDate(String strDate)
	{
		String[] Dates = strDate.split("/");
		String Month = Dates[0];
		String Date = Dates[1];
		String Year = Dates[2];
		Boolean isZerofound = Date.startsWith("0");
		if (isZerofound)
		{
			Date = Date.replaceFirst("0", "");
		}
		Boolean isZerofound1 = Month.startsWith("0");
		if (isZerofound1)
		{
			Month = Month.replaceFirst("0", "");
		}
		String ActualDate = Month + "/" + Date + "/" + Year;
		return ActualDate;
	}

	public String getDataAttributeFromLinkLetTableCheckbox(String strTableName, String strColName, String strColData, String strColNameDataToRetrive, String strAttribute) throws Exception
	{
		String strRowColData = null, strRowColData2 = null;
		int iRowIndex = 0;
		String strLinkLetTableXPath = "//td[@class='pbTitle']//*[starts-with(text(),'" + strTableName + "')]//ancestor::table/parent::div/following-sibling::div/table";
		By byLinkLetTable = By.xpath(strLinkLetTableXPath);
		try
		{
			if (isElementVisible(byLinkLetTable, "LinkLetTable - " + strTableName))
			{
				By byHeaderTableCols = By.xpath(strLinkLetTableXPath + "/tbody/tr[@class='headerRow']/th");// get
																											// column
																											// names
																											// from
																											// Header
																											// Table
				int iColNo = getColNumFromTable(byHeaderTableCols, strTableName, strColName);
				int iColNo2 = getColNumFromTable(byHeaderTableCols, strTableName, strColNameDataToRetrive);
				if (iColNo != -1 && iColNo2 != -1)
				{
					By byLinkLetTableRows = By.xpath(strLinkLetTableXPath + "/tbody/tr");
					List<WebElement> eLinkLetTableRows = driver.findElements(byLinkLetTableRows);
					int iLinkLetTableRowsCount = eLinkLetTableRows.size();
					for (int iRow = 2; iRow <= iLinkLetTableRowsCount; iRow++)
					{
						By byRowColData = By.xpath(strLinkLetTableXPath + "/tbody/tr[" + iRow + "]/*[" + iColNo + "]");
						strRowColData = getData(byRowColData, iRow + strColName, "Text");
						if (strRowColData.equalsIgnoreCase(strColData))
						{
							iRowIndex = iRow;
							highLightElement(byRowColData, iRow + strColName);
							break;
						}
					}
					if (iRowIndex > 0)
					{
						By byRowColData2 = By.xpath(strLinkLetTableXPath + "/tbody/tr[" + iRowIndex + "]/*[" + iColNo2 + "]/img");
						// highLightElement(byRowColData2, iRowIndex +
						// strColName);
						strRowColData2 = getData(byRowColData2, iRowIndex + strColName, strAttribute);
					}
					else
					{
						if (iLinkLetTableRowsCount <= 1)
						{
							report.updateTestLog("getFormTableData", " No data found in the Table: " + strTableName, Status.FAIL);
						}
						else
						{
							report.updateTestLog("getFormTableData", strColName + ":" + strColData + " data not found in the Table: " + strTableName, Status.FAIL);
						}
					}
				}
				else
				{
					if (iColNo == -1)
					{
						report.updateTestLog("getFormTableData", "Column Name:" + strColName + " not found in the Table: " + strTableName, Status.FAIL);
					}
					else
					{
						report.updateTestLog("getFormTableData", "Column Name:" + strColNameDataToRetrive + " not found in the Table: " + strTableName, Status.FAIL);
					}
				}
			}
		}
		catch (Exception e)
		{
		}
		return strRowColData2;
	}

	public void verifyActualExpectedData(String strVerifyElementDesc, By by, String strAttribute, String strExpectedData, String strVerifyOption) throws Exception
	{
		try
		{
			moveToElement(driver.findElement(by));
			String strActualData = getData(by, strVerifyElementDesc, strAttribute);
			if (strVerifyElementDesc.contains("Phone"))
			{
				strActualData = strActualData.replaceAll("-", "");
			}
			verifyActualExpectedData(strVerifyElementDesc, strActualData, strExpectedData, strVerifyOption);
		}
		catch (Exception e)
		{
		}
	}

	public void verifyActualExpectedSFData(String strElementType, String strVerifyElementDesc, String strExpectedData, String strAttribute, String strVerifyOption) throws Exception
	{
		try
		{
			// Verify
			String strActualData = getSFData(strVerifyElementDesc, strElementType, strAttribute);
			verifyActualExpectedData(strVerifyElementDesc, strActualData, strExpectedData, strVerifyOption);
		}
		catch (Exception e)
		{
		}
	}

	/////////////////////////////////////////////
	public void verifyPageElement(By byLocator, String strElementDesc, String strAttribure, String strExpectedValue, String strVerifyType, Boolean bScreenShotReq) throws Exception
	{
		System.out.println("verifyElementVisible: " + strElementDesc);
		// if(driverUtil.objectExists(byLocator)){
		if (isElementVisible(byLocator, strElementDesc))
		{
			highLightElement(byLocator, strElementDesc);
			String strActualValue = getData(byLocator, strElementDesc, strAttribure);
			System.out.println(strElementDesc + " ActualValue: " + strActualValue);
			verifyActualExpectedData(strElementDesc, strActualValue, strExpectedValue, strVerifyType, bScreenShotReq);
		}
		else
		{
			report.updateTestLog("verifyPageElement", "Element not Visible: " + strElementDesc, Status.FAIL);
		}
	}// End of verifyPageElement()

	public void pageRefresh()
	{
		switchToParentFrame();
		driver.navigate().refresh();
	}

	public String formatDateTimeMarketWise(Date dNow, String locale, String profile, String strDateOrTime)
	{
		String datecalls = "", timecalls = "";
		SimpleDateFormat time;
		switch (locale.trim().toUpperCase())
		{
			case "IE" :
				if (profile.trim().equalsIgnoreCase("Commercial"))
					datecalls = new SimpleDateFormat("d/M/YYYY").format(dNow).toString();
				else
					datecalls = new SimpleDateFormat("YYYY/MM/dd").format(dNow).toString();
				break;
			case "JP" :
				datecalls = new SimpleDateFormat("YYYY/MM/dd").format(dNow).toString();
				time = new SimpleDateFormat("HH:mm");
				timecalls = time.format(dNow).toString();
				break;
			case "EU2" :
			case "RU" :
				datecalls = new SimpleDateFormat("dd.MM.YYYY").format(dNow).toString();
				time = new SimpleDateFormat("HH:mm");
				timecalls = time.format(dNow).toString();
				break;
			case "EU1" :
			case "BR" :
			case "CA" :
				datecalls = new SimpleDateFormat("dd/MM/YYYY").format(dNow).toString();
				time = new SimpleDateFormat("h:mm a");
				timecalls = time.format(dNow).toString();
				break;
			case "ANZ" :
				datecalls = new SimpleDateFormat("d/MM/YYYY").format(dNow).toString();
				time = new SimpleDateFormat("HH:mm");
				timecalls = time.format(dNow).toString();
				break;
			case "CN" :
				datecalls = new SimpleDateFormat("YYYY/M/d").format(dNow).toString();
				time = new SimpleDateFormat("ah:mm");
				timecalls = time.format(dNow).toString();
				break;
			case "US" :
				datecalls = new SimpleDateFormat("M/d/YYYY").format(dNow).toString();
				time = new SimpleDateFormat("h:mm a");
				timecalls = time.format(dNow).toString();
				break;
			default :
				time = new SimpleDateFormat("HH:mm");
				timecalls = time.format(dNow).toString();
		}
		if (strDateOrTime.trim().equalsIgnoreCase("date"))
			return datecalls;
		else if (strDateOrTime.trim().equalsIgnoreCase("time"))
			return timecalls;
		else
			return datecalls + " " + timecalls;
	}

	public void verifyElementDataPresent(By by, String strElementDesc, String strAttribute) throws Exception
	{
		String strElement_Data = getData(by, strElementDesc, strAttribute);
		if (strElement_Data.equals(""))
		{
			report.updateTestLog("Verify " + strElementDesc, strElementDesc + " - No Value", Status.FAIL);
		}
		else
		{
			report.updateTestLog("Verify " + strElementDesc, strElementDesc + " - Value: " + strElement_Data, Status.DONE);
		}
	}

	public void verifySFElementDataPresent(String strElementDesc, String strElementType, String strAttribute) throws Exception
	{
		String strElement_Data = getSFData(strElementDesc, strElementType, strAttribute);
		if (strElement_Data.equals(""))
		{
			report.updateTestLog("Verify " + strElementDesc, strElementDesc + " - No Value", Status.FAIL);
		}
		else
		{
			report.updateTestLog("Verify " + strElementDesc, strElementDesc + " - Value: " + strElement_Data, Status.PASS);
		}
	}

	public void verifySFElementData(String strTableName, String strElementDesc, String strElementType, String strAttribute, String strExpectedValue) throws Exception
	{
		String strElement_Data;
		if (!strAttribute.trim().equalsIgnoreCase(""))
		{
			if (strTableName.trim().equalsIgnoreCase(""))
				strElement_Data = getSFData(strElementDesc, strElementType, strAttribute);
			else
				strElement_Data = getSFData(strTableName, strElementDesc, strElementType, strAttribute);
			if (strElement_Data.equals(""))
			{
				report.updateTestLog("Verify " + strElementDesc, strElementDesc + " - No Value", Status.FAIL);
			}
			else if (strElement_Data.trim().equalsIgnoreCase(strExpectedValue))
			{
				report.updateTestLog("Verify " + strElementDesc, strElementDesc + " - Value: " + strElement_Data, Status.DONE);
			}
			else
				report.updateTestLog("Verify " + strElementDesc, strElementDesc + " value mismatch. Expected : " + strExpectedValue + "; Actual : " + strElement_Data, Status.FAIL);
		}
		else
		{
			By byElement;
			if (strTableName.trim().equalsIgnoreCase(""))
				byElement = getSFElementXPath(strElementDesc, strElementType);
			else
				byElement = getSFElementXPath(strTableName, strElementDesc, strElementType);
			if (isElementPresent(byElement))
				report.updateTestLog("Verify " + strElementDesc, strElementDesc + " - is present", Status.DONE);
			else
				report.updateTestLog("Verify " + strElementDesc, strElementDesc + " - is not present", Status.FAIL);
		}
	}

	public void expandLinkLetTable(String strTableName) throws Exception
	{
		//////////// Show more ///////////////////////////////////
		if (isSFLinkLetRecordCountMore(strTableName))
		{
			By byShowMore = By.xpath("//td[@class='pbTitle']//*[text()='" + strTableName + "']//ancestor::table/parent::div/following-sibling::div/div[@class='pShowMore']/a[contains(text(),'Show')][contains(text(),'more')]");
			clickLink(byShowMore, strTableName + " - ShowMore");
			waitForSeconds(3);
		}
	}

	public void noneditable(String strTableName, String strElementDesc, String strElementType, String strAttribute) throws Exception
	{
		By by = getSFElementXPath(strTableName, strElementDesc, strElementType);
		String value = null;
		if (strElementType.equals("non-editable"))
		{
			if (isElementVisible(by, strElementDesc))
			{
				highLightElement(by, strElementDesc);
				value = getSFData(strElementDesc, strElementType, strAttribute);
				if (value.equals("!meta.editable"))
					report.updateTestLog("Verify able to see " + strElementDesc + " under " + strTableName + " as non editable.", "Able to see " + strElementDesc + " under " + strTableName + " as non editable.", Status.PASS);
				else
					report.updateTestLog("Verify able to see " + strElementDesc + " under " + strTableName + " as non editable.", "Unable to see " + strElementDesc + " under " + strTableName + " as non editable.", Status.FAIL);
			}
		}
	}

}