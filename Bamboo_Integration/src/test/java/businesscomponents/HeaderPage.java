package businesscomponents;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URLDecoder;
import java.util.concurrent.TimeUnit;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import com.mfa.framework.FrameworkException;
import com.mfa.framework.Status;
import ObjectRepository.ObjVeevaEmail;
import ObjectRepository.objHeaderPage;
import supportlibraries.ConvenienceFunctions;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import org.apache.poi.xssf.usermodel.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

public class HeaderPage extends ReusableLibrary
{
	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);

	public HeaderPage(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}

	/**
	 * This method will help with launching and logging into Veeva application
	 * 
	 * @throws Exception
	 */
	public void launchAndLoginToVeeva() throws Exception
	{
		openVeevaApp();
		loginToVeevaApp();
	}

	/**
	 * This method will launch the Veeva application
	 * 
	 * @throws Exception
	 */
	public void openVeevaApp() throws Exception
	{
		String strSalesForceUrl = properties.getProperty("VeevaAppUrl");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(80, TimeUnit.SECONDS);
		driver.manage().timeouts().setScriptTimeout(80, TimeUnit.SECONDS);
		driver.get(strSalesForceUrl);
		if (cf.isElementVisible(ObjVeevaEmail.loginUserName, "login - User Name"))
		{
			report.updateTestLog("Verify Veeva Login page is opened", "Veeva Url: <b>" + strSalesForceUrl + "<b/>, Login page is opened", Status.PASS);
		}
		else
		{
			throw new FrameworkException("Veeva Url: <b>" + strSalesForceUrl + "<b/>, Login page is not opened");
		}
	}

	/**
	 * This method will enter login credentials and login based on username and
	 * password from CommonTestData
	 * 
	 * @throws Exception
	 */
	public void loginToVeevaApp() throws Exception
	{
		String strDataSheet = "Login";
		String strUserName = dataTable.getData("Login", "SF_UserName");
		String strPassword = dataTable.getData(strDataSheet, "SF_Password");
		cf.setData(ObjVeevaEmail.loginUserName, "User Name", strUserName);
		cf.setData(ObjVeevaEmail.loginPassword, "Password", strPassword);
		report.updateTestLog("login To Veeva App", "Login Page", Status.SCREENSHOT);
		cf.clickButton(ObjVeevaEmail.loginSubmit, "Login Submit");
		if (cf.isElementVisible(By.cssSelector(".continue"), "Continue"))
		{
			report.updateTestLog("Scheduled Maintenace", "Maintenance Alert is displayed", Status.WARNING);
			cf.clickButton(By.className("continue"), "Continue Maintenance Alert");
		}
		if (cf.isElementVisible(ObjVeevaEmail.passwordResetCancel, "PasswordResetCancel", 5))
		{
			report.updateTestLog("Verify password expired", "Login Password expired. Please update the password ", Status.WARNING);
			cf.clickButton(ObjVeevaEmail.passwordResetCancel, "PasswordResetCancel");
		}
		else if (cf.isElementVisible(By.xpath("//*[@id='box']"), "Maintenance Alert Page"))
		{
			report.updateTestLog("Scheduled Maintenace", "Maintenance Alert is displayed", Status.WARNING);
			cf.clickButton(By.xpath("//*[@id='box']//a"), "Continue in maintenance alert");
		}
		else if (cf.isElementVisible(ObjVeevaEmail.menu, "Veeva - User Home Page"))
		{
			cf.waitForSeconds(2); // screenshot is taken without page load
			report.updateTestLog("Verify login is successful", "Login is successful", Status.PASS);
		}
		else
		{
			if (cf.isElementVisible(ObjVeevaEmail.passwordResetCancel, "PasswordResetCancel", 5))
			{
				report.updateTestLog("Verify password expired", "Login Password expired. Please update the password ", Status.WARNING);
				cf.clickButton(ObjVeevaEmail.passwordResetCancel, "PasswordResetCancel");
				if (cf.isElementVisible(ObjVeevaEmail.menu, "Veeva - User Home Page"))
				{
					cf.waitForSeconds(2); // screenshot is taken without page
											// load
					report.updateTestLog("Verify login is successful", "Login is successful", Status.PASS);
				}
			}

		}

		//cf.waitForSeconds(5);
		String currentUrl = driver.getCurrentUrl();
		System.out.println(currentUrl);
		if (currentUrl.contains("lightning"))
		{
			String[] splitUrl = currentUrl.split("com");
			currentUrl = splitUrl[0] + "com/ltng/switcher?destination=classic&referrer=%2Flightning%2Fpage%2Fhome";
			driver.get(currentUrl);
		}
		cf.waitForSeconds(5);
		if (cf.isElementVisible(ObjVeevaEmail.menu, "Veeva - User Home Page"))
			report.updateTestLog("Verify Successful Login", "User is successfully logged in", Status.PASS);
		else
			report.updateTestLog("Verify Successful Login", "User is not logged in", Status.FAIL);

		//languageSetupVerification();
	}

	public void loginAsSystemAdmin() throws Exception
	{
		String strLocale = dataTable.getData("Login", "Locale");
		String strCommonDataHeader = "#VeevaLogin_" + (strLocale.trim().toUpperCase()) + "_SysAdmin";// VeevaLogin_IE_SysAdmin
		String strSysAdminUserName = dataTable.getCommonData("SF_UserName", strCommonDataHeader);
		String strSysAdminPassword = dataTable.getCommonData("SF_Password", strCommonDataHeader);
		cf.setData(ObjVeevaEmail.loginUserName, "User Name", strSysAdminUserName);
		cf.setData(ObjVeevaEmail.loginPassword, "Password", strSysAdminPassword);
		cf.clickButton(ObjVeevaEmail.loginSubmit, "Login Submit");
		report.updateTestLog("login To Veeva App", "Login Page", Status.SCREENSHOT);
		if (cf.isElementVisible(ObjVeevaEmail.passwordResetCancel, "PasswordResetCancel", 5))
		{
			report.updateTestLog("Verify password expired", "Login Password expired. Please update the password ", Status.WARNING);
			cf.clickButton(ObjVeevaEmail.passwordResetCancel, "PasswordResetCancel");
		}
		if (cf.isElementVisible(By.xpath("//*[@id='box']"), "Maintenance Alert Page"))
		{
			report.updateTestLog("Scheduled Maintenace", "Maintenance Alert is displayed", Status.WARNING);
			cf.clickButton(By.xpath("//*[@id='box']//a"), "Continue in maintenance alert");
		}

		cf.waitForSeconds(5);
		String currentUrl = driver.getCurrentUrl();
		System.out.println("Current URL: "+currentUrl);
		if (currentUrl.contains("lightning"))
		{
			String[] splitUrl = currentUrl.split("com");
			currentUrl = splitUrl[0]+"com/ltng/switcher?destination=classic&referrer=%2Flightning%2Fpage%2Fhome";
			System.out.println("Changed URL: "+currentUrl);
			driver.get(currentUrl);
		}
		
		cf.waitForSeconds(5);
		if (cf.isElementVisible(ObjVeevaEmail.menu, "Veeva - User Home Page"))
			report.updateTestLog("Verify Successful Login", "User is successfully logged in as System Admin", Status.PASS);
		else
			report.updateTestLog("Verify Successful Login", "System Admin User is not logged in", Status.FAIL);
		languageSetupVerification();
	}

	/**
	 * This method is used to search for any user and login (this method is used
	 * for login without entering password)
	 * 
	 * @throws Exception
	 */
	public void searchUserAndLoginFromSysAdmin() throws Exception
	{
		cf.clickButton(By.cssSelector("a[title*='Home Tab']"), "Home");
		String strSideBarCollapseStatus = cf.getData(By.cssSelector("#sidebarCell"), "Side Bar link", "class");
		if (strSideBarCollapseStatus.toLowerCase().contains("collapsed"))
			cf.clickButton(By.cssSelector("#sidebarCell"), "Sidebar");

		String strUserName = dataTable.getData("Login", "SF_UserName");
		String strLocale = dataTable.getData("Login", "Locale");

		if (!strLocale.equals("CN"))
			cf.selectData(objHeaderPage.selectSearch, "Search Users", "Users");

		cf.setData(objHeaderPage.textSearch, "Username", strUserName);
		cf.clickButton(objHeaderPage.btnGo, "Go");
		if (cf.isElementVisible(By.xpath("//h1[contains(text(),'User')]"), "User Page"))
		{
			cf.clickSFButton("Login", "button");
			cf.waitForSeconds(5);
			String currentUrl = driver.getCurrentUrl();
			System.out.println("Current URL: "+currentUrl);
			if (currentUrl.contains("lightning"))
			{
				String[] splitUrl = currentUrl.split("com");
				currentUrl = splitUrl[0]+"com/ltng/switcher?destination=classic&referrer=%2Flightning%2Fpage%2Fhome";
				System.out.println("Changed URL: "+currentUrl);
				driver.get(currentUrl);
			}
			
			cf.waitForSeconds(5);
			if (cf.isElementVisible(ObjVeevaEmail.menu, "Veeva - User Home Page"))
				report.updateTestLog("Verify Successful Login", "User is successfully logged in as System Admin", Status.PASS);
			else
				report.updateTestLog("Verify Successful Login", "System Admin User is not logged in", Status.FAIL);
			languageSetupVerification();
			
			if (cf.isElementVisible(By.xpath("//span[contains(text(),'Logged in as')]"), "Successful login"))
			{
				report.updateTestLog("Verify Successful Login", "User '" + strUserName + "' is successfully logged in without password", Status.PASS);
			}
			else
				report.updateTestLog("Verify Successful Login", "User '" + strUserName + "' is not logged in from System Admin page", Status.FAIL);
		}
		else
			report.updateTestLog("Verify User Page", "Search user landing page is not displayed", Status.FAIL);
		
		cf.waitForSeconds(5);
		String currentUrl = driver.getCurrentUrl();
		System.out.println("Current URL: "+currentUrl);
		if (currentUrl.contains("lightning"))
		{
			String[] splitUrl = currentUrl.split("com");
			currentUrl = splitUrl[0]+"com/ltng/switcher?destination=classic&referrer=%2Flightning%2Fpage%2Fhome";
			System.out.println("Changed URL: "+currentUrl);
			driver.get(currentUrl);
		}
		
		cf.waitForSeconds(5);
		if (cf.isElementVisible(ObjVeevaEmail.menu, "Veeva - User Home Page"))
			report.updateTestLog("Verify Successful Login", "User is successfully logged in as System Admin", Status.PASS);
		else
			report.updateTestLog("Verify Successful Login", "System Admin User is not logged in", Status.FAIL);
	}

	/**
	 * This method is used to logout and land in system admin page
	 * 
	 * @throws Exception
	 */
	public void logOutToSysAdmin() throws Exception
	{
		cf.switchToParentFrame();
		if (cf.isElementVisible(ObjVeevaEmail.logOutUser, "Logout"))
		{
			driver.findElement(ObjVeevaEmail.logOutUser).click();
			cf.waitForSeconds(3);
			cf.clickByJSE(ObjVeevaEmail.logOutLink, "Logout");
			cf.waitForSeconds(3);
		}
	}

	public void logOut() throws Exception
	{
		cf.switchToParentFrame();
		if (cf.isElementVisible(ObjVeevaEmail.logOutUser, "Logout"))
		{
			driver.findElement(ObjVeevaEmail.logOutUser).click();
			cf.waitForSeconds(3);
			cf.clickByJSE(ObjVeevaEmail.logOutLink, "Logout");
			cf.waitForSeconds(3);
		}
	}
	


	

	public void languageSetupVerification() throws Exception
	{
		String strHelpAndTraining = cf.getData(objHeaderPage.linkHelpAndTraining, "Help and Training", "text");
		String strLocale = dataTable.getData("Login", "Locale");
		if (!strHelpAndTraining.trim().equalsIgnoreCase("Help & Training"))
		{
			switch (strLocale.trim().toUpperCase())
			{
				case "RU" :
				case "BR" :
				case "JP" :
					cf.clickButton(objHeaderPage.linkUserName, "Username");
					cf.clickButton(objHeaderPage.linkPersonalSetup, "Personal Setup");
					cf.clickButton(objHeaderPage.sideNavPersonalInfo, "Personal Information Side Bar Header");
					cf.clickButton(objHeaderPage.linkPersonalInformation, "Personal Information Side Bar");
					cf.clickButton(objHeaderPage.linkLanguageAndTimeZone, "Language and Time Zone");
					cf.selectData(objHeaderPage.selectLocaleSetup, "Language and Locale Setup", "English");
					cf.clickButton(objHeaderPage.btnSaveInPersonalLanguageTimeZone, "Language and Time Zone");
					break;
				case "CN" :
					cf.clickButton(objHeaderPage.linkUserName, "Username");
					cf.clickButton(objHeaderPage.setupCN, "Setup");
					cf.clickButton(objHeaderPage.sideNavPersonalInfoExpand, "Personal Information Side Bar Header");
					cf.clickButton(objHeaderPage.linkPersonalInformation, "Personal Information");
					cf.clickButton(objHeaderPage.btnEditPersonalInformation, "Edit Personal Information");
					cf.selectData(objHeaderPage.selectLanguageInChina, "Language and Locale Setup", "English");
					cf.clickButton(objHeaderPage.btnSaveInPersonalLanguageTimeZone, "Language and Time Zone");
					break;
				// Case EU2 added by Murali on 07/02/2019
				case "EU2" :
					cf.clickButton(objHeaderPage.linkUserName, "Username");
					cf.clickButton(objHeaderPage.linkPersonalSetup, "Personal Setup");
					cf.clickButton(objHeaderPage.sideNavPersonalInfo, "Personal Information Side Bar Header");
					cf.clickButton(objHeaderPage.advanceUserDetails, "AdvanceUserDetails Link");
					cf.clickButton(objHeaderPage.editbutton, "Editbutton Link");
					cf.selectData(objHeaderPage.selectLanguageInChina, "Language and Locale Setup", "English");
					cf.clickButton(objHeaderPage.saveButton, "saveButton Link");
					break;
			}
			report.updateTestLog("Verify Language Setup", "Language setup is changed to English", Status.PASS);
		}
		else
			report.updateTestLog("Verify Language Setup", "Language setup is English", Status.SCREENSHOT);
	}


	

	public void unDelete() throws Exception
	{
		String strWorkbenchAppUrl = properties.getProperty("WorkbenchAppUrl");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(80, TimeUnit.SECONDS);
		driver.get(strWorkbenchAppUrl);
		if (cf.isElementPresent(By.xpath("//label[text()='Environment:']"), "Workbench - Home Page"))
		{
			Select dropdown = new Select(driver.findElement(By.xpath("//select[@id='oauth_env']")));
			dropdown.selectByVisibleText("Sandbox");
			System.out.println("Sandbox selected from Environment");
			cf.clickElement(By.xpath("(//input[@type='checkbox'])[2]"), "Agree Button");
			cf.clickElement(By.xpath("//input[@type='submit']"), "Login with salesforce");
		}
		if (cf.isElementPresent(By.xpath("//h2[text()='Allow Access?']")))
		{
			cf.clickElement(By.xpath("//input[@title='Allow']"), "Allow");
		}
	}

	public void passwordResetWorkbench() throws Exception
	{
		String strWorkbenchAppUrl = properties.getProperty("WorkbenchAppUrl");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(80, TimeUnit.SECONDS);
		driver.get(strWorkbenchAppUrl);
		report.updateTestLog("Verify " + strWorkbenchAppUrl + "is launch", strWorkbenchAppUrl + " is launched", Status.SCREENSHOT);
		if (cf.isElementPresent(By.xpath("//label[text()='Environment:']"), "Workbench - Home Page"))
		{
			Select dropdown = new Select(driver.findElement(By.xpath("//select[@id='oauth_env']")));
			dropdown.selectByVisibleText("Sandbox");
			System.out.println("Sandbox selected from Environment");
			cf.clickElement(By.xpath("(//input[@type='checkbox'])[2]"), "Agree Button");
			cf.clickElement(By.xpath("//input[@type='submit']"), "Login with salesforce");
		}

		if (cf.isElementPresent(By.xpath("//h2[text()='Allow Access?']")))
		{
			cf.clickElement(By.xpath("//input[@title='Allow']"), "Allow");
		}
		else
		{
			String url = driver.getTitle();
			if (url.equals("Login | Salesforce"))
			{
				String strDataSheet = "Login";
				String strUserName = dataTable.getData(strDataSheet, "SF_UserName");
				String strPassword = dataTable.getData(strDataSheet, "SF_Password");
				cf.setData(ObjVeevaEmail.loginUserName, "User Name", strUserName);
				cf.setData(ObjVeevaEmail.loginPassword, "Password", strPassword);
				report.updateTestLog("login To Veeva App", "Login Page", Status.SCREENSHOT);
				cf.clickButton(ObjVeevaEmail.loginSubmit, "Login Submit");

				if (cf.isElementPresent(By.xpath("//h2[text()='Allow Access?']")))
				{
					cf.clickElement(By.xpath("//input[@title='Allow']"), "Allow");
				}
			}
		}
		if (cf.isElementPresent(By.xpath("//span[text()='utilities']")))
		{
			cf.clickElement(By.xpath("//span[text()='utilities']"), "utilities");
			cf.clickElement(By.xpath("//a[text()='Password Management']"), "Password Management");
		}
		else
		{
			report.updateTestLog("Verify utilities is Visible", "Utilities is NOT Visible", Status.FAIL);
		}
		if (cf.isElementPresent(By.xpath("//td[text()='Password Management']")))
		{
			String userid = dataTable.getData("Login", "UserID");
			// Set Password
			cf.clickElement(By.xpath("//input[@type='radio' and @value='set']"), "Radio - Set button");
			cf.setData(By.xpath("//input[@id='userId']"), "", userid);
			cf.setData(By.xpath("//input[@id='passwordOne']"), "Password", "Veeva2020");
			cf.setData(By.xpath("//input[@id='passwordConfirm']"), "Confirm Password", "Veeva2020");
			cf.clickElement(By.xpath("//input[@id='changePasswordAction']"), "Change Password - Button");

			report.updateTestLog("Verify password is change", "Password is changed", Status.SCREENSHOT);

			if (cf.isElementPresent(By.xpath("//p[text()='UNKNOWN_EXCEPTION: invalid repeated password']")))
			{
				report.updateTestLog("Verify Invalid repeated password", "Invalid repeated password", Status.FAIL);
			}
			if (cf.isElementPresent(By.xpath("//p[contains(text(),'Successfully set password for " + userid + "')]")))
			{
				report.updateTestLog("Verify Password Changed", "Password Changed", Status.PASS);
			}
			else
			{
				report.updateTestLog("Verify Password Changed", "Password is NOT Changed", Status.FAIL);
			}
		}
		else
		{
			report.updateTestLog("Verify Password Management is Visible", "Password Management is NOT Visible", Status.FAIL);
		}
	}

		
}
