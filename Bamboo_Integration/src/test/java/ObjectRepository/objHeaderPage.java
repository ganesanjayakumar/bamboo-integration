package ObjectRepository;

import org.openqa.selenium.By;

public class objHeaderPage {

	public static By plussymbol=By.cssSelector("a[href*=showAllTabs]");
	public static By lnkGlobalAccountSearch=By.cssSelector("img[alt='Global Account Search']");
	public static By lblSearchHeader=By.cssSelector("h2.mainTitle");
	public static By lblMyAccountHeader=By.cssSelector("#top.pageType");
	public static By lblMyAccount=By.cssSelector("img[alt='My Accounts']");
	public static By selectSearch=By.id("sen");
	public static By textSearch=By.cssSelector(".standardSearchElementBody > input.searchTextBox");
	public static By btnGo=By.cssSelector("input[name='search'][title='Go!']");
	public static By linkUserName=By.cssSelector("#userNavButton");
	public static By linkPersonalSetup=By.cssSelector("#userNav-menuItems a[href*='Setup']");
	public static By linkDeveloperConsole=By.cssSelector("#userNav-menuItems a:nth-child(2)");
	public static By setupCN = By.xpath("//div[@id='userNav-menuItems']//a[contains(@class,'menuButtonMenuLink firstMenuItem')]");
	public static By sideNavPersonalInfoExpand=By.cssSelector("#PersonalInfo_icon");
	public static By linkHelpAndTraining=By.cssSelector(".navLinks a[href*='help']");
	public static By sideNavPersonalInfo=By.cssSelector("#PersonalInfo > a");
	public static By linkPersonalInformation=By.cssSelector("#PersonalInfo_child a[id*=PersonalInformation]");
	public static By linkHCMSearchAndAdd=By.cssSelector("[class*='HCM-Search-&-Add']");
	public static By linkHCMSearchAndAdd1=By.cssSelector("img[alt='HCM Search & Add']");
	// Case EU2 xpath added by Murali on 07/02/2019
		public static By advanceUserDetails = By.cssSelector("#PersonalInfo_child a[id*=AdvancedUserDetails_font]");
		public static By editbutton = By.cssSelector("td.pbButton>input[name*=edit]");
		public static By saveButton = By.cssSelector("td.pbButton input[name*=save]"); 
		
	public static By btnEditPersonalInformation=By.cssSelector("td.pbButton>input[onclick*=edit]");
	public static By linkLanguageAndTimeZone=By.cssSelector("[id*='LanguageAndTimeZone']");
	public static By selectLocaleSetup=By.cssSelector("select[id*='LanguageAndTimeZoneSetup'][name*='languageLocaleKey']");
	public static By selectLanguageInChina=By.cssSelector("select#LanguageLocaleKey");
	public static By btnSaveInPersonalLanguageTimeZone=By.cssSelector(".pbButtonb > input");
}
