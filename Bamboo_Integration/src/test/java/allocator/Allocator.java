package allocator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Platform;
import com.mfa.framework.ExcelDataAccessforxlsm;
import com.mfa.framework.FrameworkParameters;
import com.mfa.framework.IterationOptions;
import com.mfa.framework.Settings;
import com.mfa.framework.selenium.*;

/**
 * Class to manage the batch execution of test scripts within the framework
 * 
 * @author MFA
 */
public class Allocator
{
	private FrameworkParameters frameworkParameters = FrameworkParameters.getInstance();
	private Properties properties;
	private Properties mobileProperties;
	private ResultSummaryManager resultSummaryManager = ResultSummaryManager.getInstance();
	public static int timeout;
	public static int toogletimeout;

	/**
	 * The entry point of the test batch execution <br>
	 * Exits with a value of 0 if the test passes and 1 if the test fails
	 * 
	 * @param args
	 *            Command line arguments to the Allocator (Not applicable)
	 * @return 
	 */

	public static void main(String[] args)
	{
		closeProcessinWindows("Chromedriver");
		//closeProcessinWindows("veevacrmengage");
		cleanTempFiles();
		//EmptyDownloadsFolder();
		Allocator allocator = new Allocator();

		allocator.driveBatchExecution();
	}

	
	public static void EmptyDownloadsFolder() 
    {
          String strDownloadPath = "C:\\Users\\" + System.getProperty("user.name") + "\\Downloads";
            File file = new File(strDownloadPath); 
            File[] files = file.listFiles();
            if(files!=null)
            {
            for(int j=0;j<files.length;j++)
            {
                   if(files[j].delete()){
                        System.out.println(files[j].getName()+" is deleted successfully"); 
                   }
                    else
                     { 
                         System.out.println("Failed to delete the file"); 
                     } 
            }
            }
    }    

	public static void closeProcessinWindows(String strProcessName)
	{
		try
		{
			Runtime.getRuntime().exec("taskkill /F /IM " + strProcessName + ".exe");
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.out.println("Error in try to close the task: " + strProcessName);
		}
	}

	public void screenLockPrevent()
	{
	
		String strUserDir = System.getProperty("user.dir")+"\\src\\test\\java";
		//System.out.println(strUserDir);
		String strEnable_ScreenLock_Prevent = properties.getProperty("Enable_ScreenLock_Prevent");
		if (strEnable_ScreenLock_Prevent.equalsIgnoreCase("Yes"))
		{
			try
			{
				Runtime.getRuntime().exec("wscript " + strUserDir + "\\supportlibraries\\screenLockPrevent.vbs");
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	//Clear Temp Files from the system
	public static void cleanTempFiles()
	{
		String name = System.getProperty("java.io.tmpdir");
		File tmpdirectory = new File(name);
		try
		{
			FileUtils.cleanDirectory(tmpdirectory);
		}
		catch (IOException e)
		{
			System.out.println("Directory cleaned up...!");
		}
	}

	public void automaticSendEmail()
	{

		String strSendEmailReport = properties.getProperty("Enable_Send_Email_Report");
		String strRunConfiguration = properties.getProperty("RunConfiguration");
		String strOutLook_Email_Ids = properties.getProperty("OutLook_Email_Ids");
		if (strSendEmailReport.equalsIgnoreCase("Yes"))
		{
			try
			{
				//String strUserDir = System.getProperty("user.dir")+"\\src\\test\\java";
				String strUserDir = System.getProperty("user.dir");
				String strResultPath = strUserDir + "\\Results\\" + strRunConfiguration;
				String strExecuteCode = "cscript " + strUserDir + "\\supportlibraries\\TiggerScript.vbs " + strResultPath + " " + strOutLook_Email_Ids;
				Runtime.getRuntime().exec(strExecuteCode);
				System.out.println("Executed automated send email");
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}

		}
	}

	private void driveBatchExecution()
	{
		resultSummaryManager.setRelativePath();
		properties = Settings.getInstance();
		///////////////// screenLockPrevent code added by Kamatchirajan on Oct 02, 2018
		screenLockPrevent();
		mobileProperties = Settings.getMobilePropertiesInstance();

		////Added by Kamatchirajan on Jul 25
		//set the timeout for maximum instance
		timeout = Integer.parseInt(properties.getProperty("Timeout"));
		toogletimeout = Integer.parseInt(properties.getProperty("ToogleTimeout"));

		String runConfiguration;
		if (System.getProperty("RunConfiguration") != null)
		{
			runConfiguration = System.getProperty("RunConfiguration");
		}
		else
		{
			runConfiguration = properties.getProperty("RunConfiguration");
		}
		resultSummaryManager.initializeTestBatch(runConfiguration);

		int nThreads = Integer.parseInt(properties.getProperty("NumberOfThreads"));
		resultSummaryManager.initializeSummaryReport(nThreads);

		resultSummaryManager.setupErrorLog();

		int testBatchStatus = executeTestBatch(nThreads);

		resultSummaryManager.wrapUp(false);

		resultSummaryManager.manageJenkinsReportsFolder();

		resultSummaryManager.launchResultSummary();

		automaticSendEmail();////////////////////////////////////////////////////////////////////////

		closeProcessinWindows("wscript");
		System.exit(testBatchStatus);

	}

	/***** When working with SeeTest/Perfecto Parellel *****/
	/*
	 * private int executeTestBatch(int nThreads) { List<SeleniumTestParameters>
	 * testInstancesToRun = getRunInfo(frameworkParameters
	 * .getRunConfiguration()); ExecutorService parallelExecutor = Executors
	 * .newFixedThreadPool(nThreads); ParallelRunner testRunner = null; int i=0;
	 * while(i<testInstancesToRun.size()) { System.out.println("I:"+i); int
	 * size=i+nThreads; //System.out.println("First For"); for(int
	 * currentTestInstance
	 * =size-nThreads;currentTestInstance<size;currentTestInstance++) {
	 * testRunner = new ParallelRunner(
	 * testInstancesToRun.get(currentTestInstance));
	 * parallelExecutor.execute(testRunner);
	 * 
	 * if(frameworkParameters.getStopExecution()) { break; } }
	 * 
	 * parallelExecutor.shutdown(); while(!parallelExecutor.isTerminated()) {
	 * try { Thread.sleep(3000); } catch (InterruptedException e) {
	 * e.printStackTrace(); } } System.out.println("Waitng for thread to stop");
	 * i=size; } if (testRunner == null) { return 0; // All tests flagged as
	 * "No" in the Run Manager } else { return testRunner.getTestBatchStatus();
	 * } }
	 */

	private int executeTestBatch(int nThreads)
	{
		List<SeleniumTestParameters> testInstancesToRun = getRunInfo(frameworkParameters.getRunConfiguration());
		ExecutorService parallelExecutor = Executors.newFixedThreadPool(nThreads);
		ParallelRunner testRunner = null;

		for (int currentTestInstance = 0; currentTestInstance < testInstancesToRun.size(); currentTestInstance++)
		{
			testRunner = new ParallelRunner(testInstancesToRun.get(currentTestInstance));
			parallelExecutor.execute(testRunner);

			if (frameworkParameters.getStopExecution())
			{
				break;
			}
		}

		parallelExecutor.shutdown();
		while (!parallelExecutor.isTerminated())
		{
			try
			{
				Thread.sleep(3000);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}

		if (testRunner == null)
		{
			return 0; // All tests flagged as "No" in the Run Manager
		}
		else
		{
			return testRunner.getTestBatchStatus();
		}
	}

	private List<SeleniumTestParameters> getRunInfo(String sheetName)
	{
		ExcelDataAccessforxlsm runManagerAccess = new ExcelDataAccessforxlsm(frameworkParameters.getRelativePath(), "Run Manager");
		runManagerAccess.setDatasheetName(sheetName);

		runManagerAccess.setDatasheetName(sheetName);
		List<SeleniumTestParameters> testInstancesToRun = new ArrayList<SeleniumTestParameters>();
		String[] keys = {"Execute", "TestScenario", "TestCase", "TestInstance", "Description", "IterationMode", "StartIteration", "EndIteration", "TestConfigurationID"};
		List<Map<String, String>> values = runManagerAccess.getValues(keys);

		for (int currentTestInstance = 0; currentTestInstance < values.size(); currentTestInstance++)
		{

			Map<String, String> row = values.get(currentTestInstance);
			String executeFlag = row.get("Execute");

			if (executeFlag.equalsIgnoreCase("Yes"))
			{
				String currentScenario = row.get("TestScenario");
				String currentTestcase = row.get("TestCase");
				SeleniumTestParameters testParameters = new SeleniumTestParameters(currentScenario, currentTestcase);
				testParameters.setCurrentTestDescription(row.get("Description"));
				testParameters.setCurrentTestInstance("Instance" + row.get("TestInstance"));
				
				//System.out.println("TestScenario: "+currentScenario+"TestCase: "+currentTestcase);
				String iterationMode = row.get("IterationMode");
				if (!iterationMode.equals(""))
				{
					testParameters.setIterationMode(IterationOptions.valueOf(iterationMode));
				}

				else
				{
					testParameters.setIterationMode(IterationOptions.RUN_ALL_ITERATIONS);
				}

				String startIteration = row.get("StartIteration");
				if (!startIteration.equals(""))
				{
					testParameters.setStartIteration(Integer.parseInt(startIteration));
				}
				String endIteration = row.get("EndIteration");
				if (!endIteration.equals(""))
				{
					testParameters.setEndIteration(Integer.parseInt(endIteration));
				}
				String testConfig = row.get("TestConfigurationID");
				if (!"".equals(testConfig))
				{
					getTestConfigValues(runManagerAccess, "TestConfigurations", testConfig, testParameters);
				}

				testInstancesToRun.add(testParameters);
				runManagerAccess.setDatasheetName(sheetName);
			}
		}
		return testInstancesToRun;
	}

	private void getTestConfigValues(ExcelDataAccessforxlsm runManagerAccess, String sheetName, String testConfigName, SeleniumTestParameters testParameters)
	{

		runManagerAccess.setDatasheetName(sheetName);
		int rowNum = runManagerAccess.getRowNum(testConfigName, 0, 1);

		String[] keys = {"TestConfigurationID", "ExecutionMode", "MobileToolName", "MobileExecutionPlatform", "MobileOSVersion", "DeviceName", "Browser", "BrowserVersion", "Platform", "SeeTestPort", "IsAlexaTestCase"};
		Map<String, String> values = runManagerAccess.getValuesForSpecificRow(keys, rowNum);

		String executionMode = values.get("ExecutionMode");
		if (!"".equals(executionMode))
		{
			testParameters.setExecutionMode(ExecutionMode.valueOf(executionMode));
		}
		else
		{
			testParameters.setExecutionMode(ExecutionMode.valueOf(properties.getProperty("DefaultExecutionMode")));
		}

		String toolName = values.get("MobileToolName");
		if (!"".equals(toolName))
		{
			testParameters.setMobileToolName(MobileToolName.valueOf(toolName));
		}
		else
		{
			testParameters.setMobileToolName(MobileToolName.valueOf(mobileProperties.getProperty("DefaultMobileToolName")));
		}

		String executionPlatform = values.get("MobileExecutionPlatform");
		if (!"".equals(executionPlatform))
		{
			testParameters.setMobileExecutionPlatform(MobileExecutionPlatform.valueOf(executionPlatform));
		}
		else
		{
			testParameters.setMobileExecutionPlatform(MobileExecutionPlatform.valueOf(mobileProperties.getProperty("DefaultMobileExecutionPlatform")));
		}

		String mobileOSVersion = values.get("MobileOSVersion");
		if (!"".equals(mobileOSVersion))
		{
			testParameters.setmobileOSVersion(mobileOSVersion);
		}

		String deviceName = values.get("DeviceName");
		if (!"".equals(deviceName))
		{
			testParameters.setDeviceName(deviceName);
		}

		String browser = values.get("Browser");
		if (!"".equals(browser))
		{
			testParameters.setBrowser(Browser.valueOf(browser));
		}
		else
		{
			testParameters.setBrowser(Browser.valueOf(properties.getProperty("DefaultBrowser")));
		}

		String browserVersion = values.get("BrowserVersion");
		if (!"".equals(browserVersion))
		{
			testParameters.setBrowserVersion(browserVersion);
		}

		String platform = values.get("Platform");
		if (!"".equals(platform))
		{
			testParameters.setPlatform(Platform.valueOf(platform));
		}
		else
		{
			testParameters.setPlatform(Platform.valueOf(properties.getProperty("DefaultPlatform")));
		}

		String seeTestPort = values.get("SeeTestPort");
		if (!"".equals(seeTestPort))
		{
			testParameters.setSeeTestPort(seeTestPort);
		}
		else
		{
			testParameters.setSeeTestPort(mobileProperties.getProperty("SeeTestDefaultPort"));
		}

		String isAlexaTestCase = values.get("IsAlexaTestCase");
		if ("".endsWith(isAlexaTestCase) || isAlexaTestCase.equalsIgnoreCase("No"))
		{
			testParameters.setIsAlexaTestcase(false);
		}
		else if (isAlexaTestCase.equalsIgnoreCase("Yes"))
		{
			testParameters.setIsAlexaTestcase(true);
		}

	}

}